/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Yufei Cheng
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Yufei Cheng   <yfcheng@ittc.ku.edu>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */

#define NS_LOG_APPEND_CONTEXT                                   \
  if (GetObject<Node> ()) { std::clog << "[node " << GetObject<Node> ()->GetId () << "] "; }

#include <list>
#include <ctime>
#include <map>
#include <limits>
#include <algorithm>
#include <iostream>

#include "ns3/config.h"
#include "ns3/enum.h"
#include "ns3/string.h"
#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/assert.h"
#include "ns3/uinteger.h"
#include "ns3/net-device.h"
#include "ns3/packet.h"
#include "ns3/boolean.h"
#include "ns3/node-list.h"
#include "ns3/double.h"
#include "ns3/pointer.h"
#include "ns3/timer.h"
#include "ns3/object-vector.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ipv4-route.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/icmpv4-l4-protocol.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/wifi-net-device.h"
#include "ns3/inet-socket-address.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/tcp-socket-factory.h"
#include "ns3/llc-snap-header.h"
#include "ns3/arp-header.h"
#include "ns3/ipv6-interface.h"

#include "lbsr-rreq-table.h"
#include "lbsr-rcache.h"
#include "lbsr-routing.h"
#include "lbsr-fs-header.h"
#include "lbsr-options.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("LbsrRouting");


namespace lbsr {

NS_OBJECT_ENSURE_REGISTERED (LbsrRouting);

/* see http://www.iana.org/assignments/protocol-numbers */
const uint8_t LbsrRouting::PROT_NUMBER = 48;
/*
 * The extension header is the fixed size lbsr header, it is response for recognizing LBSR option types
 * and demux to right options to process the packet.
 *
 * The header format with neighboring layers is as follows:
 *
 +-+-+-+-+-+-+-+-+-+-+-
 |  Application Header |
 +-+-+-+-+-+-+-+-+-+-+-+
 |   Transport Header  |
 +-+-+-+-+-+-+-+-+-+-+-+
 |   Fixed LBSR Header  |
 +---------------------+
 |     LBSR Options     |
 +-+-+-+-+-+-+-+-+-+-+-+
 |      IP Header      |
 +-+-+-+-+-+-+-+-+-+-+-+
 */

TypeId LbsrRouting::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrRouting")
    .SetParent<IpL4Protocol> ()
    .SetGroupName ("Lbsr")
    .AddConstructor<LbsrRouting> ()
    .AddAttribute ("RouteCache",
                   "The route cache for saving routes from "
                   "route discovery process.",
                   PointerValue (0),
                   MakePointerAccessor (&LbsrRouting::SetRouteCache,
                                        &LbsrRouting::GetRouteCache),
                   MakePointerChecker<LbsrRouteCache> ())
    .AddAttribute ("RreqTable",
                   "The request table to manage route requests.",
                   PointerValue (0),
                   MakePointerAccessor (&LbsrRouting::SetRequestTable,
                                        &LbsrRouting::GetRequestTable),
                   MakePointerChecker<LbsrRreqTable> ())
    .AddAttribute ("PassiveBuffer",
                   "The passive buffer to manage "
                   "promisucously received passive ack.",
                   PointerValue (0),
                   MakePointerAccessor (&LbsrRouting::SetPassiveBuffer,
                                        &LbsrRouting::GetPassiveBuffer),
                   MakePointerChecker<LbsrPassiveBuffer> ())
    .AddAttribute ("MaxSendBuffLen",
                   "Maximum number of packets that can be stored "
                   "in send buffer.",
                   UintegerValue (64),
                   MakeUintegerAccessor (&LbsrRouting::m_maxSendBuffLen),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("MaxSendBuffTime",
                   "Maximum time packets can be queued in the send buffer .",
                   TimeValue (Seconds (30)),
                   MakeTimeAccessor (&LbsrRouting::m_sendBufferTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("MaxMaintLen",
                   "Maximum number of packets that can be stored "
                   "in maintenance buffer.",
                   UintegerValue (64),
                   MakeUintegerAccessor (&LbsrRouting::m_maxMaintainLen),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("MaxMaintTime",
                   "Maximum time packets can be queued in maintenance buffer.",
                   TimeValue (Seconds (30)),
                   MakeTimeAccessor (&LbsrRouting::m_maxMaintainTime),
                   MakeTimeChecker ())
    .AddAttribute ("MaxCacheLen",
                   "Maximum number of route entries that can be stored "
                   "in route cache.",
                   UintegerValue (64),
                   MakeUintegerAccessor (&LbsrRouting::m_maxCacheLen),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("RouteCacheTimeout",
                   "Maximum time the route cache can be queued in "
                   "route cache.",
                   TimeValue (Seconds (30)),
                   MakeTimeAccessor (&LbsrRouting::m_maxCacheTime),
                   MakeTimeChecker ())
    .AddAttribute ("MaxEntriesEachDst",
                   "Maximum number of route entries for a "
                   "single destination to respond.",
                   UintegerValue (20),
                   MakeUintegerAccessor (&LbsrRouting::m_maxEntriesEachDst),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("SendBuffInterval",
                   "How often to check send buffer for packet with route.",
                   TimeValue (Seconds (1)),
                   MakeTimeAccessor (&LbsrRouting::m_sendBuffInterval),
                   MakeTimeChecker ())
    .AddAttribute ("NodeTraversalTime",
                   "The time it takes to traverse two neighboring nodes.",
                   TimeValue (MilliSeconds (100)),
                   MakeTimeAccessor (&LbsrRouting::m_nodeTraversalTime),
                   MakeTimeChecker ())
    .AddAttribute ("RreqRetries",
                   "Maximum number of retransmissions for "
                   "request discovery of a route.",
                   UintegerValue (16),
                   MakeUintegerAccessor (&LbsrRouting::m_rreqRetries),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("MaintenanceRetries",
                   "Maximum number of retransmissions for "
                   "data packets from maintenance buffer.",
                   UintegerValue (2),
                   MakeUintegerAccessor (&LbsrRouting::m_maxMaintRexmt),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("RequestTableSize",
                   "Maximum number of request entries in the request table, "
                   "set this as the number of nodes in the simulation.",
                   UintegerValue (64),
                   MakeUintegerAccessor (&LbsrRouting::m_requestTableSize),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("RequestIdSize",
                   "Maximum number of request source Ids in "
                   "the request table.",
                   UintegerValue (16),
                   MakeUintegerAccessor (&LbsrRouting::m_requestTableIds),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("UniqueRequestIdSize",
                   "Maximum number of request Ids in "
                   "the request table for a single destination.",
                   UintegerValue (256),
                   MakeUintegerAccessor (&LbsrRouting::m_maxRreqId),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("NonPropRequestTimeout",
                   "The timeout value for non-propagation request.",
                   TimeValue (MilliSeconds (30)),
                   MakeTimeAccessor (&LbsrRouting::m_nonpropRequestTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("DiscoveryHopLimit",
                   "The max discovery hop limit for route requests.",
                   UintegerValue (255),
                   MakeUintegerAccessor (&LbsrRouting::m_discoveryHopLimit),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("MaxSalvageCount",
                   "The max salvage count for a single data packet.",
                   UintegerValue (15),
                   MakeUintegerAccessor (&LbsrRouting::m_maxSalvageCount),
                   MakeUintegerChecker<uint8_t> ())
    .AddAttribute ("BlacklistTimeout",
                   "The time for a neighbor to stay in blacklist.",
                   TimeValue (Seconds (3)),
                   MakeTimeAccessor (&LbsrRouting::m_blacklistTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("GratReplyHoldoff",
                   "The time for gratuitous reply entry to expire.",
                   TimeValue (Seconds (1)),
                   MakeTimeAccessor (&LbsrRouting::m_gratReplyHoldoff),
                   MakeTimeChecker ())
    .AddAttribute ("BroadcastJitter",
                   "The jitter time to avoid collision for broadcast packets.",
                   UintegerValue (10),
                   MakeUintegerAccessor (&LbsrRouting::m_broadcastJitter),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("LinkAckTimeout",
                   "The time a packet in maintenance buffer wait for "
                   "link acknowledgment.",
                   TimeValue (MilliSeconds (100)),
                   MakeTimeAccessor (&LbsrRouting::m_linkAckTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("TryLinkAcks",
                   "The number of link acknowledgment to use.",
                   UintegerValue (1),
                   MakeUintegerAccessor (&LbsrRouting::m_tryLinkAcks),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("PassiveAckTimeout",
                   "The time a packet in maintenance buffer wait for "
                   "passive acknowledgment.",
                   TimeValue (MilliSeconds (100)),
                   MakeTimeAccessor (&LbsrRouting::m_passiveAckTimeout),
                   MakeTimeChecker ())
    .AddAttribute ("TryPassiveAcks",
                   "The number of passive acknowledgment to use.",
                   UintegerValue (1),
                   MakeUintegerAccessor (&LbsrRouting::m_tryPassiveAcks),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("RequestPeriod",
                   "The base time interval between route requests.",
                   TimeValue (MilliSeconds (500)),
                   MakeTimeAccessor (&LbsrRouting::m_requestPeriod),
                   MakeTimeChecker ())
    .AddAttribute ("MaxRequestPeriod",
                   "The max time interval between route requests.",
                   TimeValue (Seconds (10)),
                   MakeTimeAccessor (&LbsrRouting::m_maxRequestPeriod),
                   MakeTimeChecker ())
    .AddAttribute ("GraReplyTableSize",
                   "The gratuitous reply table size.",
                   UintegerValue (64),
                   MakeUintegerAccessor (&LbsrRouting::m_graReplyTableSize),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("CacheType",
                   "Use Link Cache or use Path Cache",
                   StringValue ("LinkCache"),
                   MakeStringAccessor (&LbsrRouting::m_cacheType),
                   MakeStringChecker ())
    .AddAttribute ("StabilityDecrFactor",
                   "The stability decrease factor for link cache",
                   UintegerValue (2),
                   MakeUintegerAccessor (&LbsrRouting::m_stabilityDecrFactor),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("StabilityIncrFactor",
                   "The stability increase factor for link cache",
                   UintegerValue (4),
                   MakeUintegerAccessor (&LbsrRouting::m_stabilityIncrFactor),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("InitStability",
                   "The initial stability factor for link cache",
                   TimeValue (Seconds (25)),
                   MakeTimeAccessor (&LbsrRouting::m_initStability),
                   MakeTimeChecker ())
    .AddAttribute ("MinLifeTime",
                   "The minimal life time for link cache",
                   TimeValue (Seconds (1)),
                   MakeTimeAccessor (&LbsrRouting::m_minLifeTime),
                   MakeTimeChecker ())
    .AddAttribute ("UseExtends",
                   "The extension time for link cache",
                   TimeValue (Seconds (120)),
                   MakeTimeAccessor (&LbsrRouting::m_useExtends),
                   MakeTimeChecker ())
    .AddAttribute ("EnableSubRoute",
                   "Enables saving of sub route when receiving "
                   "route error messages, only available when "
                   "using path route cache",
                   BooleanValue (true),
                   MakeBooleanAccessor (&LbsrRouting::m_subRoute),
                   MakeBooleanChecker ())
    .AddAttribute ("RetransIncr",
                   "The increase time for retransmission timer "
                   "when facing network congestion",
                   TimeValue (MilliSeconds (20)),
                   MakeTimeAccessor (&LbsrRouting::m_retransIncr),
                   MakeTimeChecker ())
    .AddAttribute ("MaxNetworkQueueSize",
                   "The max number of packet to save in the network queue.",
                   UintegerValue (400),
                   MakeUintegerAccessor (&LbsrRouting::m_maxNetworkSize),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("MaxNetworkQueueDelay",
                   "The max time for a packet to stay in the network queue.",
                   TimeValue (Seconds (30.0)),
                   MakeTimeAccessor (&LbsrRouting::m_maxNetworkDelay),
                   MakeTimeChecker ())
    .AddAttribute ("NumPriorityQueues",
                   "The max number of packet to save in the network queue.",
                   UintegerValue (2),
                   MakeUintegerAccessor (&LbsrRouting::m_numPriorityQueues),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("LinkAcknowledgment",
                   "Enable Link layer acknowledgment mechanism",
                   BooleanValue (true),
                   MakeBooleanAccessor (&LbsrRouting::m_linkAck),
                   MakeBooleanChecker ())
    .AddTraceSource ("Tx",
                     "Send LBSR packet.",
                     MakeTraceSourceAccessor (&LbsrRouting::m_txPacketTrace),
                     "ns3::lbsr::LbsrOptionSRHeader::TracedCallback")
    .AddTraceSource ("Drop",
                     "Drop LBSR packet",
                     MakeTraceSourceAccessor (&LbsrRouting::m_dropTrace),
                     "ns3::Packet::TracedCallback")
  ;
  return tid;
}

LbsrRouting::LbsrRouting ()
{
  NS_LOG_FUNCTION_NOARGS ();
  //std::cout << "LbsrRouting::routing\n";

  m_uniformRandomVariable = CreateObject<UniformRandomVariable> ();

  /*
   * The following Ptr statements created objects for all the options header for LBSR, and each of them have
   * distinct option number assigned, when LBSR Routing received a packet from higher layer, it will find
   * the following options based on the option number, and pass the packet to the appropriate option to
   * process it. After the option processing, it will pass the packet back to LBSR Routing to send down layer.
   */
  Ptr<lbsr::LbsrOptionPad1> pad1Option = CreateObject<lbsr::LbsrOptionPad1> ();
  Ptr<lbsr::LbsrOptionPadn> padnOption = CreateObject<lbsr::LbsrOptionPadn> ();
  Ptr<lbsr::LbsrOptionRreq> rreqOption = CreateObject<lbsr::LbsrOptionRreq> ();
  Ptr<lbsr::LbsrOptionRrep> rrepOption = CreateObject<lbsr::LbsrOptionRrep> ();
  Ptr<lbsr::LbsrOptionLstop> lstopOption = CreateObject<lbsr::LbsrOptionLstop>();
  Ptr<lbsr::LbsrOptionSR>   srOption = CreateObject<lbsr::LbsrOptionSR> ();
  Ptr<lbsr::LbsrOptionRerr>   rerrOption = CreateObject<lbsr::LbsrOptionRerr> ();
  Ptr<lbsr::LbsrOptionAckReq> ackReq = CreateObject<lbsr::LbsrOptionAckReq> ();
  Ptr<lbsr::LbsrOptionAck> ack = CreateObject<lbsr::LbsrOptionAck> ();

  Insert (pad1Option);
  Insert (padnOption);
  Insert (rreqOption);
  Insert (rrepOption);
  Insert (lstopOption);
  Insert (srOption);
  Insert (rerrOption);
  Insert (ackReq);
  Insert (ack);

  // Check the send buffer for sending packets
  m_sendBuffTimer.SetFunction (&LbsrRouting::SendBuffTimerExpire, this);
  m_sendBuffTimer.Schedule (Seconds (100));
}

LbsrRouting::~LbsrRouting ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

void
LbsrRouting::NotifyNewAggregate ()
{
  NS_LOG_FUNCTION (this << "NotifyNewAggregate\n");
  //std::cout <<"LbsrRouting::NotifyNewAggregate\n";
  if (m_node == 0)
    {
      Ptr<Node> node = this->GetObject<Node> ();
      if (node != 0)
        {
          m_ipv4 = this->GetObject<Ipv4L3Protocol> ();
          if (m_ipv4 != 0)
            {
              this->SetNode (node);
              m_ipv4->Insert (this);
              this->SetDownTarget (MakeCallback (&Ipv4L3Protocol::Send, m_ipv4));
            }

          m_ip = node->GetObject<Ipv4> ();
          if (m_ip != 0)
            {
              NS_LOG_DEBUG ("Ipv4 started");
            }
        }
    }
  IpL4Protocol::NotifyNewAggregate ();
  EventId notifyId = Simulator::ScheduleNow (&LbsrRouting::Start, this);
  m_evrntIdVector.push_back(notifyId);
}

int LbsrRouting::s_nodeNumber;
int LbsrRouting::BroadCastCount;
int LbsrRouting::UniCastCount;
void LbsrRouting::Start ()
{
  std::cout << "LbsrRouting::Start mainAddress\n";
  SetSendLreqFlag(false);
  NS_LOG_FUNCTION (this << "Start LBSR Routing protocol\n");

  NS_LOG_INFO ("The number of network queues " << m_numPriorityQueues);//m_numPriorityQueues is 2
 // std::cout << "The number of network queues " << m_numPriorityQueues << "\n";
  for (uint32_t i = 0; i < m_numPriorityQueues; i++)
    {
      // Set the network queue max size and the delay
      NS_LOG_INFO ("The network queue size " << m_maxNetworkSize << " and the queue delay " << m_maxNetworkDelay.GetSeconds ());
      Ptr<lbsr::LbsrNetworkQueue> queue_i = CreateObject<lbsr::LbsrNetworkQueue> (m_maxNetworkSize,m_maxNetworkDelay);
      std::pair<std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator, bool> result_i = m_priorityQueue.insert (std::make_pair (i, queue_i));
      NS_ASSERT_MSG (result_i.second, "Error in creating queues");
    }
  Ptr<lbsr::LbsrRreqTable> rreqTable = CreateObject<lbsr::LbsrRreqTable> ();
  // Set the initial hop limit
  rreqTable->SetInitHopLimit (m_discoveryHopLimit);
  // Configure the request table parameters
  rreqTable->SetRreqTableSize (m_requestTableSize);
  rreqTable->SetRreqIdSize (m_requestTableIds);
  rreqTable->SetUniqueRreqIdSize (m_maxRreqId);
  SetRequestTable (rreqTable);
  // Set the passive buffer parameters using just the send buffer parameters
  Ptr<lbsr::LbsrPassiveBuffer> passiveBuffer = CreateObject<lbsr::LbsrPassiveBuffer> ();
  passiveBuffer->SetMaxQueueLen (m_maxSendBuffLen);
  passiveBuffer->SetPassiveBufferTimeout (m_sendBufferTimeout);
  SetPassiveBuffer (passiveBuffer);

  // Set the send buffer parameters
  m_sendBuffer.SetMaxQueueLen (m_maxSendBuffLen);
  m_sendBuffer.SetSendBufferTimeout (m_sendBufferTimeout);
  // Set the error buffer parameters using just the send buffer parameters
  m_errorBuffer.SetMaxQueueLen (m_maxSendBuffLen);
  m_errorBuffer.SetErrorBufferTimeout (m_sendBufferTimeout);
  // Set the maintenance buffer parameters
  m_maintainBuffer.SetMaxQueueLen (m_maxMaintainLen);
  m_maintainBuffer.SetMaintainBufferTimeout (m_maxMaintainTime);
  // Set the gratuitous reply table size
  m_graReply.SetGraTableSize (m_graReplyTableSize);

  if (m_mainAddress == Ipv4Address ())
    {
      Ipv4Address loopback ("127.0.0.1");
      for (uint32_t i = 0; i < m_ipv4->GetNInterfaces (); i++)//i=0 addr is 127.0.0.1 GetInterfaces = 2でノードの総数を取得している
        {
          // Use primary address, if multiple
          Ipv4Address addr = m_ipv4->GetAddress (i, 0).GetLocal ();
          m_broadcast = m_ipv4->GetAddress (i, 0).GetBroadcast ();//First broadcast
         // std::cout << "m_broadcast is " << m_broadcast <<"\n";
          if (addr != loopback)//loopback = 127.0.0.1
            {
              /*
               * Set lbsr route cache
               */
              std::cout << "addr is " << addr <<"\n";
              NodeNumberpuls();

              Ptr<lbsr::LbsrRouteCache> routeCache = CreateObject<lbsr::LbsrRouteCache> ();
              // Configure the path cache parameters
              routeCache->SetCacheType (m_cacheType);//m_cacheType is "LinkCache"
              routeCache->SetSubRoute (m_subRoute);
              routeCache->SetMaxCacheLen (m_maxCacheLen);
              routeCache->SetCacheTimeout (m_maxCacheTime);
              routeCache->SetMaxEntriesEachDst (m_maxEntriesEachDst);
              // Parameters for link, cache here is all same
              routeCache->SetStabilityDecrFactor (m_stabilityDecrFactor);
              routeCache->SetStabilityIncrFactor (m_stabilityIncrFactor);
              routeCache->SetInitStability (m_initStability);
              routeCache->SetMinLifeTime (m_minLifeTime);
              routeCache->SetUseExtends (m_useExtends);
              routeCache->ScheduleTimer ();
              // The call back to handle link error and send error message to appropriate nodes
              /// TODO whether this SendRerrWhenBreaksLinkToNextHop is used or not
              // routeCache->SetCallback (MakeCallback (&LbsrRouting::SendRerrWhenBreaksLinkToNextHop, this));
              //std::cout << "routeCache is "<< routeCache << "\n";
              SetRouteCache (routeCache);
              // Set the main address as the current ip address
              m_mainAddress = addr;

              m_ipv4->GetNetDevice (1)->SetPromiscReceiveCallback (MakeCallback (&LbsrRouting::PromiscReceive, this));

              // Allow neighbor manager use this interface for layer 2 feedback if possible
              Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (addr));
              Ptr<WifiNetDevice> wifi = dev->GetObject<WifiNetDevice> ();
              if (wifi == 0)
                {
                  break;
                }
              Ptr<WifiMac> mac = wifi->GetMac ();
              if (mac == 0)
                {
                  break;
                }

              routeCache->AddArpCache (m_ipv4->GetInterface (i)->GetArpCache ());
              NS_LOG_LOGIC ("Starting LBSR on node " << m_mainAddress);
              //std::cout << "Starting LBSR on node " <<  m_mainAddress << "and m_ipv4 is "<< m_ipv4 <<"\n";
              break;
            }
        }
      promiscCount=0;
      LreqRetryCount=0;
      BroadCastCount=0;
      UniCastCount=0;
      NS_ASSERT (m_mainAddress != Ipv4Address () && m_broadcast != Ipv4Address ());
      //std::cout << "m_routeCache is " << m_routeCache << "\n";
    }
}

Ptr<NetDevice>
LbsrRouting::GetNetDeviceFromContext (std::string context)
{
	//std::cout << "LbsrRouting::GetNetDeviceFromContext\n";

  // Use "NodeList/*/DeviceList/*/ as reference
  // where element [1] is the Node Id
  // element [2] is the NetDevice Id
  std::vector <std::string> elements = GetElementsFromContext (context);
  Ptr<Node> n = NodeList::GetNode (atoi (elements[1].c_str ()));
  NS_ASSERT (n);
  return n->GetDevice (atoi (elements[3].c_str ()));
}

std::vector<std::string>
LbsrRouting::GetElementsFromContext (std::string context)
{
	//std::cout << "LbsrRouting::GetEle,emtsFromContext\n";

  std::vector <std::string> elements;
  size_t pos1 = 0, pos2;
  while (pos1 != context.npos)
    {
      pos1 = context.find ("/",pos1);
      pos2 = context.find ("/",pos1 + 1);
      elements.push_back (context.substr (pos1 + 1,pos2 - (pos1 + 1)));
      pos1 = pos2;
    }
  return elements;
}

void
LbsrRouting::DoDispose (void)
{
	//std::cout << "LbsrRouting::DoDispose\n";

  NS_LOG_FUNCTION_NOARGS ();
  m_node = 0;
  for (uint32_t i = 0; i < m_ipv4->GetNInterfaces (); i++)
    {
      // Disable layer 2 link state monitoring (if possible)
      Ptr<NetDevice> dev = m_ipv4->GetNetDevice (i);
      Ptr<WifiNetDevice> wifi = dev->GetObject<WifiNetDevice> ();
      if (wifi != 0)
        {
          Ptr<WifiMac> mac = wifi->GetMac ()->GetObject<AdhocWifiMac> ();
          if (mac != 0)
            {
              mac->TraceDisconnectWithoutContext ("TxErrHeader", m_routeCache->GetTxErrorCallback ());
              m_routeCache->DelArpCache (m_ipv4->GetInterface (i)->GetArpCache ());
            }
		  }
	  }
	IpL4Protocol::DoDispose ();
}

void
LbsrRouting::SetNode (Ptr<Node> node)
{
	//std::cout << "LbsrRouting::SetNode\n";

  m_node = node;
}

Ptr<Node>
LbsrRouting::GetNode () const
{
	//std::cout << "LbsrRouting::GetNode\n";

  NS_LOG_FUNCTION_NOARGS ();
  return m_node;
}

void LbsrRouting::SetRouteCache (Ptr<lbsr::LbsrRouteCache> r)
{
	//std::cout << "LbsrRouting::SetRouteCache\n";

  // / Set the route cache to use
  m_routeCache = r;
}

Ptr<lbsr::LbsrRouteCache>
LbsrRouting::GetRouteCache () const
{
	//std::cout << "LbsrRouting::GetRouteCache\n";

  // / Get the route cache to use
  return m_routeCache;
}

void LbsrRouting::SetRequestTable (Ptr<lbsr::LbsrRreqTable> q)
{
	//std::cout << "LbsrRouting::SetRequestTable\n";

  // / Set the request table to use
  m_rreqTable = q;
}

Ptr<lbsr::LbsrRreqTable>
LbsrRouting::GetRequestTable () const
{
	//std::cout << "LbsrRouting::GetRequestTable\n";

  // / Get the request table to use
  return m_rreqTable;
}

void LbsrRouting::SetPassiveBuffer (Ptr<lbsr::LbsrPassiveBuffer> p)
{
	//std::cout << "LbsrRouting::SetPassiveBuffer\n";

  // / Set the request table to use
  m_passiveBuffer = p;
}

Ptr<lbsr::LbsrPassiveBuffer>
LbsrRouting::GetPassiveBuffer () const
{
	//std::cout << "LbsrRouting::GetPassiveBuffer\n";

  // / Get the request table to use
  return m_passiveBuffer;
}

Ptr<Node>
LbsrRouting::GetNodeWithAddress (Ipv4Address ipv4Address)
{
	//std::cout << "LbsrRouting::GetNodeWihAddress\n";

  NS_LOG_FUNCTION (this << ipv4Address);
  int32_t nNodes = NodeList::GetNNodes ();
  for (int32_t i = 0; i < nNodes; ++i)
    {
      Ptr<Node> node = NodeList::GetNode (i);
      Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
      int32_t ifIndex = ipv4->GetInterfaceForAddress (ipv4Address);
      if (ifIndex != -1)
        {
          return node;
        }
    }
  return 0;
}

bool LbsrRouting::IsLinkCache ()
{
	//std::cout << "LbsrRouting::IsLinkCache\n";

  return m_routeCache->IsLinkCache ();
}

void LbsrRouting::UseExtends (LbsrRouteCacheEntry::IP_VECTOR rt)
{
	//std::cout << "LbsrRouting::UseExtends\n";

  m_routeCache->UseExtends (rt);
}

bool LbsrRouting::LookupRoute (Ipv4Address id, LbsrRouteCacheEntry & rt)
{
	//std::cout << "LbsrRouting::LookupRoute\n";

  return m_routeCache->LookupRoute (id, rt);
}

bool LbsrRouting::AddRoute_Link (LbsrRouteCacheEntry::IP_VECTOR nodelist, Ipv4Address source)
{
	//std::cout << "LbsrRouting::AddRoute_Link source is "<< source << "\n";
	/*
	std::cout <<"--------------------------\n";
	for (std::vector<Ipv4Address>::const_iterator i = nodelist.begin();
			i != nodelist.end(); ++i){
  	  std::cout << "nodeist is " << *i << "\n";
    }
	std::cout <<"--------------------------\n";
  */
	Ipv4Address nextHop = SearchNextHop (source, nodelist);
  m_errorBuffer.DropPacketForErrLink (source, nextHop);
  m_routeCache->AddRoute_Link (nodelist, source);
 // std::cout << "AddRoute_Link m_routeCache is "<< m_routeCache << "\n";
  return m_routeCache;
}

bool LbsrRouting::AddRoute (LbsrRouteCacheEntry & rt)
{
  //std::cout << "LbsrRouting::AddRoute\n";
  std::vector<Ipv4Address> nodelist = rt.GetVector ();
  Ipv4Address nextHop = SearchNextHop (m_mainAddress, nodelist);
  m_errorBuffer.DropPacketForErrLink (m_mainAddress, nextHop);
  return m_routeCache->AddRoute (rt);
}

void LbsrRouting::DeleteAllRoutesIncludeLink (Ipv4Address errorSrc, Ipv4Address unreachNode, Ipv4Address node)
{
	//std::cout << "LbsrRouting::DeleteAllRoutesIncludeLink\n";

  m_routeCache->DeleteAllRoutesIncludeLink (errorSrc, unreachNode, node);
}

bool LbsrRouting::UpdateRouteEntry (Ipv4Address dst)
{
	//std::cout << "LbsrRouting::UpdateRouteEntry\n";

  return m_routeCache->UpdateRouteEntry (dst);
}

bool LbsrRouting::FindSourceEntry (Ipv4Address src, Ipv4Address dst, uint16_t id)
{
	//std::cout << "LbsrRouting::FindSourceEntry\n";

  return m_rreqTable->FindSourceEntry (src, dst, id);
}

Ipv4Address
LbsrRouting::GetIPfromMAC (Mac48Address address)
{
	//std::cout << "LbsrRouting::GetIPfromMAC\n";

  NS_LOG_FUNCTION (this << address);
  int32_t nNodes = NodeList::GetNNodes ();
  for (int32_t i = 0; i < nNodes; ++i)
    {
      Ptr<Node> node = NodeList::GetNode (i);
      Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
      Ptr<NetDevice> netDevice = ipv4->GetNetDevice (1);

      if (netDevice->GetAddress () == address)
        {
          return ipv4->GetAddress (1, 0).GetLocal ();
        }
    }
  return 0;
}

void LbsrRouting::PrintVector (std::vector<Ipv4Address>& vec)
{
	//std::cout << "LbsrRouting::PrintVector\n";

  NS_LOG_FUNCTION (this);
  /*
   * Check elements in a route vector
   */
  if (!vec.size ())
    {
      NS_LOG_DEBUG ("The vector is empty");
    }
  else
    {
      NS_LOG_DEBUG ("Print all the elements in a vector");
      for (std::vector<Ipv4Address>::const_iterator i = vec.begin (); i != vec.end (); ++i)
        {
          NS_LOG_DEBUG ("The ip address " << *i);
        }
    }
}

Ipv4Address LbsrRouting::SearchNextHop (Ipv4Address ipv4Address, std::vector<Ipv4Address>& vec)
{
	//ipv4addressは自分のアドレス
	//std::cout << "LbsrRouting::SearchNextHop\n";
	//std::cout << "ipv4Address is "<< ipv4Address <<"\n";
/*
	for (std::vector<Ipv4Address>::const_iterator i = vec.begin (); i != vec.end (); ++i)
    {
    	if(i==vec.begin()){
    		std::cout <<"vec is " << *i << " || ";
    	}else if(i == vec.end() ){
    		std::cout << *i <<"\n";
    	}else{
    	std::cout << *i << " || ";
    	}
    }
    */
	/*nextHopがわかっているということはフラッディングは終わっている？*/
  NS_LOG_FUNCTION (this << ipv4Address);
  Ipv4Address nextHop;
  NS_LOG_DEBUG ("the vector size " << vec.size ());
  if (vec.size () == 2)
    {
      NS_LOG_DEBUG ("The two nodes are neighbors");
      //std::cout << "The two nodes are neighbors\n";
      nextHop = vec[1];
      return nextHop;
    }
  else
    {
      if (ipv4Address == vec.back ())//vectorの最後尾のアドレスが自分と一致している
    	  /*つまりアドレスがsource*/
        {
          NS_LOG_DEBUG ("We have reached to the final destination " << ipv4Address << " " << vec.back ());
         // std::cout << "We have reached to the final destination " << ipv4Address << " " << vec.back () << "\n";
          nextHop=vec[1];
          return nextHop;
        }
      for (std::vector<Ipv4Address>::const_iterator i = vec.begin (); i != vec.end (); ++i)
        {
    	 // std::cout <<"vector is " << *i << "\n";
          if (ipv4Address == (*i))//キャッシュの中に自分が存在する時
            {
              nextHop = *(++i);
             // std::cout << "nexthop is " << nextHop << "\n";
              return nextHop;
            }
        }
    }
  NS_LOG_DEBUG ("Next hop address not found");
  //std::cout << "Next Hop address not found \n";
  Ipv4Address none = "0.0.0.0";
  return none;
}

//taisetu//
Ptr<Ipv4Route>
LbsrRouting::SetRoute (Ipv4Address nextHop, Ipv4Address srcAddress)
{
	//std::cout << "LbsrRouting::SetRoute\n";

  NS_LOG_FUNCTION (this << nextHop << srcAddress);
  m_ipv4Route = Create<Ipv4Route> ();
  m_ipv4Route->SetDestination (nextHop);
  m_ipv4Route->SetGateway (nextHop);
  m_ipv4Route->SetSource (srcAddress);
  return m_ipv4Route;
}

int
LbsrRouting::GetProtocolNumber (void) const
{
	//std::cout << "LbsrRouting::GetProtocolNumber\n";

  // / This is the protocol number for LBSR which is 48
  return PROT_NUMBER;
}

uint16_t
LbsrRouting::GetIDfromIP (Ipv4Address address)
{
	//std::cout << "LbsrRouting::GetIDfromIP\n";

  int32_t nNodes = NodeList::GetNNodes ();
  for (int32_t i = 0; i < nNodes; ++i)
    {
      Ptr<Node> node = NodeList::GetNode (i);
      Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
      if (ipv4->GetAddress (1, 0).GetLocal () == address)
        {
          return uint16_t (i);
        }
    }
  return 256;
}

Ipv4Address
LbsrRouting::GetIPfromID (uint16_t id)
{
	//std::cout << "LbsrRouting::GetIPfromID\n";

  if (id >= 256)
    {
      NS_LOG_DEBUG ("Exceed the node range");
      return "0.0.0.0";
    }
  else
    {
      Ptr<Node> node = NodeList::GetNode (uint32_t (id));
      Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
      return ipv4->GetAddress (1, 0).GetLocal ();
    }
}

uint32_t
LbsrRouting::GetPriority (LbsrMessageType messageType)
{
	//std::cout << "LbsrRouting::GetPriority\n";

  if (messageType == LBSR_CONTROL_PACKET)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

void LbsrRouting::SendBuffTimerExpire ()
{
	//std::cout << "LbsrRouting::SendBuffTimerExpire\n";

  if (m_sendBuffTimer.IsRunning ())
    {
      m_sendBuffTimer.Cancel ();
    }
  m_sendBuffTimer.Schedule (m_sendBuffInterval);
  CheckSendBuffer ();
}

//taisetu//
void LbsrRouting::CheckSendBuffer ()
{
	//std::cout << "LbsrRouting::CheckSendBuffer\n";

  NS_LOG_INFO (Simulator::Now ().GetSeconds ()
               << " Checking send buffer at " << m_mainAddress << " with size " << m_sendBuffer.GetSize ());
  /*std::cout << Simulator::Now ().GetSeconds ()
                 << " Checking send buffer at " << m_mainAddress << "\n";
*/
  for (std::vector<LbsrSendBuffEntry>::iterator i = m_sendBuffer.GetBuffer ().begin (); i != m_sendBuffer.GetBuffer ().end (); )
    {
	  /*iにsendBufferのデータが入っている*/
      NS_LOG_DEBUG ("Here we try to find the data packet in the send buffer");
      Ipv4Address destination = i->GetDestination ();
      //std::cout << "CheckSendBuffer destination " << destination << "\n";
      //m_sendBuffer.GetBuffer ().begin () = m_sendBuffer.GetBuffer ().end ()
      LbsrRouteCacheEntry toDst;
      bool findRoute = m_routeCache->LookupRoute (destination, toDst);
     // std::cout << "CheckSendBuffer findRoute " << findRoute <<"\n";
      if (findRoute)//ルートがあったら、つまりfindRoute=1
        {
    	  //std::cout << "CheckSendBuffer destination " << destination << "\n";
          NS_LOG_INFO ("We have found a route for the packet");
         // std::cout << "We have found a route for the packet\n";
          Ptr<const Packet> packet = i->GetPacket ();
          Ptr<Packet> cleanP = packet->Copy ();
          uint8_t protocol = i->GetProtocol ();

          i = m_sendBuffer.GetBuffer ().erase (i);

          LbsrRoutingHeader lbsrRoutingHeader;
          Ptr<Packet> copyP = packet->Copy ();
          Ptr<Packet> lbsrPacket = packet->Copy ();
          lbsrPacket->RemoveHeader (lbsrRoutingHeader);
          uint32_t offset = lbsrRoutingHeader.GetLbsrOptionsOffset ();
          copyP->RemoveAtStart (offset); // Here the processed size is 8 bytes, which is the fixed sized extension header
          // The packet to get ipv4 header
          Ptr<Packet> ipv4P = copyP->Copy ();
          /*
           * Peek data to get the option type as well as length and segmentsLeft field
           */
          uint32_t size = copyP->GetSize ();
          uint8_t *data = new uint8_t[size];
          copyP->CopyData (data, size);

          uint8_t optionType = 0;
          optionType = *(data);

          if (optionType == 3)
            {
              Ptr<lbsr::LbsrOptions> lbsrOption;
              LbsrOptionHeader lbsrOptionHeader;
              uint8_t errorType = *(data + 2);

              if (errorType == 1) // This is the Route Error Option
                {
                  LbsrOptionRerrUnreachHeader rerr;
                  copyP->RemoveHeader (rerr);
                  NS_ASSERT (copyP->GetSize () == 0);

                  LbsrOptionRerrUnreachHeader newUnreach;
                  newUnreach.SetErrorType (1);
                  newUnreach.SetErrorSrc (rerr.GetErrorSrc ());
                  newUnreach.SetUnreachNode (rerr.GetUnreachNode ());
                  newUnreach.SetErrorDst (rerr.GetErrorDst ());
                  newUnreach.SetSalvage (rerr.GetSalvage ()); // Set the value about whether to salvage a packet or not

                  LbsrOptionSRHeader sourceRoute;
                  std::vector<Ipv4Address> errorRoute = toDst.GetVector ();
                  sourceRoute.SetNodesAddress (errorRoute);
                  /// When found a route and use it, UseExtends to the link cache
                  if (m_routeCache->IsLinkCache ())
                    {
                      m_routeCache->UseExtends (errorRoute);
                    }
                  sourceRoute.SetSegmentsLeft ((errorRoute.size () - 2));
                  uint8_t salvage = 0;
                  sourceRoute.SetSalvage (salvage);
                 // std::cout << "Check to Search \n";
                  Ipv4Address nextHop = SearchNextHop (m_mainAddress, errorRoute); // Get the next hop address

                  if (nextHop == "0.0.0.0")
                    {
                      PacketNewRoute (lbsrPacket, m_mainAddress, destination, protocol);
                      return;
                    }

                  SetRoute (nextHop, m_mainAddress);
                  uint8_t length = (sourceRoute.GetLength () + newUnreach.GetLength ());
                  lbsrRoutingHeader.SetNextHeader (protocol);
                  lbsrRoutingHeader.SetMessageType (1);
                  lbsrRoutingHeader.SetSourceId (GetIDfromIP (m_mainAddress));
                  lbsrRoutingHeader.SetDestId (255);
                  lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 4);
                  lbsrRoutingHeader.AddLbsrOption (newUnreach);
                  lbsrRoutingHeader.AddLbsrOption (sourceRoute);

                  Ptr<Packet> newPacket = Create<Packet> ();
                  newPacket->AddHeader (lbsrRoutingHeader); // Add the routing header with rerr and sourceRoute attached to it
                  Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
                  m_ipv4Route->SetOutputDevice (dev);

                  uint32_t priority = GetPriority (LBSR_CONTROL_PACKET); /// This will be priority 0
                  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
                  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
                  NS_LOG_LOGIC ("Will be inserting into priority queue number: " << priority);

                  //m_downTarget (newPacket, m_mainAddress, nextHop, GetProtocolNumber (), m_ipv4Route);

                  /// \todo New LbsrNetworkQueueEntry
                  LbsrNetworkQueueEntry newEntry (newPacket, m_mainAddress, nextHop, Simulator::Now (), m_ipv4Route);

                  if (lbsrNetworkQueue->Enqueue (newEntry))
                    {
                      Scheduler (priority);
                    }
                  else
                    {
                      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
                    }
                }
            }
          else//重要ノーマル
            {
              lbsrRoutingHeader.SetNextHeader (protocol);
              lbsrRoutingHeader.SetMessageType (2);
              lbsrRoutingHeader.SetSourceId (GetIDfromIP (m_mainAddress));
              lbsrRoutingHeader.SetDestId (GetIDfromIP (destination));
              LbsrOptionSRHeader sourceRoute;
              //std::vector<Ipv4Address> nodeList = toDst.GetVector (); // Get the route from the route entry we found
              Ptr<lbsr::LbsrOptions> lbsrop;
              std::vector<Ipv4Address> nodeList = lbsrop->GetCreateRoute();
      		for (std::vector<Ipv4Address>::const_iterator i =nodeList.begin(); i != nodeList.end(); ++i) {
      			std::cout << "nodeList is" << *i << "\n";
      		}
              Ipv4Address nextHop = SearchNextHop (m_mainAddress, nodeList);  // Get the next hop address for the route
              if (nextHop == "0.0.0.0")//次のホップがなければ
                {
                  PacketNewRoute (lbsrPacket, m_mainAddress, destination, protocol);
                  return;
                }
              uint8_t salvage = 0;
              sourceRoute.SetNodesAddress (nodeList); // Save the whole route in the source route header of the packet
              sourceRoute.SetSegmentsLeft ((nodeList.size () - 2)); // The segmentsLeft field will indicate the hops to go
              sourceRoute.SetSalvage (salvage);
              /// When found a route and use it, UseExtends to the link cache
              if (m_routeCache->IsLinkCache ())
                {
                  m_routeCache->UseExtends (nodeList);//ルートをルートキャッシュに入れているのでここではもうルートができている
                }
              uint8_t length = sourceRoute.GetLength ();
              lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
              lbsrRoutingHeader.AddLbsrOption (sourceRoute);
              cleanP->AddHeader (lbsrRoutingHeader);
              Ptr<const Packet> mtP = cleanP->Copy ();
              // Put the data packet in the maintenance queue for data packet retransmission
              std::cout <<"m_mainAddress " << m_mainAddress <<", nextHop " << nextHop << ", destination " << destination << "\n";
              LbsrMaintainBuffEntry newEntry (/*Packet=*/ mtP, /*Ipv4Address=*/ m_mainAddress, /*nextHop=*/ nextHop,
                                                      /*source=*/ m_mainAddress, /*destination=*/ destination, /*ackId=*/ 0,
                                                      /*SegsLeft=*/ nodeList.size () - 2, /*expire time=*/ m_maxMaintainTime);
              bool result = m_maintainBuffer.Enqueue (newEntry); // Enqueue the packet the the maintenance buffer
              if (result)
                {
                  NetworkKey networkKey;
                  networkKey.m_ackId = newEntry.GetAckId ();
                  networkKey.m_ourAdd = newEntry.GetOurAdd ();
                  networkKey.m_nextHop = newEntry.GetNextHop ();
                  networkKey.m_source = newEntry.GetSrc ();
                  networkKey.m_destination = newEntry.GetDst ();

                  PassiveKey passiveKey;
                  passiveKey.m_ackId = 0;
                  passiveKey.m_source = newEntry.GetSrc ();
                  passiveKey.m_destination = newEntry.GetDst ();
                  passiveKey.m_segsLeft = newEntry.GetSegsLeft ();

                  LinkKey linkKey;
                  linkKey.m_source = newEntry.GetSrc ();
                  linkKey.m_destination = newEntry.GetDst ();
                  linkKey.m_ourAdd = newEntry.GetOurAdd ();
                  linkKey.m_nextHop = newEntry.GetNextHop ();

                  m_addressForwardCnt[networkKey] = 0;
                  m_passiveCnt[passiveKey] = 0;
                  m_linkCnt[linkKey] = 0;

                  if (m_linkAck)
                    {
                      ScheduleLinkPacketRetry (newEntry, protocol);
                    }
                  else
                    {
                      NS_LOG_LOGIC ("Not using link acknowledgment");
                      if (nextHop != destination)
                        {
                          SchedulePassivePacketRetry (newEntry, protocol);
                        }
                      else
                        {
                          // This is the first network retry
                          ScheduleNetworkPacketRetry (newEntry, true, protocol);
                        }
                    }
                }
              // we need to suspend the normal timer that checks the send buffer
              // until we are done sending packets
              if (!m_sendBuffTimer.IsSuspended ())
                {
                  m_sendBuffTimer.Suspend ();
                }
              EventId check = Simulator::Schedule (m_sendBuffInterval, &LbsrRouting::SendBuffTimerExpire, this);
              m_evrntIdVector.push_back(check);
              return;
            }
        }
      else
        {
          ++i;
        }
    }
  //after going through the entire send buffer and send all packets found route,
  //we need to resume the timer if it has been suspended
  if (m_sendBuffTimer.IsSuspended ())
    {
      NS_LOG_DEBUG ("Resume the send buffer timer");
      //std::cout << "Resume the send buffer timer\n";
      m_sendBuffTimer.Resume ();
    }
}

int LbsrRouting::promiscCount;
bool LbsrRouting::CreateRouteFlag;
bool LbsrRouting::SendErrorFlag;

bool LbsrRouting::PromiscReceive (Ptr<NetDevice> device, Ptr<const Packet> packet, uint16_t protocol, const Address &from,
                                 const Address &to, NetDevice::PacketType packetType)
{
	//std::cout << "LbsrRouting::PromiscReceive\n";


  if (protocol != Ipv4L3Protocol::PROT_NUMBER)
    {
      return false;
    }
  // Remove the ipv4 header here
  Ptr<Packet> pktMinusIpHdr = packet->Copy ();
  Ipv4Header ipv4Header;
  pktMinusIpHdr->RemoveHeader (ipv4Header);

  if (ipv4Header.GetProtocol () != LbsrRouting::PROT_NUMBER)
    {
	  //std::cout << "PromiscReceive error \n";
      return false;
    }
  // Remove the lbsr routing header here
  Ptr<Packet> pktMinusLbsrHdr = pktMinusIpHdr->Copy ();
  LbsrRoutingHeader lbsrRouting;
  pktMinusLbsrHdr->RemoveHeader (lbsrRouting);

  /*重要
   * Message type 2 means the data packet, we will further process the data
   * packet for delivery notification, safely ignore control packet
   * Another check here is our own address, if this is the data destinated for us,
   * process it further, otherwise, just ignore it
   */
  Ipv4Address ourAddress = m_ipv4->GetAddress (1, 0).GetLocal ();
  // check if the message type is 2 and if the ipv4 address matches
  if (lbsrRouting.GetMessageType () == 2 && ourAddress == m_mainAddress)
    {
	  SetCreateRouteFlag(true);
      NS_LOG_DEBUG ("data packet receives " << packet->GetUid ());
      Ipv4Address sourceIp = GetIPfromID (lbsrRouting.GetSourceId ());
      Ipv4Address destinationIp = GetIPfromID ( lbsrRouting.GetDestId ());
      /// This is the ip address we just received data packet from　前のアドレス(誰からパケットを受け取ったか)
      Ipv4Address previousHop = GetIPfromMAC (Mac48Address::ConvertFrom (from));

      //std::cout << "data packet receives" << packet->GetUid() << " souerce" <<sourceIp <<" destination" << destinationIp << "\n";


      Ptr<Packet> p = Create<Packet> ();
      // Here the segments left value need to plus one to check the earlier hop maintain buffer entry
      //ここで、セグメントの左の値は、以前のホップがバッファエントリを維持することを確認するために1を加える必要がある
      LbsrMaintainBuffEntry newEntry;
      newEntry.SetPacket (p);
      newEntry.SetSrc (sourceIp);
      newEntry.SetDst (destinationIp);
     // std::cout << "sourceIp " << sourceIp <<", destinationIp " << destinationIp << "\n";

      /// Remember this is the entry for previous node
      newEntry.SetOurAdd (previousHop);
      newEntry.SetNextHop (ourAddress);
      /// Get the previous node's maintenance buffer and passive ack
      Ptr<Node> node = GetNodeWithAddress (previousHop);
      NS_LOG_DEBUG ("The previous node " << previousHop);
     // std::cout << "The ournode " << ourAddress << "The previous node " << previousHop << "\n";

      Ptr<lbsr::LbsrRouting> lbsr = node->GetObject<lbsr::LbsrRouting> ();
      lbsr->CancelLinkPacketTimer (newEntry);
    }

  // Receive only IP packets and packets destined for other hosts
  if (packetType == NetDevice::PACKET_OTHERHOST)
    {
      //just to minimize debug output
      NS_LOG_INFO (this << from << to << packetType << *pktMinusIpHdr);
      //std::cout << "\nNetDevice::PACKET_OTHERHOST\n";
      uint8_t offset = lbsrRouting.GetLbsrOptionsOffset ();        // Get the offset for option header, 4 bytes in this case
      uint8_t nextHeader = lbsrRouting.GetNextHeader ();
      uint32_t sourceId = lbsrRouting.GetSourceId ();
      Ipv4Address source = GetIPfromID (sourceId);
      //std::cout << "PACKET_OTHERHOST source " << source <<"\n";

      // This packet is used to peek option type
      pktMinusIpHdr->RemoveAtStart (offset);
      /*
       * Peek data to get the option type as well as length and segmentsLeft field
       */
      uint32_t size = pktMinusIpHdr->GetSize ();
      uint8_t *data = new uint8_t[size];
      pktMinusIpHdr->CopyData (data, size);
      uint8_t optionType = 0;
      optionType = *(data);

      Ptr<lbsr::LbsrOptions> lbsrOption;

      if (optionType == 96)        // This is the source route option
        {
          Ipv4Address promiscSource = GetIPfromMAC (Mac48Address::ConvertFrom (from));
          lbsrOption = GetOption (optionType);       // Get the relative LBSR option and demux to the process function
          NS_LOG_DEBUG (Simulator::Now ().GetSeconds () <<
                        " LBSR node " << m_mainAddress <<
                        " overhearing packet PID: " << pktMinusIpHdr->GetUid () <<
                        " from " << promiscSource <<
                        " to " << GetIPfromMAC (Mac48Address::ConvertFrom (to)) <<
                        " with source IP " << ipv4Header.GetSource () <<
                        " and destination IP " << ipv4Header.GetDestination () <<
                        " and packet : " << *pktMinusLbsrHdr);

          bool isPromisc = true;                     // Set the boolean value isPromisc as true

        	  lbsrOption->Process (pktMinusIpHdr, pktMinusLbsrHdr, m_mainAddress, source, ipv4Header, nextHeader, isPromisc, promiscSource);
        	//  for(int i=0; i<(int)m_evrntIdVector.size(); i++){
        	//	  m_evrntIdVector[i].Cancel();
        		  //m_evrntIdVector.clear();
        	//  }



        	  // return true;

          return true;

        }
    }
 // std::cout << "\n";
  return false;
}

void//注目
LbsrRouting::PacketNewRoute (Ptr<Packet> packet,
                            Ipv4Address source,
                            Ipv4Address destination,
                            uint8_t protocol)
{
	//std::cout << "LbsrRouting::PacketNewRoute\n";

  NS_LOG_FUNCTION (this << packet << source << destination << (uint32_t)protocol);
  // Look up routes for the specific destination
  LbsrRouteCacheEntry toDst; //I think change here
  bool findRoute = m_routeCache->LookupRoute (destination, toDst);
  // Queue the packet if there is no route pre-existing
  if (!findRoute)
    {
      NS_LOG_INFO (Simulator::Now ().GetSeconds ()
                   << "s " << m_mainAddress << " there is no route for this packet, queue the packet");
     // std::cout << "There is not routeCache\n";

      Ptr<Packet> p = packet->Copy ();
      LbsrSendBuffEntry newEntry (p, destination, m_sendBufferTimeout, protocol);     // Create a new entry for send buffer
      bool result = m_sendBuffer.Enqueue (newEntry);     // Enqueue the packet in send buffer
      if (result)//allready result has data because newEntry is created before
        {
          NS_LOG_INFO (Simulator::Now ().GetSeconds ()
                       << "s Add packet PID: " << packet->GetUid () << " to queue. Packet: " << *packet);
        //  std::cout << Simulator::Now ().GetSeconds ()
                     //  << "s Add packet PID: " << packet->GetUid () << " to queue. Packet: " << *packet << "\n";//this is newEntry packet
          NS_LOG_LOGIC ("Send RREQ to" << destination);
          if ((m_addressReqTimer.find (destination) == m_addressReqTimer.end ()) && (m_nonPropReqTimer.find (destination) == m_nonPropReqTimer.end ()))
            {
              /*
               * Call the send request function, it will update the request table entry and ttl there
               */
              SendInitialRequest (source, destination, protocol);
            }
        }
    }
  else// there is a RouteCache
    {
      Ptr<Packet> cleanP = packet->Copy ();
      LbsrRoutingHeader lbsrRoutingHeader;
      lbsrRoutingHeader.SetNextHeader (protocol);
      lbsrRoutingHeader.SetMessageType (2);
      lbsrRoutingHeader.SetSourceId (GetIDfromIP (source));
      lbsrRoutingHeader.SetDestId (GetIDfromIP (destination));

      LbsrOptionSRHeader sourceRoute;
      std::vector<Ipv4Address> nodeList = toDst.GetVector ();     // Get the route from the route entry we found
      std::cout << "There is routeCache\n";
      for(std::size_t i=0; i<nodeList.size(); i++){
    	  std::cout << "nodeList is " << nodeList[i] << "/n";
      }
      Ipv4Address nextHop = SearchNextHop (m_mainAddress, nodeList);      // Get the next hop address for the route
      if (nextHop == "0.0.0.0")
        {
          PacketNewRoute (cleanP, source, destination, protocol);
          return;
        }
      uint8_t salvage = 0;
      sourceRoute.SetNodesAddress (nodeList);     // Save the whole route in the source route header of the packet
      /// When found a route and use it, UseExtends to the link cache
      if (m_routeCache->IsLinkCache ())
        {
          m_routeCache->UseExtends (nodeList);
        }
      sourceRoute.SetSegmentsLeft ((nodeList.size () - 2));     // The segmentsLeft field will indicate the hops to go
      sourceRoute.SetSalvage (salvage);

      uint8_t length = sourceRoute.GetLength ();
      lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
      lbsrRoutingHeader.AddLbsrOption (sourceRoute);
      cleanP->AddHeader (lbsrRoutingHeader);
      Ptr<const Packet> mtP = cleanP->Copy ();
      SetRoute (nextHop, m_mainAddress);
      // Put the data packet in the maintenance queue for data packet retransmission
      LbsrMaintainBuffEntry newEntry (/*Packet=*/ mtP, /*Ipv4Address=*/ m_mainAddress, /*nextHop=*/ nextHop,
                                              /*source=*/ source, /*destination=*/ destination, /*ackId=*/ 0,
                                              /*SegsLeft=*/ nodeList.size () - 2, /*expire time=*/ m_maxMaintainTime);
      bool result = m_maintainBuffer.Enqueue (newEntry);     // Enqueue the packet the the maintenance buffer

      if (result)
        {
          NetworkKey networkKey;
          networkKey.m_ackId = newEntry.GetAckId ();
          networkKey.m_ourAdd = newEntry.GetOurAdd ();
          networkKey.m_nextHop = newEntry.GetNextHop ();
          networkKey.m_source = newEntry.GetSrc ();
          networkKey.m_destination = newEntry.GetDst ();

          PassiveKey passiveKey;
          passiveKey.m_ackId = 0;
          passiveKey.m_source = newEntry.GetSrc ();
          passiveKey.m_destination = newEntry.GetDst ();
          passiveKey.m_segsLeft = newEntry.GetSegsLeft ();

          LinkKey linkKey;
          linkKey.m_source = newEntry.GetSrc ();
          linkKey.m_destination = newEntry.GetDst ();
          linkKey.m_ourAdd = newEntry.GetOurAdd ();
          linkKey.m_nextHop = newEntry.GetNextHop ();

          m_addressForwardCnt[networkKey] = 0;
          m_passiveCnt[passiveKey] = 0;
          m_linkCnt[linkKey] = 0;

          if (m_linkAck)
            {
              ScheduleLinkPacketRetry (newEntry, protocol);
            }
          else
            {
              NS_LOG_LOGIC ("Not using link acknowledgment");
              if (nextHop != destination)
                {
                  SchedulePassivePacketRetry (newEntry, protocol);
                }
              else
                {
                  // This is the first network retry
                  ScheduleNetworkPacketRetry (newEntry, true, protocol);
                }
            }
        }
    }
}

void
LbsrRouting::SendUnreachError (Ipv4Address unreachNode, Ipv4Address destination, Ipv4Address originalDst, uint8_t salvage, uint8_t protocol)
{
	std::cout << "\nLbsrRouting::SendUnreachError\n";

  NS_LOG_FUNCTION (this << unreachNode << destination << originalDst << (uint32_t)salvage << (uint32_t)protocol);
  destination = "10.1.1.1";
  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrRoutingHeader.SetNextHeader (protocol);
  lbsrRoutingHeader.SetMessageType (1);
  lbsrRoutingHeader.SetSourceId (GetIDfromIP (m_mainAddress));
  lbsrRoutingHeader.SetDestId (GetIDfromIP (destination));
  LbsrOptionRerrUnreachHeader rerrUnreachHeader;
  rerrUnreachHeader.SetErrorType (1);
  rerrUnreachHeader.SetErrorSrc (m_mainAddress);
  rerrUnreachHeader.SetUnreachNode (unreachNode);
  rerrUnreachHeader.SetErrorDst (destination);
  rerrUnreachHeader.SetOriginalDst (originalDst);
  rerrUnreachHeader.SetSalvage (salvage);                       // Set the value about whether to salvage a packet or not
  uint8_t rerrLength = rerrUnreachHeader.GetLength ();


  LbsrRouteCacheEntry toDst;
  bool findRoute = m_routeCache->LookupRoute (destination, toDst);
  // Queue the packet if there is no route pre-existing
  Ptr<Packet> newPacket = Create<Packet> ();
  if (!findRoute)//目的地までのルートを持っていない
    {
	  /*
	  bool flag = GetSendErrorFlag();
	  if(flag == false){
		  SetSendErrorFlag(true);
		  EventId ErrorFlagId = Simulator::Schedule(Seconds(10), &LbsrRouting::SetSendErrorFlag, this, false);
		  m_evrntIdVector.push_back(ErrorFlagId);
	  }else{
		  SetCreateRouteFlag(false);
		  CheckCreateRouteFlag(m_mainAddress, originalDst, protocol);//ここのsourceを変更したい
	  }
	  */
      if (destination == m_mainAddress)//自分自身が目的地になっている
        {
          NS_LOG_INFO ("We are the error source, send request to original dst " << originalDst);
          // Send error request message if we are the source node
          SendErrorRequest (rerrUnreachHeader, protocol);
        }
      else
        {
          NS_LOG_INFO (Simulator::Now ().GetSeconds ()
                       << "s " << m_mainAddress << " there is no route for this packet, queue the packet");

          lbsrRoutingHeader.SetPayloadLength (rerrLength + 2);
          lbsrRoutingHeader.AddLbsrOption (rerrUnreachHeader);
          newPacket->AddHeader (lbsrRoutingHeader);
          Ptr<Packet> p = newPacket->Copy ();
          // Save the error packet in the error buffer
          LbsrErrorBuffEntry newEntry (p, destination, m_mainAddress, unreachNode, m_sendBufferTimeout, protocol);
          bool result = m_errorBuffer.Enqueue (newEntry);                  // Enqueue the packet in send buffer
          if (result)
            {
              NS_LOG_INFO (Simulator::Now ().GetSeconds ()
                           << "s Add packet PID: " << p->GetUid () << " to queue. Packet: " << *p);
              NS_LOG_LOGIC ("Send RREQ to" << destination);
              if ((m_addressReqTimer.find (destination) == m_addressReqTimer.end ()) && (m_nonPropReqTimer.find (destination) == m_nonPropReqTimer.end ()))
                {
                  NS_LOG_DEBUG ("When there is no existing route request for " << destination << ", initialize one");
                  /*
                   * Call the send request function, it will update the request table entry and ttl there
                   */
                  bool flag = GetSendErrorFlag();
            	  if(flag == false){
            		  SetSendErrorFlag(true);
            		  EventId ErrorFlagId = Simulator::Schedule(Seconds(10), &LbsrRouting::SetSendErrorFlag, this, false);
            		  m_evrntIdVector.push_back(ErrorFlagId);
            	  }else{
            		  SetCreateRouteFlag(false);
            		  CheckCreateRouteFlag(m_mainAddress, originalDst, protocol);//ここのsourceを変更したい
            	  }
                  SendInitialRequest (m_mainAddress, destination, protocol);
                }
            }
        }
    }
  else//目的地までのルートを持っている
    {
      std::vector<Ipv4Address> nodeList = toDst.GetVector ();
      Ipv4Address nextHop = SearchNextHop (m_mainAddress, nodeList);
      if (nextHop == "0.0.0.0")
        {
          NS_LOG_DEBUG ("The route is not right");
          PacketNewRoute (newPacket, m_mainAddress, destination, protocol);
          return;
        }
      LbsrOptionSRHeader sourceRoute;
      sourceRoute.SetNodesAddress (nodeList);
      /// When found a route and use it, UseExtends to the link cache
      if (m_routeCache->IsLinkCache ())
        {
          m_routeCache->UseExtends (nodeList);
        }
      sourceRoute.SetSegmentsLeft ((nodeList.size () - 2));
      uint8_t srLength = sourceRoute.GetLength ();
      uint8_t length = (srLength + rerrLength);

      lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 4);
      lbsrRoutingHeader.AddLbsrOption (rerrUnreachHeader);
      lbsrRoutingHeader.AddLbsrOption (sourceRoute);
      newPacket->AddHeader (lbsrRoutingHeader);

      SetRoute (nextHop, m_mainAddress);
      Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
      m_ipv4Route->SetOutputDevice (dev);
      NS_LOG_INFO ("Send the packet to the next hop address " << nextHop << " from " << m_mainAddress << " with the size " << newPacket->GetSize ());

      uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);
      std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
      Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
      NS_LOG_DEBUG ("Will be inserting into priority queue " << lbsrNetworkQueue << " number: " << priority);

      //m_downTarget (newPacket, m_mainAddress, nextHop, GetProtocolNumber (), m_ipv4Route);

      /// \todo New LbsrNetworkQueueEntry
      LbsrNetworkQueueEntry newEntry (newPacket, m_mainAddress, nextHop, Simulator::Now (), m_ipv4Route);

      if (lbsrNetworkQueue->Enqueue (newEntry))
        {
          Scheduler (priority);
        }
      else
        {
          NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
        }
    }
}

void
LbsrRouting::ForwardErrPacket (LbsrOptionRerrUnreachHeader &rerr,
                              LbsrOptionSRHeader &sourceRoute,
                              Ipv4Address nextHop,
                              uint8_t protocol,
                              Ptr<Ipv4Route> route)
{
	//std::cout << "LbsrRouting::ForwardErrPacket\n";

  NS_LOG_FUNCTION (this << rerr << sourceRoute << nextHop << (uint32_t)protocol << route);
  //std::cout << this << rerr << sourceRoute << nextHop << (uint32_t)protocol << route;
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");
  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrRoutingHeader.SetNextHeader (protocol);
  lbsrRoutingHeader.SetMessageType (1);
  lbsrRoutingHeader.SetSourceId (GetIDfromIP (rerr.GetErrorSrc ()));
  lbsrRoutingHeader.SetDestId (GetIDfromIP (rerr.GetErrorDst ()));

  uint8_t length = (sourceRoute.GetLength () + rerr.GetLength ());
  lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 4);
  lbsrRoutingHeader.AddLbsrOption (rerr);
  lbsrRoutingHeader.AddLbsrOption (sourceRoute);
  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (lbsrRoutingHeader);
  Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
  route->SetOutputDevice (dev);

  uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);
  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
  NS_LOG_DEBUG ("Will be inserting into priority queue " << lbsrNetworkQueue << " number: " << priority);

  //m_downTarget (packet, m_mainAddress, nextHop, GetProtocolNumber (), route);

  /// \todo New LbsrNetworkQueueEntry
  LbsrNetworkQueueEntry newEntry (packet, m_mainAddress, nextHop, Simulator::Now (), route);

  if (lbsrNetworkQueue->Enqueue (newEntry))
    {
      Scheduler (priority);
    }
  else
    {
      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
    }
}


//here is send
void
LbsrRouting::Send (Ptr<Packet> packet,
                  Ipv4Address source,
                  Ipv4Address destination,
                  uint8_t protocol,
                  Ptr<Ipv4Route> route)
{
  //std::cout << "LbsrRouting::Send sourc\n";
//ネットワークの確認のためにack要求ヘッダーを追加する
  NS_LOG_FUNCTION (this << packet << source << destination << (uint32_t)protocol << route);
  //std::cout << this << " " << packet << " " << source << " " << destination << " " << (uint32_t)protocol << " " << route << "\n";
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");

  if (protocol == 1)
    {
      NS_LOG_INFO ("Drop packet. Not handling ICMP packet for now");//よくわからんがTCP/ipができないよ
    }
  else
    {
      // Look up routes for the specific destination
      LbsrRouteCacheEntry toDst; //目的地までのルート
      bool findRoute = m_routeCache->LookupRoute (destination, toDst);
     // std::cout << "Send destination is " << destination <<"\n";
      /*ルートを探そうとしている、LookupRoute()でルートキャッシュの中身を見ている あったら1,無かったら0*/
     // std::cout << "findRoute --" << findRoute << "\n";
      // Queue the packet if there is no route pre-existing
      if (!findRoute)//ルートキャッシュに目的地までのルートが入っていない、一番最初の通信はここに来る
        {
          NS_LOG_INFO (Simulator::Now ().GetSeconds ()
                       << "s " << m_mainAddress << " there is no route for this packet, queue the packet");
         // std::cout <<"Send first " << Simulator::Now ().GetSeconds ()
         //                        << "s " << m_mainAddress << " there is no route for this packet, queue the packet\n";

          Ptr<Packet> p = packet->Copy ();//パケットをコピー、バッファーに入れるため
          //std::cout << "Send Packet is " << *p << "\n";
          LbsrSendBuffEntry newEntry (p, destination, m_sendBufferTimeout, protocol);     // 新しいsend bufferを作る、まだここでは空
          bool result = m_sendBuffer.Enqueue (newEntry);     // Enqueue the packet in send buffer
          if (result)
            {
              NS_LOG_INFO (Simulator::Now ().GetSeconds ()
                           << "s Add packet PID: " << packet->GetUid () << " to send buffer. Packet: " << *packet);
          /*    std:: cout << Simulator::Now ().GetSeconds ()
                           << "s Add packet PID: " << packet->GetUid () << " to send buffer. Packet: " << *packet << "\n";
            */
              // Only when there is no existing route request timer when new route request is scheduled
                //新しいルートリクエストでルートリクエストタイマが存在しなければ、一番最初はここに来そう
              if ((m_addressReqTimer.find (destination) == m_addressReqTimer.end ()) && (m_nonPropReqTimer.find (destination) == m_nonPropReqTimer.end ()))
                {
            	  //重要　ここはtimerに関するものなので変更しても意味ない
            	  /*
            	   * ------------------------
            	   * |_ id_____|_timer_|
            	   * | source  |  100s |
            	   * | des     |  200s |
            	   * | system  |  300s |
            	   * -----------------------
            	   * こんな感じ
            	   */
                  /*
                   * Call the send request function, it will update the request table entry and ttl value
                   */
                  NS_LOG_LOGIC ("Send initial RREQ to " << destination);
                  //std::cout << "Send initial RREQ to " << destination << " from(source) "<< source << "\n";
                  //ここでリクエストテーブルとttl値の更新が行われる
                  SendInitialRequest (source, destination, protocol);
                }
              else //前回のルート構築のときにルートが作成されててもう持っている
                {
                  NS_LOG_LOGIC ("There is existing route request timer with request count " << m_rreqTable->GetRreqCnt (destination));
                }
            }
        }
      else //ルートキャッシュにルートが存在するとき ここがメインデータの送信
        {
          Ptr<Packet> cleanP = packet->Copy ();
          LbsrRoutingHeader lbsrRoutingHeader;
          lbsrRoutingHeader.SetNextHeader (protocol);
          lbsrRoutingHeader.SetMessageType (2);//ルートが存在している時の通信なので多分ユニキャストの通信ではないかな
          lbsrRoutingHeader.SetSourceId (GetIDfromIP (source));
          lbsrRoutingHeader.SetDestId (GetIDfromIP (destination));
          //std::cout << "Send protocol " << (uint32_t)protocol << " source " << source << " destination " << destination <<  "\n";

          LbsrOptionSRHeader sourceRoute;
          std::vector<Ipv4Address> nodeList = toDst.GetVector ();// Get the route from the route entry we found　探索したルートをnodeListに入れてる
    /*      for(std::size_t i=0; i<nodeList.size(); i++){
        	  if(i==0){
        		  std::cout << "\nsource is       "<< nodeList[i] << "\n";
        	  }else if(i== nodeList.size() -1 ){
        		  std::cout << "desatination is "<< nodeList[i] << "\n\n";
        	  }else{
        		  std::cout << "nodeList is     "<< nodeList[i] << "\n"; //重要::ここでルートはできている、つまりtoDsrにルートがあってそれをGetVectorで取得できる
        	  }
          }
          */
          Ipv4Address nextHop = SearchNextHop (m_mainAddress, nodeList);        // Get the next hop address for the route　nodelistから次に送るノードを見る
          //std::cout << "nextHop" << nextHop << "\n";
          if (nextHop == "0.0.0.0")
            {
              PacketNewRoute (cleanP, source, destination, protocol);
              return;
            }
          uint8_t salvage = 0;
          sourceRoute.SetNodesAddress (nodeList);       // Save the whole route in the source route header of the packet　すべてのルートを保存する
          /// When found a route and use it, UseExtends to the link cache
          if (m_routeCache->IsLinkCache ())//これ確定で真じゃね
            {
              m_routeCache->UseExtends (nodeList);
            }
          sourceRoute.SetSegmentsLeft ((nodeList.size () - 2));       // The segmentsLeft field will indicate the hops to go
          sourceRoute.SetSalvage (salvage);//salvage = 0

          uint8_t length = sourceRoute.GetLength ();//ルートのノード数
          //std::cout << "length" << double(length) << "\n";

          lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
          lbsrRoutingHeader.AddLbsrOption (sourceRoute);
          cleanP->AddHeader (lbsrRoutingHeader);//もともとのパケットに上記のデータを追加したものをcleanPに入れてる

          Ptr<const Packet> mtP = cleanP->Copy ();
          NS_LOG_DEBUG ("maintain packet size " << cleanP->GetSize ());
          // Put the data packet in the maintenance queue for data packet retransmission　データの再送信のためにメンテナンスキューにパケットをセットする
          LbsrMaintainBuffEntry newEntry (/*Packet=*/ mtP, /*ourAddress=*/ m_mainAddress, /*nextHop=*/ nextHop,
                                                  /*source=*/ source, /*destination=*/ destination, /*ackId=*/ 0,
                                                  /*SegsLeft=*/ nodeList.size () - 2, /*expire time=*/ m_maxMaintainTime);
          bool result = m_maintainBuffer.Enqueue (newEntry);       // Enqueue the packet the maintenance buffer　メンテナンスバッファーのパケットを出す
          if (result)
            {//それぞれのkeyにデータを挿入する
              NetworkKey networkKey;
              networkKey.m_ackId = newEntry.GetAckId ();
              networkKey.m_ourAdd = newEntry.GetOurAdd ();
              networkKey.m_nextHop = newEntry.GetNextHop ();
              networkKey.m_source = newEntry.GetSrc ();
              networkKey.m_destination = newEntry.GetDst ();

              PassiveKey passiveKey;
              passiveKey.m_ackId = 0;
              passiveKey.m_source = newEntry.GetSrc ();
              passiveKey.m_destination = newEntry.GetDst ();
              passiveKey.m_segsLeft = newEntry.GetSegsLeft ();

              LinkKey linkKey;
              linkKey.m_source = newEntry.GetSrc ();
              linkKey.m_destination = newEntry.GetDst ();
              linkKey.m_ourAdd = newEntry.GetOurAdd ();
              linkKey.m_nextHop = newEntry.GetNextHop ();

              m_addressForwardCnt[networkKey] = 0;
              m_passiveCnt[passiveKey] = 0;
              m_linkCnt[linkKey] = 0;

              if (m_linkAck)
                {
                  ScheduleLinkPacketRetry (newEntry, protocol);
                }
              else
                {
                  NS_LOG_LOGIC ("Not using link acknowledgment");
                  if (nextHop != destination)
                    {
                      SchedulePassivePacketRetry (newEntry, protocol);
                    }
                  else
                    {
                      // This is the first network retry
                      ScheduleNetworkPacketRetry (newEntry, true, protocol);
                    }
                }
            }

          if (m_sendBuffer.GetSize () != 0 && m_sendBuffer.Find (destination)) //ルートが探索できたら
            {
              // Try to send packet from *previously* queued entries from send buffer if any
              EventId sendId = Simulator::Schedule (MilliSeconds (m_uniformRandomVariable->GetInteger (0,100)),
                                   &LbsrRouting::SendPacketFromBuffer, this, sourceRoute, nextHop, protocol);
              m_evrntIdVector.push_back(sendId);
            }
        }
    }
}

uint16_t
LbsrRouting::AddAckReqHeader (Ptr<Packet>& packet, Ipv4Address nextHop)
{
	//std::cout << "LbsrRouting::AddAckReqHeader\n";

  NS_LOG_FUNCTION (this << packet << nextHop);
  // This packet is used to peek option type
  Ptr<Packet> lbsrP = packet->Copy ();
  Ptr<Packet> tmpP = packet->Copy ();

  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrP->RemoveHeader (lbsrRoutingHeader);          // Remove the LBSR header in whole
  uint8_t protocol = lbsrRoutingHeader.GetNextHeader ();
  uint32_t sourceId = lbsrRoutingHeader.GetSourceId ();
  uint32_t destinationId = lbsrRoutingHeader.GetDestId ();
  uint32_t offset = lbsrRoutingHeader.GetLbsrOptionsOffset ();
  tmpP->RemoveAtStart (offset);       // Here the processed size is 8 bytes, which is the fixed sized extension header

  // Get the number of routers' address field
  uint8_t buf[2];
  tmpP->CopyData (buf, sizeof(buf));
  uint8_t numberAddress = (buf[1] - 2) / 4;
  LbsrOptionSRHeader sourceRoute;
  sourceRoute.SetNumberAddress (numberAddress);
  tmpP->RemoveHeader (sourceRoute);               // this is a clean packet without any lbsr involved headers

  LbsrOptionAckReqHeader ackReq;
  m_ackId = m_routeCache->CheckUniqueAckId (nextHop);
  ackReq.SetAckId (m_ackId);
  uint8_t length = (sourceRoute.GetLength () + ackReq.GetLength ());
  LbsrRoutingHeader newLbsrRoutingHeader;
  newLbsrRoutingHeader.SetNextHeader (protocol);
  newLbsrRoutingHeader.SetMessageType (2);
  newLbsrRoutingHeader.SetSourceId (sourceId);
  newLbsrRoutingHeader.SetDestId (destinationId);
  newLbsrRoutingHeader.SetPayloadLength (length + 4);
  newLbsrRoutingHeader.AddLbsrOption (sourceRoute);
  newLbsrRoutingHeader.AddLbsrOption (ackReq);
  lbsrP->AddHeader (newLbsrRoutingHeader);
  // give the lbsrP value to packet and then return
  packet = lbsrP;
  return m_ackId;
}

void
LbsrRouting::SendPacket (Ptr<Packet> packet, Ipv4Address source, Ipv4Address nextHop, uint8_t protocol)
{
	//std::cout << "LbsrRouting::SendPacket\n";

  NS_LOG_FUNCTION (this << packet << source << nextHop << (uint32_t)protocol);
  // Send out the data packet
  m_ipv4Route = SetRoute (nextHop, m_mainAddress);
  Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
  m_ipv4Route->SetOutputDevice (dev);

  uint32_t priority = GetPriority (LBSR_DATA_PACKET);
  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
  NS_LOG_INFO ("Will be inserting into priority queue number: " << priority);

  //m_downTarget (packet, source, nextHop, GetProtocolNumber (), m_ipv4Route);

  /// \todo New LbsrNetworkQueueEntry
  LbsrNetworkQueueEntry newEntry (packet, source, nextHop, Simulator::Now (), m_ipv4Route);

  if (lbsrNetworkQueue->Enqueue (newEntry))
    {
      Scheduler (priority);
    }
  else
    {
      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
    }
}

void
LbsrRouting::Scheduler (uint32_t priority)
{
	//std::cout << "LbsrRouting::Scheduler\n";

  NS_LOG_FUNCTION (this);
  PriorityScheduler (priority, true);
}

void
LbsrRouting::PriorityScheduler (uint32_t priority, bool continueWithFirst)
{
	//std::cout << "LbsrRouting::PriorityScheduler\n";
	//この関数は、ネットワークキュー内のデータパケットの再送信タイマーを増やすために呼び出されます


  NS_LOG_FUNCTION (this << priority << continueWithFirst);
  //std::cout << "continueWithFirst " << continueWithFirst << "\n";
  uint32_t numPriorities;
  if (continueWithFirst)
    {
      numPriorities = 0;
    }
  else
    {
      numPriorities = priority;
    }
  // priorities ranging from 0 to m_numPriorityQueues, with 0 as the highest priority
  //std::cout << "m_numPriorityQueues " << m_numPriorityQueues << "\n";
  for (uint32_t i = priority; numPriorities < m_numPriorityQueues; numPriorities++)
    {
      std::map<uint32_t, Ptr<LbsrNetworkQueue> >::iterator q = m_priorityQueue.find (i);
      Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = q->second;
      uint32_t queueSize = lbsrNetworkQueue->GetSize ();
      //std::cout << "queueSize " << queueSize << "\n";
      if (queueSize == 0)
        {
          if ((i == (m_numPriorityQueues - 1)) && continueWithFirst)
            {
              i = 0;
            }
          else
            {
              i++;
            }
        }
      else
        {
          uint32_t totalQueueSize = 0;
          for (std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator j = m_priorityQueue.begin (); j != m_priorityQueue.end (); j++)
            {
              NS_LOG_INFO ("The size of the network queue for " << j->first << " is " << j->second->GetSize ());
           //   std::cout << "The size of the network queue for" << j->first << " is " << j->second->GetSize () << "\n";
              totalQueueSize += j->second->GetSize ();
              NS_LOG_INFO ("The total network queue size is " << totalQueueSize);
            }
          if (totalQueueSize > 5)
            {
              // Here the queue size is larger than 5, we need to increase the retransmission timer for each packet in the network queue
              IncreaseRetransTimer ();
            }
          LbsrNetworkQueueEntry newEntry;
          lbsrNetworkQueue->Dequeue (newEntry);
          if (SendRealDown (newEntry))
            {
              NS_LOG_LOGIC ("Packet sent by Lbsr. Calling PriorityScheduler after some time");
              // packet was successfully sent down. call scheduler after some time
             // std::cout << "Packet sent \n";
             EventId prioEvent =  Simulator::Schedule (MicroSeconds (m_uniformRandomVariable->GetInteger (0, 1000)),
                                   &LbsrRouting::PriorityScheduler,this, i, false);
             m_evrntIdVector.push_back(prioEvent);
            }
          else
            {
              // packet was dropped by Lbsr. Call scheduler immediately so that we can
              // send another packet immediately.
              NS_LOG_LOGIC ("Packet dropped by Lbsr. Calling PriorityScheduler immediately");
              EventId prioEvent = Simulator::Schedule (Seconds (0), &LbsrRouting::PriorityScheduler, this, i, false);
              m_evrntIdVector.push_back(prioEvent);
            }

          if ((i == (m_numPriorityQueues - 1)) && continueWithFirst)
            {
              i = 0;
            }
          else
            {
              i++;
            }
        }
    }
}

void
LbsrRouting::IncreaseRetransTimer ()
{
	//std::cout << "LbsrRouting::IncreaseRetransTimer\n";

  NS_LOG_FUNCTION (this);
  // We may want to get the queue first and then we need to save a vector of the entries here and then find
  uint32_t priority = GetPriority (LBSR_DATA_PACKET);
  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;

  std::vector<LbsrNetworkQueueEntry> newNetworkQueue = lbsrNetworkQueue->GetQueue ();
  for (std::vector<LbsrNetworkQueueEntry>::iterator i = newNetworkQueue.begin (); i != newNetworkQueue.end (); i++)
    {
      Ipv4Address nextHop = i->GetNextHopAddress ();
      for (std::map<NetworkKey, Timer>::iterator j = m_addressForwardTimer.begin (); j != m_addressForwardTimer.end (); j++)
        {
          if (nextHop == j->first.m_nextHop)
            {
              NS_LOG_DEBUG ("The network delay left is " << j->second.GetDelayLeft ());
              j->second.SetDelay (j->second.GetDelayLeft () + m_retransIncr);
            }
        }
    }
}

bool
LbsrRouting::SendRealDown (LbsrNetworkQueueEntry & newEntry)
{
	//std::cout << "LbsrRouting::SendRealDown\n";

  NS_LOG_FUNCTION (this);
  Ipv4Address source = newEntry.GetSourceAddress ();
  Ipv4Address nextHop = newEntry.GetNextHopAddress ();
  Ptr<Packet> packet = newEntry.GetPacket ()->Copy ();
  Ptr<Ipv4Route> route = newEntry.GetIpv4Route ();
  m_downTarget (packet, source, nextHop, GetProtocolNumber (), route);
  return true;
}

void
LbsrRouting::SendPacketFromBuffer (LbsrOptionSRHeader const &sourceRoute, Ipv4Address nextHop, uint8_t protocol)
{
	//std::cout << "LbsrRouting::SendPacketFromBuffer\n";

  NS_LOG_FUNCTION (this << nextHop << (uint32_t)protocol);
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");

  // Reconstruct the route and Retransmit the data packet
  std::vector<Ipv4Address> nodeList = sourceRoute.GetNodesAddress ();
  //Ipv4Address destination = nodeList.back ();
  Ipv4Address destination = "10.1.1.1";
  Ipv4Address source = nodeList.front ();       // Get the source address
  NS_LOG_INFO ("The nexthop address " << nextHop << " the source " << source << " the destination " << destination);
  /*
   * Here we try to find data packet from send buffer, if packet with this destination found, send it out
   */
  if (m_sendBuffer.Find (destination))
    {
      NS_LOG_DEBUG ("destination over here " << destination);

      /// When found a route and use it, UseExtends to the link cache
      if (m_routeCache->IsLinkCache ())
        {
          m_routeCache->UseExtends (nodeList);
        }
      LbsrSendBuffEntry entry;
      if (m_sendBuffer.Dequeue (destination, entry))
        {
          Ptr<Packet> packet = entry.GetPacket ()->Copy ();
          Ptr<Packet> p = packet->Copy ();      // get a copy of the packet
          // Set the source route option
          LbsrRoutingHeader lbsrRoutingHeader;
          lbsrRoutingHeader.SetNextHeader (protocol);
          lbsrRoutingHeader.SetMessageType (2);
          lbsrRoutingHeader.SetSourceId (GetIDfromIP (source));
          lbsrRoutingHeader.SetDestId (GetIDfromIP (destination));
         // std::cout << "destination is " << destination << "\n";
          uint8_t length = sourceRoute.GetLength ();
          lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
          lbsrRoutingHeader.AddLbsrOption (sourceRoute);

          p->AddHeader (lbsrRoutingHeader);

          Ptr<const Packet> mtP = p->Copy ();
          // Put the data packet in the maintenance queue for data packet retransmission
          LbsrMaintainBuffEntry newEntry (/*Packet=*/ mtP, /*ourAddress=*/ m_mainAddress, /*nextHop=*/ nextHop,
                                      /*source=*/ source, /*destination=*/ destination, /*ackId=*/ 0,
                                      /*SegsLeft=*/ nodeList.size () - 2, /*expire time=*/ m_maxMaintainTime);
          bool result = m_maintainBuffer.Enqueue (newEntry);       // Enqueue the packet the the maintenance buffer

          if (result)
            {
              NetworkKey networkKey;
              networkKey.m_ackId = newEntry.GetAckId ();
              networkKey.m_ourAdd = newEntry.GetOurAdd ();
              networkKey.m_nextHop = newEntry.GetNextHop ();
              networkKey.m_source = newEntry.GetSrc ();
              networkKey.m_destination = newEntry.GetDst ();

              PassiveKey passiveKey;
              passiveKey.m_ackId = 0;
              passiveKey.m_source = newEntry.GetSrc ();
              passiveKey.m_destination = newEntry.GetDst ();
              passiveKey.m_segsLeft = newEntry.GetSegsLeft ();

              LinkKey linkKey;
              linkKey.m_source = newEntry.GetSrc ();
              linkKey.m_destination = newEntry.GetDst ();
              linkKey.m_ourAdd = newEntry.GetOurAdd ();
              linkKey.m_nextHop = newEntry.GetNextHop ();

              m_addressForwardCnt[networkKey] = 0;
              m_passiveCnt[passiveKey] = 0;
              m_linkCnt[linkKey] = 0;

              if (m_linkAck)
                {
                  ScheduleLinkPacketRetry (newEntry, protocol);
                }
              else
                {
                  NS_LOG_LOGIC ("Not using link acknowledgment");
                  if (nextHop != destination)
                    {
                      SchedulePassivePacketRetry (newEntry, protocol);
                    }
                  else
                    {
                      // This is the first network retry
                      ScheduleNetworkPacketRetry (newEntry, true, protocol);
                    }
                }
            }

          NS_LOG_DEBUG ("send buffer size here and the destination " << m_sendBuffer.GetSize () << " " << destination);
          if (m_sendBuffer.GetSize () != 0 && m_sendBuffer.Find (destination))
            {
              NS_LOG_LOGIC ("Schedule sending the next packet in send buffer");
             EventId sendPac =  Simulator::Schedule (MilliSeconds (m_uniformRandomVariable->GetInteger (0,100)),
                                   &LbsrRouting::SendPacketFromBuffer, this, sourceRoute, nextHop, protocol);
             m_evrntIdVector.push_back(sendPac);
            }
        }
      else
        {
          NS_LOG_LOGIC ("All queued packets are out-dated for the destination in send buffer");
        }
    }
  /*
   * Here we try to find data packet from send buffer, if packet with this destination found, send it out
   */
  else if (m_errorBuffer.Find (destination))
    {
      LbsrErrorBuffEntry entry;
      if (m_errorBuffer.Dequeue (destination, entry))
        {
          Ptr<Packet> packet = entry.GetPacket ()->Copy ();
          NS_LOG_DEBUG ("The queued packet size " << packet->GetSize ());

          LbsrRoutingHeader lbsrRoutingHeader;
          Ptr<Packet> copyP = packet->Copy ();
          Ptr<Packet> lbsrPacket = packet->Copy ();
          lbsrPacket->RemoveHeader (lbsrRoutingHeader);
          uint32_t offset = lbsrRoutingHeader.GetLbsrOptionsOffset ();
          copyP->RemoveAtStart (offset);       // Here the processed size is 8 bytes, which is the fixed sized extension header
          /*
           * Peek data to get the option type as well as length and segmentsLeft field
           */
          uint32_t size = copyP->GetSize ();
          uint8_t *data = new uint8_t[size];
          copyP->CopyData (data, size);

          uint8_t optionType = 0;
          optionType = *(data);
          NS_LOG_DEBUG ("The option type value in send packet " << (uint32_t)optionType);
          if (optionType == 3)
            {
              NS_LOG_DEBUG ("The packet is error packet");
              Ptr<lbsr::LbsrOptions> lbsrOption;
              LbsrOptionHeader lbsrOptionHeader;

              uint8_t errorType = *(data + 2);
              NS_LOG_DEBUG ("The error type");
              if (errorType == 1)
                {
                  NS_LOG_DEBUG ("The packet is route error unreach packet");
                  LbsrOptionRerrUnreachHeader rerr;
                  copyP->RemoveHeader (rerr);
                  NS_ASSERT (copyP->GetSize () == 0);
                  uint8_t length = (sourceRoute.GetLength () + rerr.GetLength ());

                  LbsrOptionRerrUnreachHeader newUnreach;
                  newUnreach.SetErrorType (1);
                  newUnreach.SetErrorSrc (rerr.GetErrorSrc ());
                  newUnreach.SetUnreachNode (rerr.GetUnreachNode ());
                  newUnreach.SetErrorDst (rerr.GetErrorDst ());
                  newUnreach.SetOriginalDst (rerr.GetOriginalDst ());
                  newUnreach.SetSalvage (rerr.GetSalvage ());       // Set the value about whether to salvage a packet or not

                  std::vector<Ipv4Address> nodeList = sourceRoute.GetNodesAddress ();
                  LbsrRoutingHeader newRoutingHeader;
                  newRoutingHeader.SetNextHeader (protocol);
                  newRoutingHeader.SetMessageType (1);
                  newRoutingHeader.SetSourceId (GetIDfromIP (rerr.GetErrorSrc ()));
                  newRoutingHeader.SetDestId (GetIDfromIP (rerr.GetErrorDst ()));
                  newRoutingHeader.SetPayloadLength (uint16_t (length) + 4);
                  newRoutingHeader.AddLbsrOption (newUnreach);
                  newRoutingHeader.AddLbsrOption (sourceRoute);
                  /// When found a route and use it, UseExtends to the link cache
                  if (m_routeCache->IsLinkCache ())
                    {
                      m_routeCache->UseExtends (nodeList);
                    }
                  SetRoute (nextHop, m_mainAddress);
                  Ptr<Packet> newPacket = Create<Packet> ();
                  newPacket->AddHeader (newRoutingHeader);       // Add the extension header with rerr and sourceRoute attached to it
                  Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
                  m_ipv4Route->SetOutputDevice (dev);

                  uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);
                  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
                  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
                  NS_LOG_DEBUG ("Will be inserting into priority queue " << lbsrNetworkQueue << " number: " << priority);

                  //m_downTarget (newPacket, m_mainAddress, nextHop, GetProtocolNumber (), m_ipv4Route);

                  /// \todo New LbsrNetworkQueueEntry
                  LbsrNetworkQueueEntry newEntry (newPacket, m_mainAddress, nextHop, Simulator::Now (), m_ipv4Route);

                  if (lbsrNetworkQueue->Enqueue (newEntry))
                    {
                      Scheduler (priority);
                    }
                  else
                    {
                      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
                    }
                }
            }

          if (m_errorBuffer.GetSize () != 0 && m_errorBuffer.Find (destination))
            {
              NS_LOG_LOGIC ("Schedule sending the next packet in error buffer");
              EventId sendPac = Simulator::Schedule (MilliSeconds (m_uniformRandomVariable->GetInteger (0,100)),
                                   &LbsrRouting::SendPacketFromBuffer, this, sourceRoute, nextHop, protocol);
              m_evrntIdVector.push_back(sendPac);
            }
        }
    }
  else
    {
      NS_LOG_DEBUG ("Packet not found in either the send or error buffer");
    }
}

bool
LbsrRouting::PassiveEntryCheck (Ptr<Packet> packet, Ipv4Address source, Ipv4Address destination, uint8_t segsLeft,
                               uint16_t fragmentOffset, uint16_t identification, bool saveEntry)
{
	//std::cout << "LbsrRouting::PassiveEntryCheck\n";

  NS_LOG_FUNCTION (this << packet << source << destination << (uint32_t)segsLeft);

  Ptr<Packet> p = packet->Copy ();
  // Here the segments left value need to plus one to check the earlier hop maintain buffer entry
  LbsrPassiveBuffEntry newEntry;
  newEntry.SetPacket (p);
  newEntry.SetSource (source);
  newEntry.SetDestination (destination);
  newEntry.SetIdentification (identification);
  newEntry.SetFragmentOffset (fragmentOffset);
  newEntry.SetSegsLeft (segsLeft);  // We try to make sure the segments left is larger for 1


  NS_LOG_DEBUG ("The passive buffer size " << m_passiveBuffer->GetSize ());

  if (m_passiveBuffer->AllEqual (newEntry) && (!saveEntry))
    {
      // The PromiscEqual function will remove the maintain buffer entry if equal value found
      // It only compares the source and destination address, ackId, and the segments left value
      NS_LOG_DEBUG ("We get the all equal for passive buffer here");

      LbsrMaintainBuffEntry mbEntry;
      mbEntry.SetPacket (p);
      mbEntry.SetSrc (source);
      mbEntry.SetDst (destination);
      mbEntry.SetAckId (0);
      mbEntry.SetSegsLeft (segsLeft + 1);

      CancelPassivePacketTimer (mbEntry);
      return true;
    }
  if (saveEntry)
    {
      /// Save this passive buffer entry for later check
      m_passiveBuffer->Enqueue (newEntry);
    }
  return false;
}

bool
LbsrRouting::CancelPassiveTimer (Ptr<Packet> packet, Ipv4Address source, Ipv4Address destination,
                                uint8_t segsLeft)
{
	//std::cout << "LbsrRouting::CancelPassiveTimer\n";

  NS_LOG_FUNCTION (this << packet << source << destination << (uint32_t)segsLeft);

  NS_LOG_DEBUG ("Cancel the passive timer");

  Ptr<Packet> p = packet->Copy ();
  // Here the segments left value need to plus one to check the earlier hop maintain buffer entry
  LbsrMaintainBuffEntry newEntry;
  newEntry.SetPacket (p);
  newEntry.SetSrc (source);
  newEntry.SetDst (destination);
  newEntry.SetAckId (0);
  newEntry.SetSegsLeft (segsLeft + 1);

  if (m_maintainBuffer.PromiscEqual (newEntry))
    {
      // The PromiscEqual function will remove the maintain buffer entry if equal value found
      // It only compares the source and destination address, ackId, and the segments left value
      CancelPassivePacketTimer (newEntry);
      return true;
    }
  return false;
}

void
LbsrRouting::CallCancelPacketTimer (uint16_t ackId, Ipv4Header const& ipv4Header, Ipv4Address realSrc, Ipv4Address realDst)
{
	//std::cout << "LbsrRouting::CallCancelPacketTimer\n";

  NS_LOG_FUNCTION (this << (uint32_t)ackId << ipv4Header << realSrc << realDst);
  Ipv4Address sender = ipv4Header.GetDestination ();
  Ipv4Address receiver = ipv4Header.GetSource ();
  /*
   * Create a packet to fill maintenance buffer, not used to compare with maintenance entry
   * The reason is ack header doesn't have the original packet copy
   */
  Ptr<Packet> mainP = Create<Packet> ();
  LbsrMaintainBuffEntry newEntry (/*Packet=*/ mainP, /*ourAddress=*/ sender, /*nextHop=*/ receiver,
                                          /*source=*/ realSrc, /*destination=*/ realDst, /*ackId=*/ ackId,
                                          /*SegsLeft=*/ 0, /*expire time=*/ Simulator::Now ());
  CancelNetworkPacketTimer (newEntry);  // Only need to cancel network packet timer
}

void
LbsrRouting::CancelPacketAllTimer (LbsrMaintainBuffEntry & mb)
{
	//std::cout << "LbsrRouting::CancelPacketAllTimer\n";

  NS_LOG_FUNCTION (this);
  CancelLinkPacketTimer (mb);
  CancelNetworkPacketTimer (mb);
  CancelPassivePacketTimer (mb);
}

void
LbsrRouting::CancelLinkPacketTimer (LbsrMaintainBuffEntry & mb)
{
	//std::cout << "LbsrRouting::CancelLinkPacketTimer\n";

  NS_LOG_FUNCTION (this);
  LinkKey linkKey;
  linkKey.m_ourAdd = mb.GetOurAdd ();
  linkKey.m_nextHop = mb.GetNextHop ();
  linkKey.m_source = mb.GetSrc ();
  linkKey.m_destination = mb.GetDst ();
  /*
   * Here we have found the entry for send retries, so we get the value and increase it by one
   */
  /// TODO need to think about this part
  m_linkCnt[linkKey] = 0;
  m_linkCnt.erase (linkKey);

  // TODO if find the linkkey, we need to remove it

  // Find the network acknowledgment timer
  std::map<LinkKey, Timer>::const_iterator i =
    m_linkAckTimer.find (linkKey);
  if (i == m_linkAckTimer.end ())
    {
      NS_LOG_INFO ("did not find the link timer");
    }
  else
    {
      NS_LOG_INFO ("did find the link timer");
      /*
       * Schedule the packet retry
       * Push back the nextHop, source, destination address
       */
      m_linkAckTimer[linkKey].Cancel ();
      m_linkAckTimer[linkKey].Remove ();
      if (m_linkAckTimer[linkKey].IsRunning ())
        {
          NS_LOG_INFO ("Timer not canceled");
        }
      m_linkAckTimer.erase (linkKey);
    }

  // Erase the maintenance entry
  // yet this does not check the segments left value here
  NS_LOG_DEBUG ("The link buffer size " << m_maintainBuffer.GetSize ());
  if (m_maintainBuffer.LinkEqual (mb))
    {
      NS_LOG_INFO ("Link acknowledgment received, remove same maintenance buffer entry");
    }
}

void
LbsrRouting::CancelNetworkPacketTimer (LbsrMaintainBuffEntry & mb)
{
	//std::cout << "LbsrRouting::CancelNetworkPacketTimer\n";

  NS_LOG_FUNCTION (this);
  NetworkKey networkKey;
  networkKey.m_ackId = mb.GetAckId ();
  networkKey.m_ourAdd = mb.GetOurAdd ();
  networkKey.m_nextHop = mb.GetNextHop ();
  networkKey.m_source = mb.GetSrc ();
  networkKey.m_destination = mb.GetDst ();
  /*
   * Here we have found the entry for send retries, so we get the value and increase it by one
   */
  m_addressForwardCnt[networkKey] = 0;
  m_addressForwardCnt.erase (networkKey);

  NS_LOG_INFO ("ackId " << mb.GetAckId () << " ourAdd " << mb.GetOurAdd () << " nextHop " << mb.GetNextHop ()
                        << " source " << mb.GetSrc () << " destination " << mb.GetDst ()
                        << " segsLeft " << (uint32_t)mb.GetSegsLeft ()
               );
  // Find the network acknowledgment timer
  std::map<NetworkKey, Timer>::const_iterator i =
    m_addressForwardTimer.find (networkKey);
  if (i == m_addressForwardTimer.end ())
    {
      NS_LOG_INFO ("did not find the packet timer");
    }
  else
    {
      NS_LOG_INFO ("did find the packet timer");
      /*
       * Schedule the packet retry
       * Push back the nextHop, source, destination address
       */
      m_addressForwardTimer[networkKey].Cancel ();
      m_addressForwardTimer[networkKey].Remove ();
      if (m_addressForwardTimer[networkKey].IsRunning ())
        {
          NS_LOG_INFO ("Timer not canceled");
        }
      m_addressForwardTimer.erase (networkKey);
    }
  // Erase the maintenance entry
  // yet this does not check the segments left value here
  if (m_maintainBuffer.NetworkEqual (mb))
    {
      NS_LOG_INFO ("Remove same maintenance buffer entry based on network acknowledgment");
    }
}

void
LbsrRouting::CancelPassivePacketTimer (LbsrMaintainBuffEntry & mb)
{
	//std::cout << "LbsrRouting::CancelPassivePacketTimer\n";

  NS_LOG_FUNCTION (this);
  PassiveKey passiveKey;
  passiveKey.m_ackId = 0;
  passiveKey.m_source = mb.GetSrc ();
  passiveKey.m_destination = mb.GetDst ();
  passiveKey.m_segsLeft = mb.GetSegsLeft ();

  m_passiveCnt[passiveKey] = 0;
  m_passiveCnt.erase (passiveKey);

  // Find the passive acknowledgment timer
  std::map<PassiveKey, Timer>::const_iterator j =
    m_passiveAckTimer.find (passiveKey);
  if (j == m_passiveAckTimer.end ())
    {
      NS_LOG_INFO ("did not find the passive timer");
    }
  else
    {
      NS_LOG_INFO ("find the passive timer");
      /*
       * Cancel passive acknowledgment timer
       */
      m_passiveAckTimer[passiveKey].Cancel ();
      m_passiveAckTimer[passiveKey].Remove ();
      if (m_passiveAckTimer[passiveKey].IsRunning ())
        {
          NS_LOG_INFO ("Timer not canceled");
        }
      m_passiveAckTimer.erase (passiveKey);
    }
}

void
LbsrRouting::CancelPacketTimerNextHop (Ipv4Address nextHop, uint8_t protocol)
{
	//std::cout << "LbsrRouting::CancelPacketTimerNextHop\n";

  NS_LOG_FUNCTION (this << nextHop << (uint32_t)protocol);

  LbsrMaintainBuffEntry entry;
  std::vector<Ipv4Address> previousErrorDst;
  if (m_maintainBuffer.Dequeue (nextHop, entry))
    {
      Ipv4Address source = entry.GetSrc ();
      Ipv4Address destination = entry.GetDst ();

      Ptr<Packet> lbsrP = entry.GetPacket ()->Copy ();
      Ptr<Packet> p = lbsrP->Copy ();
      Ptr<Packet> packet = lbsrP->Copy ();
      LbsrRoutingHeader lbsrRoutingHeader;
      lbsrP->RemoveHeader (lbsrRoutingHeader);          // Remove the lbsr header in whole
      uint32_t offset = lbsrRoutingHeader.GetLbsrOptionsOffset ();
      p->RemoveAtStart (offset);

      // Get the number of routers' address field
      uint8_t buf[2];
      p->CopyData (buf, sizeof(buf));
      uint8_t numberAddress = (buf[1] - 2) / 4;
      NS_LOG_DEBUG ("The number of addresses " << (uint32_t)numberAddress);
      LbsrOptionSRHeader sourceRoute;
      sourceRoute.SetNumberAddress (numberAddress);
      p->RemoveHeader (sourceRoute);
      std::vector<Ipv4Address> nodeList = sourceRoute.GetNodesAddress ();
      uint8_t salvage = sourceRoute.GetSalvage ();
      Ipv4Address address1 = nodeList[1];
      PrintVector (nodeList);

      /*
       * If the salvage is not 0, use the first address in the route as the error dst in error header
       * otherwise use the source of packet as the error destination
       */
      Ipv4Address errorDst;
      if (salvage)
        {
          errorDst = address1;
        }
      else
        {
          errorDst = source;
        }
      /// TODO if the errorDst is not seen before
      if (std::find (previousErrorDst.begin (), previousErrorDst.end (), destination) == previousErrorDst.end ())
        {
          NS_LOG_DEBUG ("have not seen this dst before " << errorDst << " in " << previousErrorDst.size ());
          SendUnreachError (nextHop, errorDst, destination, salvage, protocol);
          previousErrorDst.push_back (errorDst);
        }

      /*
       * Cancel the packet timer and then salvage the data packet
       */

      CancelPacketAllTimer (entry);
      SalvagePacket (packet, source, destination, protocol);

      if (m_maintainBuffer.GetSize () && m_maintainBuffer.Find (nextHop))
        {
          NS_LOG_INFO ("Cancel the packet timer for next maintenance entry");
         EventId cancelPac = Simulator::Schedule (MilliSeconds (m_uniformRandomVariable->GetInteger (0,100)),
                               &LbsrRouting::CancelPacketTimerNextHop,this,nextHop,protocol);
         m_evrntIdVector.push_back(cancelPac);
        }
    }
  else
    {
      NS_LOG_INFO ("Maintenance buffer entry not found");
    }
  /// TODO need to think about whether we need the network queue entry or not
}

void
LbsrRouting::SalvagePacket (Ptr<const Packet> packet, Ipv4Address source, Ipv4Address dst, uint8_t protocol)
{
	//std::cout << "LbsrRouting::SalvagePacket\n";

  NS_LOG_FUNCTION (this << packet << source << dst << (uint32_t)protocol);
  // Create two copies of packet
  Ptr<Packet> p = packet->Copy ();
  Ptr<Packet> newPacket = packet->Copy ();
  // Remove the routing header in a whole to get a clean packet
  LbsrRoutingHeader lbsrRoutingHeader;
  p->RemoveHeader (lbsrRoutingHeader);
  // Remove offset of lbsr routing header
  uint8_t offset = lbsrRoutingHeader.GetLbsrOptionsOffset ();
  newPacket->RemoveAtStart (offset);

  // Get the number of routers' address field
  uint8_t buf[2];
  newPacket->CopyData (buf, sizeof(buf));
  uint8_t numberAddress = (buf[1] - 2) / 4;

  LbsrOptionSRHeader sourceRoute;
  sourceRoute.SetNumberAddress (numberAddress);
  newPacket->RemoveHeader (sourceRoute);
  uint8_t salvage = sourceRoute.GetSalvage ();
  /*
   * Look in the route cache for other routes for this destination
   */
  LbsrRouteCacheEntry toDst;
  bool findRoute = m_routeCache->LookupRoute (dst, toDst);
  if (findRoute && (salvage < m_maxSalvageCount))
    {
      NS_LOG_DEBUG ("We have found a route for the packet");
      LbsrRoutingHeader newLbsrRoutingHeader;
      newLbsrRoutingHeader.SetNextHeader (protocol);
      newLbsrRoutingHeader.SetMessageType (2);
      newLbsrRoutingHeader.SetSourceId (GetIDfromIP (source));
      newLbsrRoutingHeader.SetDestId (GetIDfromIP (dst));

      std::vector<Ipv4Address> nodeList = toDst.GetVector ();     // Get the route from the route entry we found
      Ipv4Address nextHop = SearchNextHop (m_mainAddress, nodeList);      // Get the next hop address for the route
      if (nextHop == "0.0.0.0")
        {
          PacketNewRoute (p, source, dst, protocol);
          return;
        }
      // Increase the salvage count by 1
      salvage++;
      LbsrOptionSRHeader sourceRoute;
      sourceRoute.SetSalvage (salvage);
      sourceRoute.SetNodesAddress (nodeList);     // Save the whole route in the source route header of the packet
      sourceRoute.SetSegmentsLeft ((nodeList.size () - 2));     // The segmentsLeft field will indicate the hops to go
      /// When found a route and use it, UseExtends to the link cache
      if (m_routeCache->IsLinkCache ())
        {
          m_routeCache->UseExtends (nodeList);
        }
      uint8_t length = sourceRoute.GetLength ();
      NS_LOG_INFO ("length of source route header " << (uint32_t)(sourceRoute.GetLength ()));
      newLbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
      newLbsrRoutingHeader.AddLbsrOption (sourceRoute);
      p->AddHeader (newLbsrRoutingHeader);

      SetRoute (nextHop, m_mainAddress);
      Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
      m_ipv4Route->SetOutputDevice (dev);

      // Send out the data packet
      uint32_t priority = GetPriority (LBSR_DATA_PACKET);
      std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
      Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
      NS_LOG_DEBUG ("Will be inserting into priority queue " << lbsrNetworkQueue << " number: " << priority);

      //m_downTarget (p, m_mainAddress, nextHop, GetProtocolNumber (), m_ipv4Route);

      /// \todo New LbsrNetworkQueueEntry
      LbsrNetworkQueueEntry newEntry (p, m_mainAddress, nextHop, Simulator::Now (), m_ipv4Route);

      if (lbsrNetworkQueue->Enqueue (newEntry))
        {
          Scheduler (priority);
        }
      else
        {
          NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
        }

      /*
       * Mark the next hop address in blacklist
       */
//      NS_LOG_DEBUG ("Save the next hop node in blacklist");
//      m_rreqTable->MarkLinkAsUnidirectional (nextHop, m_blacklistTimeout);
    }
  else
    {
      NS_LOG_DEBUG ("Will not salvage this packet, silently drop");
    }
}

void
LbsrRouting::ScheduleLinkPacketRetry (LbsrMaintainBuffEntry & mb,
                                     uint8_t protocol)
{
	//std::cout << "LbsrRouting::ScheduleLinkPacketRetry\n";

  NS_LOG_FUNCTION (this << (uint32_t) protocol);

  Ptr<Packet> p = mb.GetPacket ()->Copy ();
  Ipv4Address source = mb.GetSrc ();
  Ipv4Address nextHop = mb.GetNextHop ();

  // Send the data packet out before schedule the next packet transmission
  SendPacket (p, source, nextHop, protocol);

  LinkKey linkKey;
  linkKey.m_source = mb.GetSrc ();
  linkKey.m_destination = mb.GetDst ();
  linkKey.m_ourAdd = mb.GetOurAdd ();
  linkKey.m_nextHop = mb.GetNextHop ();

  if (m_linkAckTimer.find (linkKey) == m_linkAckTimer.end ())
    {
      Timer timer (Timer::CANCEL_ON_DESTROY);
      m_linkAckTimer[linkKey] = timer;
    }
  m_linkAckTimer[linkKey].SetFunction (&LbsrRouting::LinkScheduleTimerExpire, this);
  m_linkAckTimer[linkKey].Remove ();
  m_linkAckTimer[linkKey].SetArguments (mb, protocol);
  m_linkAckTimer[linkKey].Schedule (m_linkAckTimeout);
}

void
LbsrRouting::SchedulePassivePacketRetry (LbsrMaintainBuffEntry & mb,
                                        uint8_t protocol)
{
	//std::cout << "LbsrRouting::SchedulePassivePacketRetry\n";

  NS_LOG_FUNCTION (this << (uint32_t)protocol);

  Ptr<Packet> p = mb.GetPacket ()->Copy ();
  Ipv4Address source = mb.GetSrc ();
  Ipv4Address nextHop = mb.GetNextHop ();

  // Send the data packet out before schedule the next packet transmission
  SendPacket (p, source, nextHop, protocol);

  PassiveKey passiveKey;
  passiveKey.m_ackId = 0;
  passiveKey.m_source = mb.GetSrc ();
  passiveKey.m_destination = mb.GetDst ();
  passiveKey.m_segsLeft = mb.GetSegsLeft ();

  if (m_passiveAckTimer.find (passiveKey) == m_passiveAckTimer.end ())
    {
      Timer timer (Timer::CANCEL_ON_DESTROY);
      m_passiveAckTimer[passiveKey] = timer;
    }
  NS_LOG_DEBUG ("The passive acknowledgment option for data packet");
  m_passiveAckTimer[passiveKey].SetFunction (&LbsrRouting::PassiveScheduleTimerExpire, this);
  m_passiveAckTimer[passiveKey].Remove ();
  m_passiveAckTimer[passiveKey].SetArguments (mb, protocol);
  m_passiveAckTimer[passiveKey].Schedule (m_passiveAckTimeout);
}

void
LbsrRouting::ScheduleNetworkPacketRetry (LbsrMaintainBuffEntry & mb,
                                        bool isFirst,
                                        uint8_t protocol)
{
	//std::cout << "LbsrRouting::ScheduleNetworkPacketRetry\n";

  Ptr<Packet> p = Create<Packet> ();
  Ptr<Packet> lbsrP = Create<Packet> ();
  // The new entry will be used for retransmission
  NetworkKey networkKey;
  Ipv4Address nextHop = mb.GetNextHop ();
  NS_LOG_DEBUG ("is the first retry or not " << isFirst);
  if (isFirst)
    {
      // This is the very first network packet retry
      p = mb.GetPacket ()->Copy ();
      // Here we add the ack request header to the data packet for network acknowledgement
      uint16_t ackId = AddAckReqHeader (p, nextHop);

      Ipv4Address source = mb.GetSrc ();
      Ipv4Address nextHop = mb.GetNextHop ();
      // Send the data packet out before schedule the next packet transmission
      SendPacket (p, source, nextHop, protocol);

      lbsrP = p->Copy ();
      LbsrMaintainBuffEntry newEntry = mb;
      // The function AllEqual will find the exact entry and delete it if found
      m_maintainBuffer.AllEqual (mb);
      newEntry.SetPacket (lbsrP);
      newEntry.SetAckId (ackId);
      newEntry.SetExpireTime (m_maxMaintainTime);

      networkKey.m_ackId = newEntry.GetAckId ();
      networkKey.m_ourAdd = newEntry.GetOurAdd ();
      networkKey.m_nextHop = newEntry.GetNextHop ();
      networkKey.m_source = newEntry.GetSrc ();
      networkKey.m_destination = newEntry.GetDst ();

      m_addressForwardCnt[networkKey] = 0;
      if (!m_maintainBuffer.Enqueue (newEntry))
        {
          NS_LOG_ERROR ("Failed to enqueue packet retry");
        }

      if (m_addressForwardTimer.find (networkKey) == m_addressForwardTimer.end ())
        {
          Timer timer (Timer::CANCEL_ON_DESTROY);
          m_addressForwardTimer[networkKey] = timer;
        }

      // After m_tryPassiveAcks, schedule the packet retransmission using network acknowledgment option
      m_addressForwardTimer[networkKey].SetFunction (&LbsrRouting::NetworkScheduleTimerExpire, this);
      m_addressForwardTimer[networkKey].Remove ();
      m_addressForwardTimer[networkKey].SetArguments (newEntry, protocol);
      NS_LOG_DEBUG ("The packet retries time for " << newEntry.GetAckId () << " is " << m_sendRetries
                                                   << " and the delay time is " << Time (2 * m_nodeTraversalTime).GetSeconds ());
      // Back-off mechanism
      m_addressForwardTimer[networkKey].Schedule (Time (2 * m_nodeTraversalTime));
    }
  else
    {
      networkKey.m_ackId = mb.GetAckId ();
      networkKey.m_ourAdd = mb.GetOurAdd ();
      networkKey.m_nextHop = mb.GetNextHop ();
      networkKey.m_source = mb.GetSrc ();
      networkKey.m_destination = mb.GetDst ();
      /*
       * Here we have found the entry for send retries, so we get the value and increase it by one
       */
      m_sendRetries = m_addressForwardCnt[networkKey];
      NS_LOG_DEBUG ("The packet retry we have done " << m_sendRetries);

      p = mb.GetPacket ()->Copy ();
      lbsrP = mb.GetPacket ()->Copy ();

      Ipv4Address source = mb.GetSrc ();
      Ipv4Address nextHop = mb.GetNextHop ();
      // Send the data packet out before schedule the next packet transmission
      SendPacket (p, source, nextHop, protocol);

      NS_LOG_DEBUG ("The packet with lbsr header " << lbsrP->GetSize ());
      networkKey.m_ackId = mb.GetAckId ();
      networkKey.m_ourAdd = mb.GetOurAdd ();
      networkKey.m_nextHop = mb.GetNextHop ();
      networkKey.m_source = mb.GetSrc ();
      networkKey.m_destination = mb.GetDst ();
      /*
       *  If a data packet has been attempted SendRetries times at the maximum TTL without
       *  receiving any ACK, all data packets destined for the corresponding destination SHOULD be
       *  dropped from the send buffer
       *
       *  The maxMaintRexmt also needs to decrease one for the passive ack packet
       */
      /*
       * Check if the send retry time for a certain packet has already passed max maintenance retransmission
       * time or not
       */

      // After m_tryPassiveAcks, schedule the packet retransmission using network acknowledgment option
      m_addressForwardTimer[networkKey].SetFunction (&LbsrRouting::NetworkScheduleTimerExpire, this);
      m_addressForwardTimer[networkKey].Remove ();
      m_addressForwardTimer[networkKey].SetArguments (mb, protocol);
      NS_LOG_DEBUG ("The packet retries time for " << mb.GetAckId () << " is " << m_sendRetries
                                                   << " and the delay time is " << Time (2 * m_sendRetries *  m_nodeTraversalTime).GetSeconds ());
      // Back-off mechanism
      m_addressForwardTimer[networkKey].Schedule (Time (2 * m_sendRetries * m_nodeTraversalTime));
    }
}

void
LbsrRouting::LinkScheduleTimerExpire  (LbsrMaintainBuffEntry & mb,
                                      uint8_t protocol)
{
	//std::cout << "LbsrRouting::LinkScheduleTimerExpire\n";

  NS_LOG_FUNCTION (this << (uint32_t)protocol);
  Ipv4Address nextHop = mb.GetNextHop ();
  Ptr<const Packet> packet = mb.GetPacket ();
  SetRoute (nextHop, m_mainAddress);
  Ptr<Packet> p = packet->Copy ();

  LinkKey lk;
  lk.m_source = mb.GetSrc ();
  lk.m_destination = mb.GetDst ();
  lk.m_ourAdd = mb.GetOurAdd ();
  lk.m_nextHop = mb.GetNextHop ();

  // Cancel passive ack timer
  m_linkAckTimer[lk].Cancel ();
  m_linkAckTimer[lk].Remove ();
  if (m_linkAckTimer[lk].IsRunning ())
    {
      NS_LOG_DEBUG ("Timer not canceled");
    }
  m_linkAckTimer.erase (lk);

  // Increase the send retry times
  m_linkRetries = m_linkCnt[lk];
  if (m_linkRetries < m_tryLinkAcks)
    {
      m_linkCnt[lk] = ++m_linkRetries;
      ScheduleLinkPacketRetry (mb, protocol);
    }
  else
    {
      NS_LOG_INFO ("We need to send error messages now");

      // Delete all the routes including the links
      m_routeCache->DeleteAllRoutesIncludeLink (m_mainAddress, nextHop, m_mainAddress);
      /*
       * here we cancel the packet retransmission time for all the packets have next hop address as nextHop
       * Also salvage the packet for the all the packet destined for the nextHop address
       * this is also responsible for send unreachable error back to source
       */
      CancelPacketTimerNextHop (nextHop, protocol);
    }
}

void
LbsrRouting::PassiveScheduleTimerExpire  (LbsrMaintainBuffEntry & mb,
                                         uint8_t protocol)
{
	//std::cout << "LbsrRouting::PassiveScheduleTimerExpire\n";

  NS_LOG_FUNCTION (this << (uint32_t)protocol);
  Ipv4Address nextHop = mb.GetNextHop ();
  Ptr<const Packet> packet = mb.GetPacket ();
  SetRoute (nextHop, m_mainAddress);
  Ptr<Packet> p = packet->Copy ();

  PassiveKey pk;
  pk.m_ackId = 0;
  pk.m_source = mb.GetSrc ();
  pk.m_destination = mb.GetDst ();
  pk.m_segsLeft = mb.GetSegsLeft ();

  // Cancel passive ack timer
  m_passiveAckTimer[pk].Cancel ();
  m_passiveAckTimer[pk].Remove ();
  if (m_passiveAckTimer[pk].IsRunning ())
    {
      NS_LOG_DEBUG ("Timer not canceled");
    }
  m_passiveAckTimer.erase (pk);

  // Increase the send retry times
  m_passiveRetries = m_passiveCnt[pk];
  if (m_passiveRetries < m_tryPassiveAcks)
    {
      m_passiveCnt[pk] = ++m_passiveRetries;
      SchedulePassivePacketRetry (mb, protocol);
    }
  else
    {
      // This is the first network acknowledgement retry
      // Cancel the passive packet timer now and remove maintenance buffer entry for it
      CancelPassivePacketTimer (mb);
      ScheduleNetworkPacketRetry (mb, true, protocol);
    }
}

int64_t
LbsrRouting::AssignStreams (int64_t stream)
{
	//std::cout << "LbsrRouting::AssignStreams\n";

  NS_LOG_FUNCTION (this << stream);
  m_uniformRandomVariable->SetStream (stream);
  return 1;
}

void
LbsrRouting::NetworkScheduleTimerExpire  (LbsrMaintainBuffEntry & mb,
                                         uint8_t protocol)
{
	//std::cout << "LbsrRouting::NetworkScheduleTimerExpire\n";

  Ptr<Packet> p = mb.GetPacket ()->Copy ();
  Ipv4Address source = mb.GetSrc ();
  Ipv4Address nextHop = mb.GetNextHop ();
  Ipv4Address dst = mb.GetDst ();

  NetworkKey networkKey;
  networkKey.m_ackId = mb.GetAckId ();
  networkKey.m_ourAdd = mb.GetOurAdd ();
  networkKey.m_nextHop = nextHop;
  networkKey.m_source = source;
  networkKey.m_destination = dst;

  // Increase the send retry times
  m_sendRetries = m_addressForwardCnt[networkKey];

  if (m_sendRetries >= m_maxMaintRexmt)
    {
      // Delete all the routes including the links
      m_routeCache->DeleteAllRoutesIncludeLink (m_mainAddress, nextHop, m_mainAddress);
      /*
       * here we cancel the packet retransmission time for all the packets have next hop address as nextHop
       * Also salvage the packet for the all the packet destined for the nextHop address
       */
      CancelPacketTimerNextHop (nextHop, protocol);
    }
  else
    {
      m_addressForwardCnt[networkKey] = ++m_sendRetries;
      ScheduleNetworkPacketRetry (mb, false, protocol);
    }
}

void
LbsrRouting::ForwardPacket (Ptr<const Packet> packet,
                           LbsrOptionSRHeader &sourceRoute,
                           Ipv4Header const& ipv4Header,
                           Ipv4Address source,
                           Ipv4Address nextHop,
                           Ipv4Address targetAddress,
                           uint8_t protocol,
                           Ptr<Ipv4Route> route)
{
	//std::cout << "LbsrRouting::ForwardPacket\n";

  NS_LOG_FUNCTION (this << packet << sourceRoute << source << nextHop << targetAddress << (uint32_t)protocol << route);
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");

  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrRoutingHeader.SetNextHeader (protocol);
  lbsrRoutingHeader.SetMessageType (2);
  lbsrRoutingHeader.SetSourceId (GetIDfromIP (source));
  lbsrRoutingHeader.SetDestId (GetIDfromIP (targetAddress));
  //std::cout << "targetAddress is " << targetAddress << "\n";
  // We get the salvage value in sourceRoute header and set it to route error header if triggered error
  Ptr<Packet> p = packet->Copy ();
  uint8_t length = sourceRoute.GetLength ();
  lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
  lbsrRoutingHeader.AddLbsrOption (sourceRoute);
  p->AddHeader (lbsrRoutingHeader);

  Ptr<const Packet> mtP = p->Copy ();

  LbsrMaintainBuffEntry newEntry (/*Packet=*/ mtP, /*ourAddress=*/ m_mainAddress, /*nextHop=*/ nextHop,
                              /*source=*/ source, /*destination=*/ targetAddress, /*ackId=*/ m_ackId,
                              /*SegsLeft=*/ sourceRoute.GetSegmentsLeft (), /*expire time=*/ m_maxMaintainTime);
  bool result = m_maintainBuffer.Enqueue (newEntry);

  if (result)
    {
      NetworkKey networkKey;
      networkKey.m_ackId = newEntry.GetAckId ();
      networkKey.m_ourAdd = newEntry.GetOurAdd ();
      networkKey.m_nextHop = newEntry.GetNextHop ();
      networkKey.m_source = newEntry.GetSrc ();
      networkKey.m_destination = newEntry.GetDst ();

      PassiveKey passiveKey;
      passiveKey.m_ackId = 0;
      passiveKey.m_source = newEntry.GetSrc ();
      passiveKey.m_destination = newEntry.GetDst ();
      passiveKey.m_segsLeft = newEntry.GetSegsLeft ();

      LinkKey linkKey;
      linkKey.m_source = newEntry.GetSrc ();
      linkKey.m_destination = newEntry.GetDst ();
      linkKey.m_ourAdd = newEntry.GetOurAdd ();
      linkKey.m_nextHop = newEntry.GetNextHop ();

      m_addressForwardCnt[networkKey] = 0;
      m_passiveCnt[passiveKey] = 0;
      m_linkCnt[linkKey] = 0;

      if (m_linkAck)
        {
          ScheduleLinkPacketRetry (newEntry, protocol);
        }
      else
        {
          NS_LOG_LOGIC ("Not using link acknowledgment");
          if (nextHop != targetAddress)
            {
              SchedulePassivePacketRetry (newEntry, protocol);
            }
          else
            {
              // This is the first network retry
              ScheduleNetworkPacketRetry (newEntry, true, protocol);
            }
        }
    }
}

void
LbsrRouting::SendInitialRequest (Ipv4Address source,
                                Ipv4Address destination,
                                uint8_t protocol)
{
	std::cout << "LbsrRouting::SendInitialRequest source " << source <<" destination " << destination<< " protocol "<< (uint32_t)protocol << "\n";


	SetSendErrorFlag(false);
  NS_LOG_FUNCTION (this << source << destination << (uint32_t)protocol);
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");
  Ptr<Packet> packet = Create<Packet> ();
  // Create an empty Ipv4 route ptr
  Ptr<Ipv4Route> route;
  /*
   * Construct the route request option header
   */
  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrRoutingHeader.SetNextHeader (protocol);
  lbsrRoutingHeader.SetMessageType (1); //１はデータパケット
  lbsrRoutingHeader.SetSourceId (GetIDfromIP (source));
  lbsrRoutingHeader.SetDestId (255);

  LbsrOptionRreqHeader rreqHeader;                                  // has an alignment of 4n+0
  rreqHeader.AddNodeAddress (m_mainAddress);                       // Add our own address in the headerここでノード追加している
  //std::cout << "sendIntial m_mainAddress is " << m_mainAddress << "\n";
  rreqHeader.SetTarget (destination);
  /*重要ここでヘッダーの中にdestinationが存在しないか見ている*/
  m_requestId = m_rreqTable->CheckUniqueRreqId (destination);      // Check the Id cache for duplicate ones重要
 // std::cout << "m_requestId " << m_requestId << "\n";
  rreqHeader.SetId (m_requestId);

  lbsrRoutingHeader.AddLbsrOption (rreqHeader);                      // Add the rreqHeader to the lbsr extension header
  uint8_t length = rreqHeader.GetLength ();
  lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
  packet->AddHeader (lbsrRoutingHeader);

  // Schedule the route requests retry with non-propagation set true
  //非伝播をtrueに設定して、ルート要求の再試行をスケジュールします
  bool nonProp = true;
  std::vector<Ipv4Address> address;
  address.push_back (source);//address[0] = source
  address.push_back (destination);//address[1] = destination
  /*
   * Add the socket ip ttl tag to the packet to limit the scope of route requests
   */
  SocketIpTtlTag tag;
  tag.SetTtl (0);//ttlはデータの有効時間
  Ptr<Packet> nonPropPacket = packet->Copy ();
  nonPropPacket->AddPacketTag (tag);
  // Increase the request count
  m_rreqTable->FindAndUpdate (destination);
  EventId checkId = Simulator::Schedule(Seconds(40), &LbsrRouting::CheckCreateRouteFlag, this, source, destination, protocol);
  m_evrntIdVector.push_back(checkId);
  SendLreq (nonPropPacket, source);
  // Schedule the next route request
  ScheduleRreqRetry (packet, address, nonProp, m_requestId, protocol);
}

void
LbsrRouting::SendErrorRequest (LbsrOptionRerrUnreachHeader &rerr, uint8_t protocol)
{
	std::cout << "\nLbsrRouting::SendErrorRequest m_ipv4Address "<< m_mainAddress << "\n";

  NS_LOG_FUNCTION (this << (uint32_t)protocol);
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");
  uint8_t salvage = rerr.GetSalvage ();
  Ipv4Address dst = rerr.GetOriginalDst ();
  NS_LOG_DEBUG ("our own address here " << m_mainAddress << " error source " << rerr.GetErrorSrc () << " error destination " << rerr.GetErrorDst ()
                                        << " error next hop " << rerr.GetUnreachNode () << " original dst " << rerr.GetOriginalDst ()
                );
  LbsrRouteCacheEntry toDst;
  if (m_routeCache->LookupRoute (dst, toDst))
    {
      /*
       * Found a route the dst, construct the source route option header
       */
      LbsrOptionSRHeader sourceRoute;
      std::vector<Ipv4Address> ip = toDst.GetVector ();
      sourceRoute.SetNodesAddress (ip);
      /// When found a route and use it, UseExtends to the link cache
      if (m_routeCache->IsLinkCache ())
        {
          m_routeCache->UseExtends (ip);
        }
      sourceRoute.SetSegmentsLeft ((ip.size () - 2));
      sourceRoute.SetSalvage (salvage);
      Ipv4Address nextHop = SearchNextHop (m_mainAddress, ip);       // Get the next hop address
      NS_LOG_DEBUG ("The nextHop address " << nextHop);
      Ptr<Packet> packet = Create<Packet> ();
      if (nextHop == "0.0.0.0")
        {
          NS_LOG_DEBUG ("Error next hop address");
          PacketNewRoute (packet, m_mainAddress, dst, protocol);
          return;
        }
      SetRoute (nextHop, m_mainAddress);
      CancelRreqTimer (dst, true);
      /// Try to send out the packet from the buffer once we found one route
      if (m_sendBuffer.GetSize () != 0 && m_sendBuffer.Find (dst))
        {
          SendPacketFromBuffer (sourceRoute, nextHop, protocol);
        }
      NS_LOG_LOGIC ("Route to " << dst << " found");
      return;
    }
  else
    {
      NS_LOG_INFO ("No route found, initiate route error request");
      Ptr<Packet> packet = Create<Packet> ();
      Ipv4Address originalDst = rerr.GetOriginalDst ();
      // Create an empty route ptr
      Ptr<Ipv4Route> route = 0;
      /*
       * Construct the route request option header
       */
      LbsrRoutingHeader lbsrRoutingHeader;
      lbsrRoutingHeader.SetNextHeader (protocol);
      lbsrRoutingHeader.SetMessageType (1);
      lbsrRoutingHeader.SetSourceId (GetIDfromIP (m_mainAddress));
      lbsrRoutingHeader.SetDestId (255);

      Ptr<Packet> dstP = Create<Packet> ();
      LbsrOptionRreqHeader rreqHeader;                                // has an alignment of 4n+0
      rreqHeader.AddNodeAddress (m_mainAddress);                     // Add our own address in the header
      rreqHeader.SetTarget (originalDst);
      m_requestId = m_rreqTable->CheckUniqueRreqId (originalDst);       // Check the Id cache for duplicate ones
      rreqHeader.SetId (m_requestId);

      lbsrRoutingHeader.AddLbsrOption (rreqHeader);         // Add the rreqHeader to the lbsr extension header
      lbsrRoutingHeader.AddLbsrOption (rerr);
      uint8_t length = rreqHeader.GetLength () + rerr.GetLength ();
      lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 4);
      dstP->AddHeader (lbsrRoutingHeader);
      // Schedule the route requests retry, propagate the route request message as it contains error
      bool nonProp = false;
      std::vector<Ipv4Address> address;
      address.push_back (m_mainAddress);
      address.push_back (originalDst);
      /*
       * Add the socket ip ttl tag to the packet to limit the scope of route requests
       */
      SocketIpTtlTag tag;
      tag.SetTtl ((uint8_t)m_discoveryHopLimit);
      Ptr<Packet> propPacket = dstP->Copy ();
      propPacket->AddPacketTag (tag);

      if ((m_addressReqTimer.find (originalDst) == m_addressReqTimer.end ()) && (m_nonPropReqTimer.find (originalDst) == m_nonPropReqTimer.end ()))
        {
          NS_LOG_INFO ("Only when there is no existing route request time when the initial route request is scheduled");
          SendLreq (propPacket, m_mainAddress);
          ScheduleRreqRetry (dstP, address, nonProp, m_requestId, protocol);
        }
      else
        {
          NS_LOG_INFO ("There is existing route request, find the existing route request entry");
          /*
           * Cancel the route request timer first before scheduling the route request
           * in this case, we do not want to remove the route request entry, so the isRemove value is false
           */
          CancelRreqTimer (originalDst, false);
          ScheduleRreqRetry (dstP, address, nonProp, m_requestId, protocol);
        }
    }
}

void
LbsrRouting::CancelRreqTimer (Ipv4Address dst, bool isRemove)
{
	//std::cout << "LbsrRouting::CancelRreqTimer\n";

  NS_LOG_FUNCTION (this << dst << isRemove);
  // Cancel the non propagation request timer if found
  if (m_nonPropReqTimer.find (dst) == m_nonPropReqTimer.end ())
    {
      NS_LOG_DEBUG ("Did not find the non-propagation timer");
    }
  else
    {
      NS_LOG_DEBUG ("did find the non-propagation timer");
    }
  m_nonPropReqTimer[dst].Cancel ();
  m_nonPropReqTimer[dst].Remove ();

  if (m_nonPropReqTimer[dst].IsRunning ())
    {
      NS_LOG_DEBUG ("Timer not canceled");
    }
  m_nonPropReqTimer.erase (dst);

  // Cancel the address request timer if found
  if (m_addressReqTimer.find (dst) == m_addressReqTimer.end ())
    {
      NS_LOG_DEBUG ("Did not find the propagation timer");
    }
  else
    {
      NS_LOG_DEBUG ("did find the propagation timer");
    }
  m_addressReqTimer[dst].Cancel ();
  m_addressReqTimer[dst].Remove ();
  if (m_addressReqTimer[dst].IsRunning ())
    {
      NS_LOG_DEBUG ("Timer not canceled");
    }
  m_addressReqTimer.erase (dst);
  /*
   * If the route request is scheduled to remove the route request entry
   * Remove the route request entry with the route retry times done for certain destination
   */
  if (isRemove)
    {
      // remove the route request entry from route request table
      m_rreqTable->RemoveRreqEntry (dst);
    }
}

void
LbsrRouting::ScheduleRreqRetry (Ptr<Packet> packet, std::vector<Ipv4Address> address, bool nonProp, uint32_t requestId, uint8_t protocol)
{
	std::cout << "nLbsrRouting::ScheduleRreqRetry\n";

  NS_LOG_FUNCTION (this << packet << nonProp << requestId << (uint32_t)protocol);
  Ipv4Address source = address[0];
  Ipv4Address dst = address[1];
  if (nonProp)
    {
      // The nonProp route request is only sent out only and is already used
      if (m_nonPropReqTimer.find (dst) == m_nonPropReqTimer.end ())
        {
          Timer timer (Timer::CANCEL_ON_DESTROY);
          m_nonPropReqTimer[dst] = timer;
        }


      std::vector<Ipv4Address> SetAddress;// ここでaddressをもう一度つくってる
      SetAddress.push_back (source);
      SetAddress.push_back (dst);
      m_nonPropReqTimer[dst].SetFunction (&LbsrRouting::RouteRequestTimerExpire, this);
      m_nonPropReqTimer[dst].Remove ();
      m_nonPropReqTimer[dst].SetArguments (packet, SetAddress, requestId, protocol);
      m_nonPropReqTimer[dst].Schedule (m_nonpropRequestTimeout);
    }
  else
    {
      // Cancel the non propagation request timer if found
      m_nonPropReqTimer[dst].Cancel ();
      m_nonPropReqTimer[dst].Remove ();
      if (m_nonPropReqTimer[dst].IsRunning ())
        {
          NS_LOG_DEBUG ("Timer not canceled");
        }
      m_nonPropReqTimer.erase (dst);

      if (m_addressReqTimer.find (dst) == m_addressReqTimer.end ())
        {
          Timer timer (Timer::CANCEL_ON_DESTROY);
          m_addressReqTimer[dst] = timer;
        }
      std::vector<Ipv4Address> SetAddress;
      SetAddress.push_back (source);
      SetAddress.push_back (dst);
      m_addressReqTimer[dst].SetFunction (&LbsrRouting::RouteRequestTimerExpire, this);
      m_addressReqTimer[dst].Remove ();
      m_addressReqTimer[dst].SetArguments (packet, SetAddress, requestId, protocol);
      Time rreqDelay;
      // back off mechanism for sending route requests
      if (m_rreqTable->GetRreqCnt (dst))
        {
          // When the route request count is larger than 0
          // This is the exponential back-off mechanism for route request
          rreqDelay = Time (std::pow (static_cast<double> (m_rreqTable->GetRreqCnt (dst)), 2.0) * m_requestPeriod);
        }
      else
        {
          // This is the first route request retry
          rreqDelay = m_requestPeriod;
        }
      NS_LOG_LOGIC ("Request count for " << dst << " " << m_rreqTable->GetRreqCnt (dst) << " with delay time " << rreqDelay.GetSeconds () << " second");
      if (rreqDelay > m_maxRequestPeriod)
        {
          // use the max request period
          NS_LOG_LOGIC ("The max request delay time " << m_maxRequestPeriod.GetSeconds ());
          m_addressReqTimer[dst].Schedule (m_maxRequestPeriod);
        }
      else
        {
          NS_LOG_LOGIC ("The request delay time " << rreqDelay.GetSeconds () << " second");
          m_addressReqTimer[dst].Schedule (rreqDelay);
        }
    }
}

int LbsrRouting::LreqRetryCount;
void
LbsrRouting::RouteRequestTimerExpire (Ptr<Packet> packet, std::vector<Ipv4Address> address, uint32_t requestId, uint8_t protocol)
{
	//std::cout << "LbsrRouting::RouteRequestTimerExpire m_mainAddress "<< m_mainAddress << "\n";

  NS_LOG_FUNCTION (this << packet << requestId << (uint32_t)protocol);
  // Get a clean packet without lbsr header
  Ptr<Packet> lbsrP = packet->Copy ();
  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrP->RemoveHeader (lbsrRoutingHeader);          // Remove the lbsr header in whole

  Ipv4Address source = address[0];
  Ipv4Address dst = address[1];
  LbsrRouteCacheEntry toDst;
  if (m_routeCache->LookupRoute (dst, toDst))
    {
      /*
       * Found a route the dst, construct the source route option header
       */
	//  std::cout << "RouteRequestTimerExpire find toDst\n";
      LbsrOptionSRHeader sourceRoute;
      std::vector<Ipv4Address> ip = toDst.GetVector ();
      sourceRoute.SetNodesAddress (ip);
      // When we found the route and use it, UseExtends for the link cache
      if (m_routeCache->IsLinkCache ())
        {
          m_routeCache->UseExtends (ip);
        }
      sourceRoute.SetSegmentsLeft ((ip.size () - 2));
      /// Set the salvage value to 0
      sourceRoute.SetSalvage (0);
      Ipv4Address nextHop = SearchNextHop (m_mainAddress, ip);       // Get the next hop address
      NS_LOG_INFO ("The nextHop address is " << nextHop);
      if (nextHop == "0.0.0.0")
        {
          NS_LOG_DEBUG ("Error next hop address");
          PacketNewRoute (lbsrP, source, dst, protocol);
          return;
        }
      SetRoute (nextHop, m_mainAddress);
      CancelRreqTimer (dst, true);
      /// Try to send out data packet from the send buffer if found
      if (m_sendBuffer.GetSize () != 0 && m_sendBuffer.Find (dst))
        {
          SendPacketFromBuffer (sourceRoute, nextHop, protocol);
        }
      NS_LOG_LOGIC ("Route to " << dst << " found");
      return;
    }
  /*
   *  If a route discovery has been attempted m_rreqRetries times at the maximum TTL without
   *  receiving any RREP, all data packets destined for the corresponding destination SHOULD be
   *  dropped from the buffer and a Destination Unreachable message SHOULD be delivered to the application.
   */
  NS_LOG_LOGIC ("The new request count for " << dst << " is " << m_rreqTable->GetRreqCnt (dst) << " the max " << m_rreqRetries);
  if (m_rreqTable->GetRreqCnt (dst) >= m_rreqRetries)
    {
      NS_LOG_LOGIC ("Route discovery to " << dst << " has been attempted " << m_rreqRetries << " times");
      std::cout << "Route discovery to " << dst << " has been attempted " << m_rreqRetries << " times\n";

      CancelRreqTimer (dst, true);
      NS_LOG_DEBUG ("Route not found. Drop packet with dst " << dst);
      m_sendBuffer.DropPacketWithDst (dst);
    }
  else
    {//何回もここに入ってsourceのSendLreqに入る
	  if(LreqRetryCount <2){
	  std::cout << "first RouteRequestTimerExpire requestId "<< requestId << "m_mainAddress "<<m_mainAddress<<"\n";
      SocketIpTtlTag tag;
      tag.SetTtl ((uint8_t)m_discoveryHopLimit);
      Ptr<Packet> propPacket = packet->Copy ();
      propPacket->AddPacketTag (tag);
      // Increase the request count
      m_rreqTable->FindAndUpdate (dst);//destinationが見つかる
      SendLreq (propPacket, source);
      NS_LOG_DEBUG ("Check the route request entry " << source << " " << dst);
      ScheduleRreqRetry (packet, address, false, requestId, protocol);
      LreqRetryCount++;
      if(LreqRetryCount == 2){
			EventId flagId = Simulator::Schedule(Seconds(5), &LbsrRouting::SetLreqRetryCount, this);
			m_evrntIdVector.push_back(flagId);
      }
	  }
    }
  return;
}


//here is SendLreq
bool LbsrRouting::SendLreqFlag;
void
LbsrRouting::SendLreq (Ptr<Packet> packet,
                         Ipv4Address source)
{
	//std::cout << "LbsrRouting::SendLreq m_mainAddress " << m_mainAddress << "\n";

  NS_LOG_FUNCTION (this << packet << source);
  //std::cout << this << " " <<packet << " " << source <<"\n";

  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");
  if(!m_downTarget.IsNull ()){
	  //std::cout << "Error, LbsrRouting cannot send downward \n";
  }else{
	//  std::cout << "downTarget is exist\n";
  }
  /*
   * The destination address here is directed broadcast address
   * ここのdestinationアドレスはm_mainAddressのブロードキャスト先になる
   * 実行回数もブロードキャスト先の数だけある
   */

	std::string temp;
	int nodeNum=0;
	Ipv4Address nextHop;
	int num=0;
	num=GetNodeNumber();
	Ipv4Address realSource;
	for(int j = 0; j < num; j++){
		temp = "10.1.1." + std::to_string(j+1);
		if(source == ns3::Ipv4Address(temp.c_str())){
			nodeNum=j;
		}
		if(j == num-1){
			temp = "10.1.1." + std::to_string(j+1);
			realSource = ns3::Ipv4Address(temp.c_str());
		}
	}
	Ptr<lbsr::LbsrOptions> lbsrop;

    if(m_mainAddress == realSource){//m_mainAddressがsourceの場合
	  	  //std::cout << "SendRreq from source\n";
	  	  bool sendFlag = GetSendLreqFlag();
	//if(m_mainAddress == "10.1.1.20")
	  	  //ここをカウントじゃなくて時間にしたほうがいいと思う。フラグを作って入ったらtureにする、中でScheduleで時間後にfalseにする
	  	//  if(sendFlag == false){
	  		std::cout << "sendFlag is false\n\n";
  	  		m_finalRoute.clear();
			std::vector<Ipv4Address> temp;
			temp.push_back("0.0.0.0");
			/*
			for(int i=0; i<nodenum; i++){
				lbsrop->SetNodeCache(i, false, false, false, "0.0.0.0", 255, 0);//一番最初のここではまだNodeCacheは存在しない
				lbsrop->SetNodesRoute(i, temp);
			}
			*/
			sendFlag = true;
			SetSendLreqFlag(sendFlag);
			//EventId flagId = Simulator::Schedule(Seconds(5), &LbsrRouting::SetSendLreqFlag, this, false);
			//m_evrntIdVector.push_back(flagId);
	  	 // }

    	promiscCount++;
    	//if(promiscCount % 30 ==0 || promiscCount < 2){
    		//std::cout << "LbsrRouting::SendLreq m_mainAddress " << m_mainAddress << "\n";
      uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);//priority = 0
      std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
      Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
      NS_LOG_LOGIC ("Inserting into priority queue number: " << priority);

      //m_downTarget (packet, source, m_broadcast, GetProtocolNumber (), 0);

      /// \todo New LbsrNetworkQueueEntry
      //std::cout << "SendLreq m_mainAddress " << m_mainAddress << "\n";
/*
      Ipv4Address nextAddress=m_broadcast;
      if(nextHop != "0.0.0.0"){
    	  nextAddress = nextHop;
      }
*/
      LbsrNetworkQueueEntry newEntry (packet, source, m_broadcast, Simulator::Now (), 0);
      if (lbsrNetworkQueue->Enqueue (newEntry))
        {
    	  BroadCastCount++;
          Scheduler (priority);
        //  std::cout << "priority is " << priority << "\n";
        }
      else
        {
          NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
        }
	  //	  }
    //	}
    }else{
	nextHop=lbsrop->GetNext(nodeNum);

  uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);//priority = 0
  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
  NS_LOG_LOGIC ("Inserting into priority queue number: " << priority);

  //m_downTarget (packet, source, m_broadcast, GetProtocolNumber (), 0);

  /// \todo New LbsrNetworkQueueEntry
  //std::cout << "SendLreq m_mainAddress " << m_mainAddress << "\n";

  Ipv4Address nextAddress=m_broadcast;
	  if(nextHop != "0.0.0.0"){
		  nextAddress = nextHop;
	  }
	  LbsrNetworkQueueEntry newEntry (packet, source, nextAddress, Simulator::Now (), 0);
	  if (lbsrNetworkQueue->Enqueue (newEntry))
	    {
		  if(nextAddress == nextHop){
		  UniCastCount++;
		  }else{
			  BroadCastCount++;
		  }
	      Scheduler (priority);
	    //  std::cout << "priority is " << priority << "\n";
	    }
	  else
	    {
	      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
	    }
/*
  LbsrNetworkQueueEntry newEntry (packet, source, nextAddress, Simulator::Now (), 0);
  if (lbsrNetworkQueue->Enqueue (newEntry))
    {
      Scheduler (priority);
    //  std::cout << "priority is " << priority << "\n";
    }
  else
    {
      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
    }
    */
    }
}

void
LbsrRouting::ScheduleInterRequest (Ptr<Packet> packet)
{
	//std::cout << "LbsrRouting::ScheduleInterRequest\n";

  NS_LOG_FUNCTION (this << packet);
  /*
   * This is a forwarding case when sending route requests, a random delay time [0, m_broadcastJitter]
   * used before forwarding as link-layer broadcast
   */
  EventId intRequest = Simulator::Schedule (MilliSeconds (m_uniformRandomVariable->GetInteger (0, m_broadcastJitter)), &LbsrRouting::SendLreq, this,
                       packet, m_mainAddress);
  m_evrntIdVector.push_back(intRequest);
}

void
LbsrRouting::SendGratuitousReply (Ipv4Address source, Ipv4Address srcAddress, std::vector<Ipv4Address> &nodeList, uint8_t protocol)
{
	std::cout << "LbsrRouting::SendGratuitousReply\n";
	/*重要ここがreply messageを作る最初のメソッド*/

  NS_LOG_FUNCTION (this << source << srcAddress << (uint32_t)protocol);
  if (!(m_graReply.FindAndUpdate (source, srcAddress, m_gratReplyHoldoff)))     // Find the gratuitous reply entry
    {
      NS_LOG_LOGIC ("Update gratuitous reply " << source);
      GraReplyEntry graReplyEntry (source, srcAddress, m_gratReplyHoldoff + Simulator::Now ());
      m_graReply.AddEntry (graReplyEntry);
      /*
       * Automatic route shortening
       */
      m_finalRoute.clear ();      // Clear the final route vector
      /**
       * Push back the node addresses other than those between srcAddress and our own ip address
       */
      std::vector<Ipv4Address>::iterator before = find (nodeList.begin (), nodeList.end (), srcAddress);//source, destination, soruceAddres
      for (std::vector<Ipv4Address>::iterator i = nodeList.begin (); i != before; ++i)
        {
          m_finalRoute.push_back (*i);
         // std::cout << "before m_finalRoute" << *i << "\n";
        }
      m_finalRoute.push_back (srcAddress);
      std::vector<Ipv4Address>::iterator after = find (nodeList.begin (), nodeList.end (), m_mainAddress);
      for (std::vector<Ipv4Address>::iterator j = after; j != nodeList.end (); ++j)
        {
          m_finalRoute.push_back (*j);
       //   std::cout << "after m_finalRoute" << *j << "\n";

        }

      LbsrOptionRrepHeader rrep;
      rrep.SetNodesAddress (m_finalRoute);           // Set the node addresses in the route reply header
      // Get the real reply source and destination
      Ipv4Address replySrc = m_finalRoute.back ();
      Ipv4Address replyDst = m_finalRoute.front ();
      /*
       * Set the route and use it in send back route reply
       */
      m_ipv4Route = SetRoute (srcAddress, m_mainAddress);
      /*
       * This part adds LBSR header to the packet and send reply
       */
      LbsrRoutingHeader lbsrRoutingHeader;
      lbsrRoutingHeader.SetNextHeader (protocol);
      lbsrRoutingHeader.SetMessageType (1);
      lbsrRoutingHeader.SetSourceId (GetIDfromIP (replySrc));
      lbsrRoutingHeader.SetDestId (GetIDfromIP (replyDst));

      uint8_t length = rrep.GetLength ();        // Get the length of the rrep header excluding the type header
      lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
      lbsrRoutingHeader.AddLbsrOption (rrep);
      Ptr<Packet> newPacket = Create<Packet> ();
      newPacket->AddHeader (lbsrRoutingHeader);
      /*
       * Send gratuitous reply
       */
      NS_LOG_INFO ("Send back gratuitous route reply");
      SendLconf (newPacket, m_mainAddress, srcAddress, m_ipv4Route);
    }
  else
    {
      NS_LOG_INFO ("The same gratuitous route reply has already sent");
    }
}

void
LbsrRouting::SendLconf (Ptr<Packet> packet,
                       Ipv4Address source,
                       Ipv4Address nextHop,
                       Ptr<Ipv4Route> route)
{
//	std::cout << "LbsrRouting::SendLconf\n";

  NS_LOG_FUNCTION (this << packet << source << nextHop);
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");

  Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (m_mainAddress));
  route->SetOutputDevice (dev);
  NS_LOG_INFO ("The output device " << dev << " packet is: " << *packet);
  //std::cout << "The output device " << dev << " packet is: " << *packet << "\n";//ここみて
 // std::cout << "route " << *route << "\n";
  uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);
  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;
  NS_LOG_INFO ("Inserting into priority queue number: " << priority);

  //m_downTarget (packet, source, nextHop, GetProtocolNumber (), route);

  /// \todo New LbsrNetworkQueueEntry
  LbsrNetworkQueueEntry newEntry (packet, source, nextHop, Simulator::Now (), route);
  if (lbsrNetworkQueue->Enqueue (newEntry))
    {
	  UniCastCount++;
      Scheduler (priority);
    }
  else
    {
      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
    }
}

void
LbsrRouting::ScheduleInitialReply (Ptr<Packet> packet,
                                  Ipv4Address source,
                                  Ipv4Address nextHop,
                                  Ptr<Ipv4Route> route)
{
	//std::cout << "LbsrRouting::ScheduleInitialReply\n";

  NS_LOG_FUNCTION (this << packet << source << nextHop);
  EventId intReply = Simulator::ScheduleNow (&LbsrRouting::SendLconf, this,
                          packet, source, nextHop, route);
  m_evrntIdVector.push_back(intReply);
}

void
LbsrRouting::ScheduleCachedReply (Ptr<Packet> packet,
                                 Ipv4Address source,
                                 Ipv4Address destination,
                                 Ptr<Ipv4Route> route,
                                 double hops)
{
	//std::cout << "LbsrRouting::ScheduleCachedReply\n";

  NS_LOG_FUNCTION (this << packet << source << destination);
  EventId cachedReply = Simulator::Schedule (Time (2 * m_nodeTraversalTime * (hops - 1 + m_uniformRandomVariable->GetValue (0,1))), &LbsrRouting::SendLconf, this, packet, source, destination, route);
  m_evrntIdVector.push_back(cachedReply);
}

void
LbsrRouting::SendAck   (uint16_t ackId,
                       Ipv4Address destination,
                       Ipv4Address realSrc,
                       Ipv4Address realDst,
                       uint8_t protocol,
                       Ptr<Ipv4Route> route)
{
	//std::cout << "LbsrRouting::SendAck\n";

  NS_LOG_FUNCTION (this << ackId << destination << realSrc << realDst << (uint32_t)protocol << route);
  NS_ASSERT_MSG (!m_downTarget.IsNull (), "Error, LbsrRouting cannot send downward");

  // This is a route reply option header
  LbsrRoutingHeader lbsrRoutingHeader;
  lbsrRoutingHeader.SetNextHeader (protocol);
  lbsrRoutingHeader.SetMessageType (1);
  lbsrRoutingHeader.SetSourceId (GetIDfromIP (m_mainAddress));
  lbsrRoutingHeader.SetDestId (GetIDfromIP (destination));

  LbsrOptionAckHeader ack;
  /*
   * Set the ack Id and set the ack source address and destination address
   */
  ack.SetAckId (ackId);
  ack.SetRealSrc (realSrc);
  ack.SetRealDst (realDst);

  uint8_t length = ack.GetLength ();
  lbsrRoutingHeader.SetPayloadLength (uint16_t (length) + 2);
  lbsrRoutingHeader.AddLbsrOption (ack);

  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (lbsrRoutingHeader);
  Ptr<NetDevice> dev = m_ip->GetNetDevice (m_ip->GetInterfaceForAddress (m_mainAddress));
  route->SetOutputDevice (dev);

  uint32_t priority = GetPriority (LBSR_CONTROL_PACKET);
  std::map<uint32_t, Ptr<lbsr::LbsrNetworkQueue> >::iterator i = m_priorityQueue.find (priority);
  Ptr<lbsr::LbsrNetworkQueue> lbsrNetworkQueue = i->second;

  NS_LOG_LOGIC ("Will be inserting into priority queue " << lbsrNetworkQueue << " number: " << priority);

  //m_downTarget (packet, m_mainAddress, destination, GetProtocolNumber (), route);

  /// \todo New LbsrNetworkQueueEntry
  LbsrNetworkQueueEntry newEntry (packet, m_mainAddress, destination, Simulator::Now (), route);
  if (lbsrNetworkQueue->Enqueue (newEntry))
    {
      Scheduler (priority);
    }
  else
    {
      NS_LOG_INFO ("Packet dropped as lbsr network queue is full");
    }
}


//here is receive
enum IpL4Protocol::RxStatus
LbsrRouting::Receive (Ptr<Packet> p,
                     Ipv4Header const &ip,
                     Ptr<Ipv4Interface> incomingInterface)
{
	//std::cout << "LbsrRouting::Receive ip is "<< ip <<"\n";

  NS_LOG_FUNCTION (this << p << ip << incomingInterface);

  NS_LOG_INFO ("Our own IP address " << m_mainAddress << " The incoming interface address " << incomingInterface);
  m_node = GetNode ();                        // Get the node
  Ptr<Packet> packet = p->Copy ();            // Save a copy of the received packet
  /*
   * When forwarding or local deliver packets, this one should be used always!!
   */
  LbsrRoutingHeader lbsrRoutingHeader;
  packet->RemoveHeader (lbsrRoutingHeader);          // Remove the LBSR header in whole
  Ptr<Packet> copy = packet->Copy ();

  uint8_t protocol = lbsrRoutingHeader.GetNextHeader ();
  uint32_t sourceId = lbsrRoutingHeader.GetSourceId ();
  Ipv4Address source = GetIPfromID (sourceId);
  NS_LOG_INFO ("The source address " << source << " with source id " << sourceId);
  /*
   * Get the IP source and destination address
   */
  Ipv4Address src = ip.GetSource ();

  bool isPromisc = false;
  uint32_t offset = lbsrRoutingHeader.GetLbsrOptionsOffset ();        // Get the offset for option header, 8 bytes in this case

  // This packet is used to peek option type
  //ここでヘッダーのoptionType以外を消している
  p->RemoveAtStart (offset);

  //Ptr<lbsr::LbsrOptions> lbsrOption;
  LbsrOptionHeader lbsrOptionHeader;
  /*
   * Peek data to get the option type as well as length and segmentsLeft field
   */
  uint32_t size = p->GetSize ();
  uint8_t *data = new uint8_t[size];
  p->CopyData (data, size);

  uint8_t optionType = 0;
  uint8_t optionLength = 0;
  uint8_t segmentsLeft = 0;

  optionType = *(data);
  //std::cout << "optionType " << (uint32_t)optionType << "\n";
  NS_LOG_LOGIC ("The option type value " << (uint32_t)optionType << " with packet id " << p->GetUid ());
  recieveLbsrOption = GetOption (optionType);       // Get the relative lbsr option and demux to the process function
  Ipv4Address promiscSource;      /// this is just here for the sake of passing in the promisc source
  //std::cout << "optiontype " << optionType <<"\n";
  //std::cout << recieveLbsrOption << "\n";
  if (optionType == 1)        // This is the routeRequest option
    {
      BlackList *blackList = m_rreqTable->FindUnidirectional (src);
      if (blackList)
        {
          NS_LOG_INFO ("Discard this packet due to unidirectional link");//一方向リンクのため、このパケットを破棄する
          m_dropTrace (p);
        }

      recieveLbsrOption = GetOption (optionType);
      /*ここでProcessを実行してる*/
      //std::cout << "Lreq process \n";

      optionLength = recieveLbsrOption->Process (p, packet, m_mainAddress, source, ip, protocol, isPromisc, promiscSource);

      if (optionLength == 0)
        {
          NS_LOG_INFO ("Discard this packet");
          //std::cout << "Discard this packet\n";
          m_dropTrace (p);
        }
    }
  else if (optionType == 2)//This is the routeReply option
    {
	  recieveLbsrOption = GetOption (optionType);
	  optionLength = recieveLbsrOption->Process (p, packet, m_mainAddress, source, ip, protocol, isPromisc, promiscSource);

      if (optionLength == 0)
        {
          NS_LOG_INFO ("Discard this packet");
          m_dropTrace (p);
        }
    }
  else if(optionType == 5)//this is the Loop Stop option
  	{
	  recieveLbsrOption = GetOption (optionType);
	  optionLength = recieveLbsrOption->Process (p, packet, m_mainAddress, source, ip, protocol, isPromisc, promiscSource);
      if (optionLength == 0)
        {
          NS_LOG_INFO ("Discard this packet");
          m_dropTrace (p);
        }
    }

  else if (optionType == 32)       // This is the ACK option
    {
      NS_LOG_INFO ("This is the ack option");
      recieveLbsrOption = GetOption (optionType);
      optionLength = recieveLbsrOption->Process (p, packet, m_mainAddress, source, ip, protocol, isPromisc, promiscSource);

      if (optionLength == 0)
        {
          NS_LOG_INFO ("Discard this packet");
          m_dropTrace (p);
        }
    }

  else if (optionType == 3)       // This is a route error header
    {
      // populate this route error
      NS_LOG_INFO ("The option type value " << (uint32_t)optionType);

      recieveLbsrOption = GetOption (optionType);
      optionLength = recieveLbsrOption->Process (p, packet, m_mainAddress, source, ip, protocol, isPromisc, promiscSource);

      if (optionLength == 0)
        {
          NS_LOG_INFO ("Discard this packet");
          m_dropTrace (p);
        }
      NS_LOG_INFO ("The option Length " << (uint32_t)optionLength);
    }

  else if (optionType == 96)       // This is the source route option
    {
	  recieveLbsrOption = GetOption (optionType);
	  optionLength = recieveLbsrOption->Process (p, packet, m_mainAddress, source, ip, protocol, isPromisc, promiscSource);
      segmentsLeft = *(data + 3);//
      if (optionLength == 0)
        {
          NS_LOG_INFO ("Discard this packet");
          m_dropTrace (p);
        }
      else
        {
          if (segmentsLeft == 0)
            {
              // / Get the next header
              uint8_t nextHeader = lbsrRoutingHeader.GetNextHeader ();
              Ptr<Ipv4L3Protocol> l3proto = m_node->GetObject<Ipv4L3Protocol> ();
              Ptr<IpL4Protocol> nextProto = l3proto->GetProtocol (nextHeader);
              if (nextProto != 0)
                {
                  // we need to make a copy in the unlikely event we hit the
                  // RX_ENDPOINT_UNREACH code path
                  // Here we can use the packet that has been get off whole LBSR header
                  enum IpL4Protocol::RxStatus status =
                    nextProto->Receive (copy, ip, incomingInterface);
                  NS_LOG_DEBUG ("The receive status " << status);
                  switch (status)
                    {
                    case IpL4Protocol::RX_OK:
                    // fall through
                    case IpL4Protocol::RX_ENDPOINT_CLOSED:
                    // fall through
                    case IpL4Protocol::RX_CSUM_FAILED:
                      break;
                    case IpL4Protocol::RX_ENDPOINT_UNREACH:
                      if (ip.GetDestination ().IsBroadcast () == true
                          || ip.GetDestination ().IsMulticast () == true)
                        {
                          break;       // Do not reply to broadcast or multicast
                        }
                      // Another case to suppress ICMP is a subnet-directed broadcast
                    }
                  return status;
                }
              else
                {
                  NS_FATAL_ERROR ("Should not have 0 next protocol value");
                }
            }
          else
            {
              NS_LOG_INFO ("This is not the final destination, the packet has already been forward to next hop");
            }
        }
    }
  else
    {
      NS_LOG_LOGIC ("Unknown Option. Drop!");
      /*
       * Initialize the salvage value to 0
       */
      uint8_t salvage = 0;

      LbsrOptionRerrUnsupportHeader rerrUnsupportHeader;
      rerrUnsupportHeader.SetErrorType (3);               // The error type 3 means Option not supported
      rerrUnsupportHeader.SetErrorSrc (m_mainAddress);       // The error source address is our own address
      rerrUnsupportHeader.SetUnsupported (optionType);       // The unsupported option type number
      rerrUnsupportHeader.SetErrorDst (src);              // Error destination address is the destination of the data packet
      rerrUnsupportHeader.SetSalvage (salvage);           // Set the value about whether to salvage a packet or not

      /*
       * The unknown option error is not supported currently in this implementation, and it's also not likely to
       * happen in simulations
       */
//            SendError (rerrUnsupportHeader, 0, protocol); // Send the error packet
    }
  //std::cout << "IpL4Protocol::RX_OK" << IpL4Protocol::RX_OK << "\n";
  return IpL4Protocol::RX_OK;
}

enum IpL4Protocol::RxStatus
LbsrRouting::Receive (Ptr<Packet> p,
                     Ipv6Header const &ip,
                     Ptr<Ipv6Interface> incomingInterface)
{
	//std::cout << "LbsrRouting::Receive\n";

  NS_LOG_FUNCTION (this << p << ip.GetSourceAddress () << ip.GetDestinationAddress () << incomingInterface);
  return IpL4Protocol::RX_ENDPOINT_UNREACH;
}

void
LbsrRouting::SetDownTarget (DownTargetCallback callback)
{
	//std::cout << "LbsrRouting::SetDownTarget\n";

  m_downTarget = callback;
}

void
LbsrRouting::SetDownTarget6 (DownTargetCallback6 callback)
{
	//std::cout << "LbsrRouting::SetDownTarget6\n";

  NS_FATAL_ERROR ("Unimplemented");
}


IpL4Protocol::DownTargetCallback
LbsrRouting::GetDownTarget (void) const
{
	//std::cout << "LbsrRouting::GetDownTarget\n";

  return m_downTarget;
}

IpL4Protocol::DownTargetCallback6
LbsrRouting::GetDownTarget6 (void) const
{
	//std::cout << "LbsrRouting::GetDownTarget6\n";

  NS_FATAL_ERROR ("Unimplemented");
  return MakeNullCallback<void,Ptr<Packet>, Ipv6Address, Ipv6Address, uint8_t, Ptr<Ipv6Route> > ();
}

void LbsrRouting::Insert (Ptr<lbsr::LbsrOptions> option)
{
	//std::cout << "LbsrRouting::Insert\n";

  m_options.push_back (option);
}

void LbsrRouting::CheckCreateRouteFlag(Ipv4Address source, Ipv4Address destination, uint8_t protocol)
{
	bool flag = GetCreateRouteFlag();
	std::cout << "CreateRouteFlag is " << flag <<"\n";
	if(flag == false){
		  std::cout <<"\n\n CheckCreateRouteFlag \n\n";
			std::vector<Ipv4Address> temp;
			temp.push_back("0.0.0.0");
			Ptr<lbsr::LbsrOptions> lbsrop;
			int nodenum=GetNodeNumber();
			lbsrop->ClearNodeCache();
			for(int i=0; i<nodenum; i++){
				lbsrop->PushBackNodeCache(false, false, false, "0.0.0.0", 255, 0);
			}
		  SendInitialRequest(source, destination, protocol);
	}
}

Ptr<lbsr::LbsrOptions> LbsrRouting::GetOption (int optionNumber)
{
	//std::cout << "LbsrRouting::GetOption\n";

  for (LbsrOptionList_t::iterator i = m_options.begin (); i != m_options.end (); ++i)
    {
      if ((*i)->GetOptionNumber () == optionNumber)
        {
          return *i;
        }
    }
  return 0;
}
}  /* namespace lbsr */
}  /* namespace ns3 */
