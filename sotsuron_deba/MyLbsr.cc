/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * This is an example script for AODV manet routing protocol. 
 *
 * Authors: Pavel Boyko <boyko@iitp.ru>
 */

#include <iostream>
#include <cmath>
#include "ns3/aodv-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/v4ping-helper.h"
#include "ns3/yans-wifi-helper.h"
/*lbsr分の追加*/
#include "ns3/lbsr-module.h"
#include <sstream>
#include "ns3/applications-module.h"
#include "ns3/config-store-module.h"
#include "ns3/netanim-module.h"
#include <vector>
#include <unistd.h>//乱数のgetpid()用
#include "ns3/energy-module.h"
#include "ns3/wifi-radio-energy-model-helper.h"
#include "ns3/device-energy-model-container.h"
#include <iomanip>

#include "ns3/lbsr-routing.h"

/*ここまで*/
using namespace ns3;

/**
 * \ingroup aodv-examples
 * \ingroup examples
 * \brief Test script.
 * 
 * This script creates 1-dimensional grid topology and then ping last node from the first one:
 * 
 * [10.0.0.1] <-- step --> [10.0.0.2] <-- step --> [10.0.0.3] <-- step --> [10.0.0.4]
 * 
 * ping 10.0.0.4
 */
class MyLbsr
{
public:
	MyLbsr ();
  /**
   * \brief Configure script parameters
   * \param argc is the command line argument count
   * \param argv is the command line arguments
   * \return true on successful configuration
  */
  bool Configure (int argc, char **argv);
  /// Run simulation
  void Run ();
  /**
   * Report results
   * \param os the output stream
   */
  void Report (std::ostream & os);

private:

  /*Lbsrのparameters*/
  uint32_t nWifis;
  uint32_t nSinks;
  double TotalTime;
  double dataTime;
  double ppers;
  uint32_t packetSize;
  double dataStart; // start sending data at 100s

  //mobility parameters
  double pauseTime;
  double nodeSpeed;
  double txpDistance;

  bool _energyDrained;
  double _remainingEnergy;

  std::string rate = "0.512kbps";
  std::string dataMode = "DsssRate11Mbps";
  std::string phyMode = "DsssRate11Mbps";

  EnergySourceContainer m_e;

  ///animation_fali

  /*ここまで*/

  // network
  /// nodes used in the example
  NodeContainer nodes;
  /// devices used in the example
  NetDeviceContainer devices;
  /// interfaces used in the example
  Ipv4InterfaceContainer interfaces;

private:
  /// Create the nodes
  void CreateNodes ();
  /// Create the devices
  void CreateDevices ();
  /// Create the network
  void InstallInternetStack ();
  /// Create the simulation applications
  Ptr<PacketSink> InstallApplications ();

  void NotifyDrained();
  EnergySourceContainer AttachEnergyModelToDevices (double initialEnergy, double txcurrent, double rxcurrent);
  void GetRemainingEnergy(double oldValue, double remainingEnergy);
  void TraceRemainingEnergy(uint32_t n, EnergySourceContainer e);
  void TotalEnergy(double oldValue, double totalEnergy);
  void TraceTotalEnergy(uint32_t n, EnergySourceContainer e);

  EnergySourceContainer GetEnergySourceContainer();

  static void Trace();
};

int main (int argc, char **argv)
{
	MyLbsr test;
  if (!test.Configure (argc, argv))
    NS_FATAL_ERROR ("Configuration failed. Aborted.");

  test.Run ();
  test.Report (std::cout);
  return 0;
}

//-----------------------------------------------------------------------------
MyLbsr::MyLbsr () :
  /*Lbsr*/
  nWifis (40),
  nSinks (1),
  TotalTime (130.0),
  dataTime (130.0),
  ppers (1),
  packetSize (64),
  dataStart (100.0),
  pauseTime (0.0),
  nodeSpeed (20.0),
  txpDistance (250.0),
  _energyDrained(false),
  _remainingEnergy(0.0)

{
}

bool
MyLbsr::Configure (int argc, char **argv)
{
  // Enable AODV logs by default. Comment this if too noisy
  //LogComponentEnable("LbsrRreqTable", LOG_LEVEL_INFO);

 SeedManager::SetSeed (getpid());//乱数をプロセスの番号で指定
	//SeedManager::SetSeed (5420);
  CommandLine cmd;

  /*lbsr*/
  cmd.AddValue ("nWifis", "Number of wifi nodes", nWifis);
  cmd.AddValue ("nSinks", "Number of SINK traffic nodes", nSinks);
  cmd.AddValue ("rate", "CBR traffic rate(in kbps), Default:8", rate);
  cmd.AddValue ("nodeSpeed", "Node speed in RandomWayPoint model, Default:20", nodeSpeed);
  cmd.AddValue ("packetSize", "The packet size", packetSize);
  cmd.AddValue ("txpDistance", "Specify node's transmit range, Default:300", txpDistance);
  cmd.AddValue ("pauseTime", "pauseTime for mobility model, Default: 0", pauseTime);
 /*ここまで*/
  cmd.Parse (argc, argv);
  return true;
}

void
MyLbsr::Run ()
{
	CommandLine cmd;
	std::string traceFlag("total");
	uint32_t traceNum=0;

	cmd.AddValue("traceFlag", "trace find(remained | total)", traceFlag);
	cmd.AddValue("traceNode", "trace Node's number", traceNum);
//  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", UintegerValue (1)); // enable rts cts all the time.
  Ptr<PacketSink> si;
  CreateNodes ();
  CreateDevices ();
  EnergySourceContainer e = AttachEnergyModelToDevices(200, 0.0174, 0.0197);
  /*
  if(traceFlag == "remained"){
	  TraceRemainingEnergy(traceNum, m_e);
  }else{
	  TraceTotalEnergy(traceNum, m_e);
  }
  */
  InstallInternetStack ();
  si=InstallApplications ();




  std::string animFile = "aodvtolbsr.xml" ;  // Name of file for animation output
  AnimationInterface anim (animFile);
  anim.EnablePacketMetadata (); // Optional
  anim.EnableIpv4L3ProtocolCounters (Seconds (0), Seconds (TotalTime)); // Optional

  std::cout << "aodvToLbsr_Start\n";
  std::cout << "Starting simulation for " << TotalTime << " s ...\n";

  Simulator::Stop (Seconds (TotalTime));
  Simulator::Run ();
  Ptr<lbsr::LbsrRouting> lbsrr;
  std::cout << "BroadCast is " << lbsrr->GetBroadCastCount() << " UniCast is " << lbsrr->GetUniCastCount() << "\n";
  Ptr<BasicEnergySource> basicPtr;
  double batt=0.0, Average=0.0;
  for(int i=0; i<(int)nWifis; i++){
	  basicPtr = DynamicCast<BasicEnergySource>(e.Get(i));
	  batt=basicPtr->GetRemainingEnergy();
	  Average+=batt;
  }
  Average = Average / nWifis;
  std::cout << "AverageBattery is " << Average << "\n";
  std::cout << "getpid() " << getpid() <<"\n";
  std::cout << "total sink packet is " << si->GetTotalRx()<<"bytes\n";
  double source = (dataTime-dataStart) * (0.512 * 1000) /8;
  std::cout << "total source packet is "<< source << "bytes\n";
  std::cout << "total packet reachability is "<<si->GetTotalRx()/source*100<<"%\n";
  Simulator::Destroy ();

  std::cout << "simulation stop\n";
}

void
MyLbsr::Report (std::ostream &)
{
}
//called by callback in LbsrTracing
void
MyLbsr::Trace ()
{
	//int count;
	//count++;
	std::cout <<"this is Trace Log \n";

//	std::cout <<"this is Trace Log " <<count<<" times \n";
}

void
MyLbsr::CreateNodes ()
{
  nodes.Create (nWifis);
  MobilityHelper mobility;
  ObjectFactory pos;
  pos.SetTypeId ("ns3::RandomBoxPositionAllocator");	//位置
  pos.Set ("X", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=500.0]"));
  pos.Set ("Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=500.0]"));
  pos.Set ("Z", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=500.0]"));

  Ptr<PositionAllocator> taPositionAlloc = pos.Create ()->GetObject<PositionAllocator> ();

  std::ostringstream speedUniformRandomVariableStream;//速さ
  speedUniformRandomVariableStream << "ns3::UniformRandomVariable[Min=0.0|Max="
                                   << nodeSpeed
                                   << "]";

  std::ostringstream pauseConstantRandomVariableStream;//止まる時間
  pauseConstantRandomVariableStream << "ns3::ConstantRandomVariable[Constant="
                                    << pauseTime
                                    << "]";


  mobility.SetMobilityModel ("ns3::RandomWaypointMobilityModel",//上３つより次の目的地・速さ・時間をもとに作る
  	  	  	  	  	  	  	  	  	"Speed", StringValue (speedUniformRandomVariableStream.str ()),
                                "Pause", StringValue (pauseConstantRandomVariableStream.str ()),
                                "PositionAllocator", PointerValue (taPositionAlloc)
                                );
  mobility.Install (nodes);

  std::cout << "createnodes\n";
}

void
MyLbsr::CreateDevices ()
{
  WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::RangePropagationLossModel", "MaxRange", DoubleValue (txpDistance));
  wifiPhy.SetChannel (wifiChannel.Create ());
  WifiHelper wifi;
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6Mbps"), "RtsCtsThreshold", UintegerValue (0));
  devices = wifi.Install (wifiPhy, wifiMac, nodes); 

  std::cout << "CreateDevices\n";
}

void
MyLbsr::InstallInternetStack ()
{
  //AodvHelper aodv;
	LbsrMainHelper lbsrMain;
	LbsrHelper lbsr;
  // you can configure AODV attributes here using aodv.Set(name, value)
  InternetStackHelper stack;
 // stack.SetRoutingHelper (lbsrMain); // has effect on the next Install ()

  stack.Install (nodes);
  lbsrMain.Install (lbsr, nodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");
  interfaces = address.Assign (devices);
/*
  Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("aodvToLbsrRouting.tr", std::ios::out);
  lbsr.PrintRoutingTableAllEvery(Seconds (5), routingStream);
*/
  std::cout << "installInternetStack\n";

}

Ptr<PacketSink>
MyLbsr::InstallApplications ()
{
  uint16_t port = 9;
  double randomStartTime = (1 / ppers) / nSinks; //distributed btw 1s evenly as we are sending 4pkt/s
  Ptr<PacketSink> si;
//  for (uint32_t i = 0; i < nSinks; ++i)
//	{

	  //ここでsink(destination)を作っている
	  PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), port));
	  ApplicationContainer apps_sink = sink.Install (nodes.Get (0));
	  std::cout << "sink is " << nodes.Get(0)<<"\n";
	  apps_sink.Start (Seconds (0.0));
	  apps_sink.Stop (Seconds (TotalTime));

	  //ここで送信側を作っている
	  OnOffHelper onoff1 ("ns3::UdpSocketFactory", Address (InetSocketAddress (interfaces.GetAddress (0), port)));
	  onoff1.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"));
	  onoff1.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));
	  onoff1.SetAttribute ("PacketSize", UintegerValue (packetSize));//64
	  onoff1.SetAttribute ("DataRate", DataRateValue (DataRate (rate)));//0.512kbps

	  std::cout << "source is " << interfaces.GetAddress (0)<<"\n";

	  ApplicationContainer apps1 = onoff1.Install (nodes.Get (0 + nWifis - nSinks));
	  apps1.Start (Seconds (dataStart + 0 * randomStartTime));
	  apps1.Stop (Seconds (dataTime + 0 * randomStartTime));
	  //apps1.Start (Seconds (0.0));
	  //apps1.Stop (Seconds (TotalTime));

	  std::cout << "test\n";

	  si = DynamicCast<PacketSink>(apps_sink.Get(0));


  std::cout << "Installapplication\n";
  return si;
}

void
MyLbsr::NotifyDrained()
{
	std::cout <<"Energy was Drained. Stop send.\n";
	_energyDrained = true;
}

EnergySourceContainer
MyLbsr::AttachEnergyModelToDevices (double initialEnergy, double txcurrent, double rxcurrent)
{
	BasicEnergySourceHelper basicSourceHelper;
	basicSourceHelper.Set("BasicEnergySourceInitialEnergyJ", DoubleValue(initialEnergy));
	EnergySourceContainer e = basicSourceHelper.Install(nodes);

	WifiRadioEnergyModelHelper radioEnergyHelper;
	radioEnergyHelper.Set("TxCurrentA", DoubleValue(txcurrent));
	radioEnergyHelper.Set("RxCurrentA", DoubleValue(rxcurrent));
	//radioEnergyHelper.SetDepletionCallback(MakeCallback(&MyLbsr::NotifyDrained, this));
	DeviceEnergyModelContainer deviceModels = radioEnergyHelper.Install(devices, e);
	return e;
}

void
MyLbsr::GetRemainingEnergy(double oldValue, double remainingEnergy)
{
	// save current remaining energy(J)
	std::cout << std::setw(10) << Simulator::Now().GetSeconds() << "t" << remainingEnergy << "\n";
	_remainingEnergy = remainingEnergy;
}

void
MyLbsr::TraceRemainingEnergy(uint32_t n, EnergySourceContainer e)
{
	Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource> (e.Get(n));
	basicSourcePtr->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&MyLbsr::GetRemainingEnergy, this));
}

void
MyLbsr::TotalEnergy(double oldValue, double totalEnergy)
{
	//NS_LOG_UNCOND(Simulator::Now().GetSeconds << "\t" << totalEnergy);
	std::cout << Simulator::Now().GetSeconds() << "\t" << totalEnergy <<"\n";
}

void
MyLbsr::TraceTotalEnergy(uint32_t n, EnergySourceContainer e)
{
	Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource>(e.Get(n));
	Ptr<DeviceEnergyModel> basicRadioModelPtr = basicSourcePtr->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0);
	NS_ASSERT(basicRadioModelPtr != NULL);
	basicRadioModelPtr->TraceConnectWithoutContext("TotalEnergyCosumption", MakeCallback(&MyLbsr::TotalEnergy, this));
}

EnergySourceContainer
MyLbsr::GetEnergySourceContainer(){
	return m_e;
};
