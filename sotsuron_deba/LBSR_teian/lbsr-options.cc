/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Yufei Cheng
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Yufei Cheng   <yfcheng@ittc.ku.edu>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */

#define NS_LOG_APPEND_CONTEXT                                   \
  if (GetObject<Node> ()) { std::clog << "[node " << GetObject<Node> ()->GetId () << "] "; }
  //else if (GetObject<EnergySourceContainer> ()) { std::clog << "[EnergySourceContainer " << GetObject<EnergySourceContainer> ()->GetTypeId () << "] "; }




#include <unistd.h>

#include <list>
#include <ctime>
#include <map>
#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/assert.h"
#include "ns3/fatal-error.h"
#include "ns3/node.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-header.h"
#include "ns3/pointer.h"
#include "ns3/node-list.h"
#include "ns3/object-vector.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ipv4-interface.h"
#include "ns3/ipv4-header.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-route.h"
#include "ns3/icmpv4-l4-protocol.h"
#include "ns3/ip-l4-protocol.h"


#include "lbsr-option-header.h"
#include "lbsr-options.h"
#include "lbsr-rcache.h"
#include "lbsr-routing.h"

#include <iomanip>
#include "ns3/energy-module.h"
#include "ns3/wifi-radio-energy-model-helper.h"
#include "ns3/device-energy-model-container.h"
//#include "/home/debakenji/ns-allinone-3.29/ns-3.29/scratch/MyLbsr.cc"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("LbsrOptions");

namespace lbsr {

NS_OBJECT_ENSURE_REGISTERED(LbsrOptions);

TypeId LbsrOptions::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptions").SetParent<Object>().SetGroupName(
					"Lbsr").AddAttribute("OptionNumber",
					"The Lbsr option number.", UintegerValue(0),
					MakeUintegerAccessor(&LbsrOptions::GetOptionNumber),
					MakeUintegerChecker<uint8_t>()).AddTraceSource("Drop",
					"Packet dropped.",
					MakeTraceSourceAccessor(&LbsrOptions::m_dropTrace),
					"ns3::Packet::TracedCallback").AddTraceSource("Rx",
					"Receive LBSR packet.",
					MakeTraceSourceAccessor(&LbsrOptions::m_rxPacketTrace),
					"ns3::lbsr::LbsrOptionSRHeader::TracedCallback");
	return tid;
}

//bool LbsrOptins::Lflag::Lreq;

LbsrOptions::LbsrOptions() {
	//std::cout << "LbsrOptions::LbsrOptions()\n";
}



LbsrOptions::~LbsrOptions() {
	NS_LOG_FUNCTION_NOARGS ();
}

void LbsrOptions::SetNode(Ptr<Node> node) {
	NS_LOG_FUNCTION(this << node);
	m_node = node;
}

Ptr<Node> LbsrOptions::GetNode() const {
	NS_LOG_FUNCTION_NOARGS ();
	return m_node;
}

bool LbsrOptions::ContainAddressAfter(Ipv4Address ipv4Address,
		Ipv4Address destAddress, std::vector<Ipv4Address> &nodeList) {
	NS_LOG_FUNCTION(this << ipv4Address << destAddress);
	std::vector<Ipv4Address>::iterator it = find(nodeList.begin(),
			nodeList.end(), destAddress);

	for (std::vector<Ipv4Address>::iterator i = it; i != nodeList.end(); ++i) {
		if ((ipv4Address == (*i)) && ((*i) != nodeList.back())) {
			return true;
		}
	}
	return false;
}

std::vector<Ipv4Address> LbsrOptions::CutRoute(Ipv4Address ipv4Address,
		std::vector<Ipv4Address> &nodeList) {
	NS_LOG_FUNCTION(this << ipv4Address);
	std::vector<Ipv4Address>::iterator it = find(nodeList.begin(),
			nodeList.end(), ipv4Address);
	std::vector<Ipv4Address> cutRoute;
	for (std::vector<Ipv4Address>::iterator i = it; i != nodeList.end(); ++i) {
		cutRoute.push_back(*i);
	}
	return cutRoute;
}

Ptr<Ipv4Route> LbsrOptions::SetRoute(Ipv4Address nextHop,
		Ipv4Address srcAddress) {
	NS_LOG_FUNCTION(this << nextHop << srcAddress);
	m_ipv4Route = Create<Ipv4Route>();
	m_ipv4Route->SetDestination(nextHop);
	m_ipv4Route->SetGateway(nextHop);
	m_ipv4Route->SetSource(srcAddress);
	return m_ipv4Route;
}

bool LbsrOptions::ReverseRoutes(std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this);
	std::vector<Ipv4Address> vec2(vec);
	vec.clear();    // To ensure vec is empty before start
	for (std::vector<Ipv4Address>::reverse_iterator ri = vec2.rbegin();
			ri != vec2.rend(); ++ri) {
		vec.push_back(*ri);
	}

	if ((vec.size() == vec2.size()) && (vec.front() == vec2.back())) {
		return true;
	}
	return false;
}

Ipv4Address LbsrOptions::SearchNextHop(Ipv4Address ipv4Address,
		std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this << ipv4Address);
	Ipv4Address nextHop;
	NS_LOG_DEBUG("the vector size " << vec.size ());
	if (vec.size() == 2) {
		NS_LOG_DEBUG("The two nodes are neighbors");
		nextHop = vec[1];
		return nextHop;
	} else {
		if (ipv4Address == vec.back()) {
			NS_LOG_DEBUG(
					"We have reached to the final destination " << ipv4Address << " " << vec.back ());
			return ipv4Address;
		}
		for (std::vector<Ipv4Address>::const_iterator i = vec.begin();
				i != vec.end(); ++i) {
			if (ipv4Address == (*i)) {
				nextHop = *(++i);
				return nextHop;
			}
		}
	}
	NS_LOG_DEBUG("next hop address not found, route corrupted");
	Ipv4Address none = "0.0.0.0";
	return none;
}

Ipv4Address LbsrOptions::ReverseSearchNextHop(Ipv4Address ipv4Address,
		std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this << ipv4Address);
	Ipv4Address nextHop;
	if (vec.size() == 2) {
		NS_LOG_DEBUG("The two nodes are neighbors");
		nextHop = vec[0];
		return nextHop;
	} else {
		for (std::vector<Ipv4Address>::reverse_iterator ri = vec.rbegin();
				ri != vec.rend(); ++ri) {
			if (ipv4Address == (*ri)) {
				nextHop = *(++ri);
				return nextHop;
			}
		}
	}
	NS_LOG_DEBUG("next hop address not found, route corrupted");
	Ipv4Address none = "0.0.0.0";
	return none;
}

Ipv4Address LbsrOptions::ReverseSearchNextTwoHop(Ipv4Address ipv4Address,
		std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this << ipv4Address);
	Ipv4Address nextTwoHop;
	NS_LOG_DEBUG("The vector size " << vec.size ());
	NS_ASSERT(vec.size() > 2);
	for (std::vector<Ipv4Address>::reverse_iterator ri = vec.rbegin();
			ri != vec.rend(); ++ri) {
		if (ipv4Address == (*ri)) {
			nextTwoHop = *(ri + 2);
			return nextTwoHop;
		}
	}
	NS_FATAL_ERROR("next hop address not found, route corrupted");
	Ipv4Address none = "0.0.0.0";
	return none;
}

void LbsrOptions::PrintVector(std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this);
	/*
	 * Check elements in a route vector
	 */
	if (!vec.size()) {
		NS_LOG_DEBUG("The vector is empty");
	} else {
		NS_LOG_DEBUG("Print all the elements in a vector");
		for (std::vector<Ipv4Address>::const_iterator i = vec.begin();
				i != vec.end(); ++i) {
			NS_LOG_DEBUG("The ip address " << *i);
		}
	}
}

bool LbsrOptions::IfDuplicates(std::vector<Ipv4Address> &vec,
		std::vector<Ipv4Address> &vec2) {
	NS_LOG_FUNCTION(this);
	for (std::vector<Ipv4Address>::const_iterator i = vec.begin();
			i != vec.end(); ++i) {
		for (std::vector<Ipv4Address>::const_iterator j = vec2.begin();
				j != vec2.end(); ++j) {
			if ((*i) == (*j)) {
				return true;
			} else {
				continue;
			}
		}
	}
	return false;
}

bool LbsrOptions::CheckDuplicates(Ipv4Address ipv4Address,
		std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this << ipv4Address);
	for (std::vector<Ipv4Address>::const_iterator i = vec.begin();
			i != vec.end(); ++i) {
		if ((*i) == ipv4Address) {
			return true;
		} else {
			continue;
		}
	}
	return false;
}

void LbsrOptions::RemoveDuplicates(std::vector<Ipv4Address> &vec) {
	NS_LOG_FUNCTION(this);
	//Remove duplicate ip address from the route if any, should not happen with normal behavior nodes
	std::vector<Ipv4Address> vec2(vec); // declare vec2 as a copy of the vec
	PrintVector(vec2); // Print all the ip address in the route
	vec.clear(); // clear vec
	for (std::vector<Ipv4Address>::const_iterator i = vec2.begin();
			i != vec2.end(); ++i) {
		if (vec.empty()) {
			vec.push_back(*i);
			continue;
		} else {
			for (std::vector<Ipv4Address>::iterator j = vec.begin();
					j != vec.end(); ++j) {
				if ((*i) == (*j)) {
					if ((j + 1) != vec.end()) {
						vec.erase(j + 1, vec.end()); // Automatic shorten the route
						break;
					} else {
						break;
					}
				} else if (j == (vec.end() - 1)) {
					vec.push_back(*i);
					break;
				} else {
					continue;
				}
			}
		}
	}
}

uint32_t LbsrOptions::GetIDfromIP(Ipv4Address address) {
	NS_LOG_FUNCTION(this << address);
	int32_t nNodes = NodeList::GetNNodes();
	for (int32_t i = 0; i < nNodes; ++i) {
		Ptr<Node> node = NodeList::GetNode(i);
		Ptr<Ipv4> ipv4 = node->GetObject<Ipv4>();
		if (ipv4->GetAddress(1, 0).GetLocal() == address) {
			return i;
		}
	}
	return 255;
}

Ptr<Node> LbsrOptions::GetNodeWithAddress(Ipv4Address ipv4Address) {
	NS_LOG_FUNCTION(this << ipv4Address);
	int32_t nNodes = NodeList::GetNNodes();
	for (int32_t i = 0; i < nNodes; ++i) {
		Ptr<Node> node = NodeList::GetNode(i);
		Ptr<Ipv4> ipv4 = node->GetObject<Ipv4>();
		int32_t ifIndex = ipv4->GetInterfaceForAddress(ipv4Address);
		if (ifIndex != -1) {
			return node;
		}
	}
	return 0;
}


std::vector<Ipv4Address> LbsrOptions::CreateRoute;
std::vector<std::vector<Ipv4Address>> LbsrOptions::SaveRoute;
void LbsrOptions::SendRouteNode(){
	std::cout<<"\n\n SendRouteNode \n\n";
	int routenum=0;
	//int minsize=0;
	std::vector<Ipv4Address> changeRoute;
	double battery;
	double minBattery=10000.0;
	for(int i=0; i<GetSaveSize(); i++){
		changeRoute=GetRouteNodeList(i);
		PushBackSaveRoute(changeRoute);
		if((int)changeRoute.size() == 3){
			routenum=i;
			break;
		}else{
			battery = GetSaveBattery(i);
			if(minBattery > battery){
				minBattery=battery;
				routenum=i;
			}
		}
	}
	changeRoute=GetRouteNodeList(routenum);
	for (std::vector<Ipv4Address>::const_iterator i = changeRoute.begin();
			i != changeRoute.end(); ++i) {
		m_finalRoute.push_back(*i);
	}
	SetCreateRoute(changeRoute);
	int addr = GetSaveAddr(routenum);
	uint8_t protocol=GetSaveProtocol(routenum);
	Ipv4Address ipv4Address=GetSaveAddress(routenum);
	uint8_t numberAddress=GetSaveNumAddr(routenum);
	battery=GetSaveBattery(routenum);
		//std::cout << "SendRouteNode battery is " << battery << "\n";

	Ptr<Node> node = GetNodeWithAddress(ipv4Address);
	Ptr<lbsr::LbsrRouting> lbsr = node->GetObject<lbsr::LbsrRouting>();

     Ipv4Address nextHop = changeRoute[changeRoute.size()-addr];
	NS_LOG_DEBUG("The nextHop address " << nextHop);

	//
	// Create the route entry to the rreq originator and save it to route cache, also need to reverse the route
	//


		//PrintVector(changeRoute);
	    ReverseRoutes(changeRoute);
		//Ipv4Address dst = changeRoute.back();
		Ipv4Address dst = "10.1.1.1";
		bool addRoute = false;
		if (numberAddress > 0) {
			LbsrRouteCacheEntry toSource(changeRoute, //IP_VECTOR=
					dst,  //dst=
					ActiveRouteTimeout //expire time=
					);
			if (lbsr->IsLinkCache()) { //here
				addRoute = lbsr->AddRoute_Link(changeRoute, ipv4Address);
			} else {
				addRoute = lbsr->AddRoute(toSource);
			}
		} else {
			NS_LOG_DEBUG("Abnormal RouteRequest");
		}

		if (addRoute) {
			//
			// Found a route to the dst, construct the source route option header
			//
			std::cout << "Create Route Time is " << Simulator::Now().GetSeconds() << "\n";
			std::cout << "------Create S->D->S ----------\n";
			for (std::vector<Ipv4Address>::const_iterator i =
					changeRoute.begin(); i != changeRoute.end(); ++i) {
				std::cout << "changeRoute is" << *i << "\n";
			}
			std::cout << "--------------------------------\n";

			Ptr<lbsr::LbsrRouting> lbsrr;
			int num = lbsrr->GetNodeNumber();
			std::string temp;
			Ipv4Address batteryAddress;
			Ptr<Node> batteryNode;
			double batt=0.0;
			double AverageBattery=0.0;
			for(int i=0; i < num; i++){
				temp = "10.1.1." + std::to_string(i+1);
				batteryAddress = ns3::Ipv4Address(temp.c_str());
				batteryNode = GetNodeWithAddress(batteryAddress);
				if (batteryNode != NULL) {
					Ptr<EnergySourceContainer> ene = batteryNode->GetObject<EnergySourceContainer>();
					if (ene != NULL) {
						//ここのeneにはipv4Addressの値だけが入っているのでGet()の値は0になる
						Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource>(ene->Get(0));
						batt = basicSourcePtr->GetRemainingEnergy();
						//std::cout << "Lconf 1 battery is " << battery << "\n";
					} else {
						//std::cout << "ene == NULL\n";
					}
				}
				AverageBattery += batt;
			}
			AverageBattery = AverageBattery / num;
			std::cout << "AverageBattery is " << AverageBattery << "\n";

			LbsrOptionSRHeader sourceRoute;
			NS_LOG_DEBUG("The route length " << changeRoute.size ());
			sourceRoute.SetNodesAddress(changeRoute);

			/// TODO !!!!!!!!!!!!!!
			/// Think about this part, we just added the route,
			/// probability no need to increase stability now?????
			// if (lbsr->IsLinkCache ())
			//   {
			//     lbsr->UseExtends (m_finalRoute);
			//   }
			sourceRoute.SetSegmentsLeft((changeRoute.size() - 2));
			// The salvage value here is 0
			sourceRoute.SetSalvage(0);
			//Ipv4Address nextHop = SearchNextHop(ipv4Address, changeRoute); // Get the next hop address
	         Ipv4Address nextHop = changeRoute[changeRoute.size()-2];
			NS_LOG_DEBUG("The nextHop address " << nextHop);
			std::cout <<"nexthop = " << nextHop<<" ipv4address = " << ipv4Address <<"\n";

			if (nextHop == "0.0.0.0") {
				//lbsr->PacketNewRoute(lbsrP, ipv4Address, dst, protocol);
				//return 0;
			}
			SetRoute(nextHop, ipv4Address);
			//
			// Send the data packet from the send buffer
			//
			lbsr->SendPacketFromBuffer(sourceRoute, nextHop, protocol);
			// Cancel the route request timer for destination after sending the data packet
			lbsr->CancelRreqTimer(dst, true);
		} else {
			NS_LOG_DEBUG("The route is failed to add in cache");
			//return 0;
		}
		//ClearRouteNodeList();
		//Setshceflag(false);
		for(int i=0; i<(int)m_eventVector.size(); i++){
			m_eventVector[i].Cancel();
		}
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionPad1);

TypeId LbsrOptionPad1::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionPad1").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionPad1>();
	return tid;
}

LbsrOptionPad1::LbsrOptionPad1() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionPad1::~LbsrOptionPad1() {
	NS_LOG_FUNCTION_NOARGS ();
}

uint8_t LbsrOptionPad1::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();

	return OPT_NUMBER;
}

uint8_t LbsrOptionPad1::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	Ptr<Packet> p = packet->Copy();
	LbsrOptionPad1Header pad1Header;
	p->RemoveHeader(pad1Header);

	isPromisc = false;

	return pad1Header.GetSerializedSize();
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionPadn);

TypeId LbsrOptionPadn::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionPadn").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionPadn>();
	return tid;
}

LbsrOptionPadn::LbsrOptionPadn() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionPadn::~LbsrOptionPadn() {
	NS_LOG_FUNCTION_NOARGS ();
}

uint8_t LbsrOptionPadn::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();
	return OPT_NUMBER;
}

uint8_t LbsrOptionPadn::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);

	Ptr<Packet> p = packet->Copy();
	LbsrOptionPadnHeader padnHeader;
	p->RemoveHeader(padnHeader);

	isPromisc = false;

	return padnHeader.GetSerializedSize();
}


//Lreq受信処理
NS_OBJECT_ENSURE_REGISTERED(LbsrOptionRreq);

TypeId LbsrOptionRreq::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionRreq").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionRreq>();
	return tid;
}

TypeId LbsrOptionRreq::GetInstanceTypeId() const {
	return GetTypeId();
}

LbsrOptionRreq::LbsrOptionRreq() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionRreq::~LbsrOptionRreq() {
	NS_LOG_FUNCTION_NOARGS ();
}

uint8_t LbsrOptionRreq::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();

	return OPT_NUMBER;
}

bool LbsrOptions::Lreq;

std::vector<bool> LbsrOptions::req_flag;
std::vector<bool> LbsrOptions::stop_flag;
std::vector<bool> LbsrOptions::conf_flag;
std::vector<std::map<Ipv4Address, int>> LbsrOptions::next;
std::vector<int> LbsrOptions::hops;
std::vector<int> LbsrOptions::addr_num;
std::vector<std::vector<Ipv4Address>> LbsrOptions::nodesRoute;

std::map<int, std::vector<LbsrRoutingHeader>> LbsrOptions::SaveReq::saveHeadermap;
std::map<int, std::vector<Ipv4Address>> LbsrOptions::SaveReq::saveAddressmap;
std::map<int, std::vector<uint8_t>> LbsrOptions::SaveReq::saveTtlmap;

bool LbsrOptions::shceflag;
std::vector<std::vector<Ipv4Address>> LbsrOptions::RouteNodeList;
std::vector<int> LbsrOptions::saveAddr;
std::vector<uint8_t> LbsrOptions::saveProtocol;
std::vector<Ipv4Address> LbsrOptions::saveAddress;
std::vector<uint8_t> LbsrOptions::saveNumAddr;
std::vector<double> LbsrOptions::saveBattery;
bool LbsrOptions::CreateFlag;


//Lreq受信処理メイン
uint8_t LbsrOptionRreq::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {

	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	// Fields from IP header
	Ipv4Address srcAddress = ipv4Header.GetSource();
	bool DstFlag = false; //ルートの中にdestination(targetAddress)が存在した場合、trueにする
	//
	// \ when the ip source address is equal to the address of our own, this is request packet originated
	// \ by the node itself, discard it
	//
	//一番最初にそれぞれのノードのキャッシュを設定する
	SaveReq savereq;
	bool LoopReq = Lreq;

	//
	// Get the node associated with the ipv4 address and get several objects from the node and leave for further use
	//
	Ptr<Node> node = GetNodeWithAddress(ipv4Address);
	Ptr<lbsr::LbsrRouting> lbsr = node->GetObject<lbsr::LbsrRouting>();


	//それぞれのノードが持つべき値の初期化
 	if (LoopReq == false) {
		//here is define nodeCache
		//i<sumNode
 		int nodenum=0;
 		nodenum=lbsr->GetNodeNumber();

 		for(int j = 0; j < nodenum; j++){
			//nCache.PushBackNodeCache(false, false, false, "0.0.0.0", 255, 0);
			PushBackNodeCache(false, false, false, "0.0.0.0", 255, 0);
		}

		LoopReq = true;
		CreateFlag = false;
		SetFlag(LoopReq);
	//	std::cout << "Lreq Second " << lflag.GetFlag() << "\n";
		m_addr = 0;
	}


	Ptr<Packet> p = packet->Copy(); // Note: The packet here doesn't contain the fixed size lbsr header
	//
	// \brief Get the number of routers' address field before removing the header
	// \peek the packet and get the value
	//
	uint8_t buf[2];
	p->CopyData(buf, sizeof(buf));
	uint8_t numberAddress = (buf[1] - 6) / 4;
	NS_LOG_DEBUG("The number of Ip addresses " << (uint32_t)numberAddress);
	if (numberAddress >= 255)   //ルート構築した時にノードが多すぎたら中止
			{
		NS_LOG_DEBUG(
				"Discard the packet, malformed header since two many ip addresses in route");
		m_dropTrace(packet); // call the drop trace to show in the tracing
		return 0;
	}

	//
	// Create the lbsr rreq header
	//
	LbsrOptionRreqHeader rreq;
	//
	// Set the number of addresses with the value from peek data and remove the rreq header
	//
	rreq.SetNumberAddress(numberAddress);
	// Remove the route request header
	p->RemoveHeader(rreq);
	// Verify the option length
	uint8_t length = rreq.GetLength();
	if (length % 2 != 0) {
		NS_LOG_LOGIC("Malformed header. Drop!");
		std::cout << "Malformed header. Drop! \n";
		m_dropTrace(packet); // call drop trace
		return 0;
	}
	// Check the rreq id for verifying the request id
	//uint16_t requestId = rreq.GetId();
	// The target address is where we want to send the data packets
	Ipv4Address targetAddress = rreq.GetTarget();
	// Get the node list and source address from the route request header
	std::vector<Ipv4Address> mainVector = rreq.GetNodesAddresses();
	std::vector<Ipv4Address> nodeList(mainVector);
	// Get the real source address of this request, it will be used when checking if we have received the save
	// route request before or not
	//Ipv4Address sourceAddress = nodeList.front();
	PrintVector(nodeList);

	int maincount=0;
	for (std::vector<Ipv4Address>::const_iterator i = nodeList.begin();
			i != nodeList.end(); i++) {
		if ((*i) == targetAddress) {
			DstFlag = true;
			//std::cout << "DstFlag is " << DstFlag << "\n";
		}else if((*i) == ipv4Address){
			maincount++;
		}

		if(maincount >= 2){
			std::cout << "nowNode is more 2";
			return 0;
		}
		//std::cout <<"nodeList "<< *i << " \n";
	}


	//
	// Construct the lbsr routing header for later use
	//
	LbsrRoutingHeader lbsrRoutingHeader;
	lbsrRoutingHeader.SetNextHeader(protocol);
	lbsrRoutingHeader.SetMessageType(1);
	lbsrRoutingHeader.SetSourceId(GetIDfromIP(source));
	lbsrRoutingHeader.SetDestId(255);

	// check whether we have received this request or not, if not, it will save the request in the table for
	// later use, if not found, return false, and push the newly received source request entry in the cache

	// Get the TTL value, this is used to test if the packet will be forwarded or not
	uint8_t ttl = ipv4Header.GetTtl();
	//bool dupRequest = false;  // initialize the duplicate request check value
	//std::cout << "The ttl value here " << (uint32_t)ttl << "\n";

	if (ttl) {
		// if the ttl value is not 0, then this request will be forwarded, then we need to
		// save it in the source entry
		//dupRequest = lbsr->FindSourceEntry(sourceAddress, targetAddress,
		///		requestId);
	}

	///
	// Before processing the route request, we need to check two things
	// 1. if this is the exact same request we have just received, ignore it
	// 2. if our address is already in the path list, ignore it
	// 3. otherwise process further



	//nodeCache[i] determine
	int nodeNum = 0;
	//Ipv4Address tempAddress = "10.1.1.0";
	std::string temp;
	int nodenum=0;
	nodenum=lbsr->GetNodeNumber();
	for(int j = 0; j < nodenum; j++){
		temp = "10.1.1." + std::to_string(j+1);
		if(ipv4Address == ns3::Ipv4Address(temp.c_str())){
			nodeNum=j;
		}
	}

//	std::cout <<"nodeNum " << nodeNum << " beforeNum " << beforeNum << "\n";
	bool rflag=false;
	bool sflag=false;
	bool cflag=false;
	std::map<Ipv4Address, int> nextmap;
	Ipv4Address nexti;
	int hp=0;
	int addr=0;


	rflag = GetReqFlag(nodeNum);
	sflag = GetStopFlag(nodeNum);
	cflag = GetConfFlag(nodeNum);
	nextmap = GetNext(nodeNum);
	hp = GetHops(nodeNum);
	addr = GetAddr(nodeNum);

	std::vector<Ipv4Address> getRoute;
	getRoute = GetNodesRoute(nodeNum);
	std::vector<Ipv4Address> nextAddresses;

	//---------------------ここでnextの値を一つに決めている--------------//
	if(ipv4Address == source){
		nexti = nodeList[1];
	}else{
		if((int)getRoute.size() >= 3){//今回のルートにdestinationがある時
			nexti = "10.1.1.1";
		}else if((int)getRoute.size() < 3){//ルートにdestinationがなく、まだLconfを受け取っていない(ノードがルートを保存していない)時
			nexti = "0.0.0.0";
			}
	}

	if (ipv4Address == source) {//受け取ったノードがソース
		if (sflag == false) {
			if (DstFlag == false) {
			//	optiontype = 2;
				rflag =true;
				cflag = true;
				addr = (nodeList.size()+1)-1;
			//	std::cout << "source and DstFlag = false and conf_flag = "<< cflag << " \n";
				//nCache.SetNodeCache(nodeNum, rflag, sflag, cflag, nexti, hp, addr);
				SetNodeCache_ButNext(nodeNum, rflag, sflag, cflag, hp, addr);
				//Ipv4Address nextHop = SearchNextHop(ipv4Address, nodeList);
			}else if (DstFlag == true) {
				//sflag = true;
				rflag =true;
				cflag = true;
				addr = (nodeList.size()+1)-1;
				//std::cout << "source and DstFlag = true and conf_flag = true "<<cflag << "\n";
				//nCache.SetNodeCache(nodeNum, rflag, sflag, cflag, nexti, hp, addr);
				SetNodeCache_ButNext(nodeNum, rflag, sflag, cflag, hp, addr);
			}
				std::vector<Ipv4Address> changeRoute(nodeList);
				changeRoute.push_back(ipv4Address);    // push back our own address

				Ipv4Address nextHop = ReverseSearchNextHop(ipv4Address, changeRoute); // get the next hop

				//create route reply and setUp ,So I should delete it
				LbsrOptionRrepHeader rrep;
				rrep.SetNodesAddress(changeRoute); // Set the node addresses in the route reply header
				NS_LOG_DEBUG("The nextHop address " << nextHop);
				Ipv4Address replyDst = changeRoute.front();
				rrep.SetAddrNum(addr);
				//
				// This part add lbsr header to the packet and send route reply packet
				//
				LbsrRoutingHeader lbsrRoutingHeader;
				lbsrRoutingHeader.SetNextHeader(protocol);
				lbsrRoutingHeader.SetMessageType(1);
				lbsrRoutingHeader.SetSourceId(GetIDfromIP(ipv4Address));
				lbsrRoutingHeader.SetDestId(GetIDfromIP(replyDst));
				// Set the route for route reply
				SetRoute(nextHop, ipv4Address);//Set m_ipv4Route

				uint8_t length = rrep.GetLength(); // Get the length of the rrep header excluding the type header
				lbsrRoutingHeader.SetPayloadLength(length + 2);
				lbsrRoutingHeader.AddLbsrOption(rrep);
				Ptr<Packet> newPacket = Create<Packet>();
				newPacket->AddHeader(lbsrRoutingHeader);
				lbsr->ScheduleInitialReply(newPacket, ipv4Address, nextHop, m_ipv4Route);
				isPromisc = false;
		}
	//	std::cout << "Set the Lconf\n";
		return rreq.GetSerializedSize();
		} else if (ipv4Address != source) {
			if (rflag == true
					&& sflag == false) {
				if (nexti == "0.0.0.0") {//まだLconfを受け取っていない
					//m_dropTrace(packet);

					uint8_t ttl = ipv4Header.GetTtl();
						//here is main req_flag=true && next=null
						mainVector.push_back(ipv4Address);
						NS_ASSERT(mainVector.front() == source);
						NS_LOG_DEBUG("Print out the main vector");
						//std::cout	<< "aaaaanot destination and do not have routeCache\n";
						rreq.SetNodesAddress(mainVector);

						Ptr<Packet> errP = p->Copy();
						if (errP->GetSize()) {
							NS_LOG_DEBUG("Error header included");
							//std::cout << "Error header included\n";
							LbsrOptionRerrUnreachHeader rerr;
							p->RemoveHeader(rerr);
							Ipv4Address errorSrc = rerr.GetErrorSrc();
							Ipv4Address unreachNode = rerr.GetUnreachNode();
							Ipv4Address errorDst = rerr.GetErrorDst();

							if ((errorSrc == srcAddress)
									&& (unreachNode == ipv4Address)) {
								NS_LOG_DEBUG(
										"The error link back to work again");
								uint16_t length = rreq.GetLength();
								NS_LOG_DEBUG(
										"The RREQ header length " << length);
								std::cout << "The RREQ header length " << length
										<< "\n";
								lbsrRoutingHeader.AddLbsrOption(rreq);
								lbsrRoutingHeader.SetPayloadLength(length + 2);
							} else {
								lbsr->DeleteAllRoutesIncludeLink(errorSrc,
										unreachNode, ipv4Address);

								LbsrOptionRerrUnreachHeader newUnreach;
								newUnreach.SetErrorType(1);
								newUnreach.SetErrorSrc(errorSrc);
								newUnreach.SetUnreachNode(unreachNode);
								newUnreach.SetErrorDst(errorDst);
								newUnreach.SetSalvage(rerr.GetSalvage()); // Set the value about whether to salvage a packet or not
								uint16_t length = rreq.GetLength()
										+ newUnreach.GetLength();
								NS_LOG_DEBUG(
										"The RREQ and newUnreach header length " << length);
								//std::cout<< "The RREQ and newUnreach header length "<< length << "\n";
								lbsrRoutingHeader.SetPayloadLength(length + 4);
								lbsrRoutingHeader.AddLbsrOption(rreq);
								lbsrRoutingHeader.AddLbsrOption(newUnreach);
							}
						} else { //別にエラーもない
							uint16_t length = rreq.GetLength();
							NS_LOG_DEBUG("The RREQ header length " << length);
						//	std::cout << "The RREQ header length " << length
							//		<< "\n";
							lbsrRoutingHeader.AddLbsrOption(rreq);
							lbsrRoutingHeader.SetPayloadLength(length + 2);
						}
						// Get the TTL value
						NS_LOG_DEBUG("The ttl value here " << (uint32_t)ttl);
						if (ttl) {
							//Ptr<Packet> interP = Create<Packet>();
							SocketIpTtlTag tag;
							tag.SetTtl(ttl - 1);
							//interP->AddPacketTag(tag);
							//interP->AddHeader(lbsrRoutingHeader);
							//lbsr->ScheduleInterRequest(interP);
							isPromisc = false;
						//	std::cout << "SaveReq \n";
							savereq.PushBackSaveReq(nodeNum, lbsrRoutingHeader, ipv4Address, ttl);
						}
						return 0;
						//return rreq.GetSerializedSize();
				} else if (nexti != "0.0.0.0") {//Lconfを受け取ったことがある
					mainVector.push_back(ipv4Address);
			//		optiontype = 1;		   //Lreq
					//nextにユニキャスト
					NS_ASSERT(mainVector.front() == source);
					NS_LOG_DEBUG("Print out the main vector");
					PrintVector(mainVector);
					rreq.SetNodesAddress(mainVector);

					Ptr<Packet> errP = p->Copy();
					if (errP->GetSize()) {
						NS_LOG_DEBUG("Error header included");
						LbsrOptionRerrUnreachHeader rerr;
						p->RemoveHeader(rerr);
						Ipv4Address errorSrc = rerr.GetErrorSrc();
						Ipv4Address unreachNode = rerr.GetUnreachNode();
						Ipv4Address errorDst = rerr.GetErrorDst();

						if ((errorSrc == srcAddress)
								&& (unreachNode == ipv4Address)) {
							NS_LOG_DEBUG("The error link back to work again");
							//std::cout << "The error link back to work again \n";
							uint16_t length = rreq.GetLength();
							NS_LOG_DEBUG("The RREQ header length " << length);
							lbsrRoutingHeader.AddLbsrOption(rreq);
							lbsrRoutingHeader.SetPayloadLength(length + 2);
						} else {
							lbsr->DeleteAllRoutesIncludeLink(errorSrc,
									unreachNode, ipv4Address);

							LbsrOptionRerrUnreachHeader newUnreach;
							newUnreach.SetErrorType(1);
							newUnreach.SetErrorSrc(errorSrc);
							newUnreach.SetUnreachNode(unreachNode);
							newUnreach.SetErrorDst(errorDst);
							newUnreach.SetSalvage(rerr.GetSalvage()); // Set the value about whether to salvage a packet or not
							uint16_t length = rreq.GetLength()
									+ newUnreach.GetLength();
							NS_LOG_DEBUG(
									"The RREQ and newUnreach header length " << length);
							//std::cout<< "The RREQ and newUnreach header length\n";
							lbsrRoutingHeader.SetPayloadLength(length + 4);
							lbsrRoutingHeader.AddLbsrOption(rreq);
							lbsrRoutingHeader.AddLbsrOption(newUnreach);
						}
					} else {
						uint16_t length = rreq.GetLength();
						NS_LOG_DEBUG("The RREQ header length " << length);
						lbsrRoutingHeader.AddLbsrOption(rreq);
						lbsrRoutingHeader.SetPayloadLength(length + 2);
					}
					// Get the TTL value
					uint8_t ttl = ipv4Header.GetTtl();
					///
					// Decrease the TTL value in the packet tag by one, this tag will go to ip layer 3 send function
					// and drop packet when TTL value equals to 0
					///
					NS_LOG_DEBUG("The ttl value here " << (uint32_t)ttl);
					if (ttl) {
						Ptr<Packet> interP = Create<Packet>();
						SocketIpTtlTag tag;
						tag.SetTtl(ttl - 1);
						interP->AddPacketTag(tag);
						interP->AddHeader(lbsrRoutingHeader);

						lbsr->ScheduleInterRequest(interP);
						//lbsr->SendLreqUnicast(interP, source, nexti);
						isPromisc = false;
					}
					return rreq.GetSerializedSize();		       //ここはoptiontype=1
				}
			} else if (rflag == false
					&& sflag == false) {
				uint8_t ttl = ipv4Header.GetTtl();
				if(ttl){
					rflag = true;
					if(ipv4Address == targetAddress ){
						std::cout << "des get Lreq\n";
					}
					//std::cout << "\n\n------rflag = true so, start lbsr-----\n";
				}else{
					rflag = false;
				}
				//nCache.SetNodeCache(nodeNum, rflag, sflag, cflag, nexti, hp, addr);
				SetNodeCache_ButNext(nodeNum, rflag, sflag, cflag, hp, addr);
			//	std::cout << "nodeNum " << nodeNum << " req_flag "
					//	<< rflag << "\n";
				//アドレスの追加
				//ブロードキャスト
				mainVector.push_back(ipv4Address);

				NS_ASSERT(mainVector.front() == source);
				NS_LOG_DEBUG("Print out the main vector");
			//	std::cout << "not destination and do not have routeCache\n";
				PrintVector(mainVector);

				rreq.SetNodesAddress(mainVector);//rreqにアドレスを入れている

				Ptr<Packet> errP = p->Copy();
				if (errP->GetSize()) {//パケットに何も入ってない
					NS_LOG_DEBUG("Error header included");
					//std::cout << "Error header included\n";
					LbsrOptionRerrUnreachHeader rerr;
					p->RemoveHeader(rerr);
					Ipv4Address errorSrc = rerr.GetErrorSrc();
					Ipv4Address unreachNode = rerr.GetUnreachNode();
					Ipv4Address errorDst = rerr.GetErrorDst();

					if ((errorSrc == srcAddress)
							&& (unreachNode == ipv4Address)) {
						NS_LOG_DEBUG("The error link back to work again");
						uint16_t length = rreq.GetLength();
						NS_LOG_DEBUG("The RREQ header length " << length);
						std::cout << "The RREQ header length " << length<< "\n";
						lbsrRoutingHeader.AddLbsrOption(rreq);
						lbsrRoutingHeader.SetPayloadLength(length + 2);
					} else {
						lbsr->DeleteAllRoutesIncludeLink(errorSrc, unreachNode,
								ipv4Address);

						LbsrOptionRerrUnreachHeader newUnreach;
						newUnreach.SetErrorType(1);
						newUnreach.SetErrorSrc(errorSrc);
						newUnreach.SetUnreachNode(unreachNode);
						newUnreach.SetErrorDst(errorDst);
						newUnreach.SetSalvage(rerr.GetSalvage()); // Set the value about whether to salvage a packet or not
						uint16_t length = rreq.GetLength()
								+ newUnreach.GetLength();
						NS_LOG_DEBUG(
								"The RREQ and newUnreach header length " << length);
						//std::cout << "The RREQ and newUnreach header length "<< length << "\n";
						lbsrRoutingHeader.SetPayloadLength(length + 4);
						lbsrRoutingHeader.AddLbsrOption(rreq);
						lbsrRoutingHeader.AddLbsrOption(newUnreach);
					}
				} else { //別にエラーもない
					uint16_t length = rreq.GetLength();
					NS_LOG_DEBUG("The RREQ header length " << length);
				//	std::cout << "The RREQ header length " << length << "\n";
					lbsrRoutingHeader.AddLbsrOption(rreq);
					lbsrRoutingHeader.SetPayloadLength(length + 2);
				}
				// Get the TTL value
				///
				// Decrease the TTL value in the packet tag by one, this tag will go to ip layer 3 send function
				// and drop packet when TTL value equals to 0
				///
				NS_LOG_DEBUG("The ttl value here " << (uint32_t)ttl);
				//std::cout << "The ttl value here " << (uint32_t) ttl << "\n";
				if (ttl) {
					Ptr<Packet> interP = Create<Packet>();
					SocketIpTtlTag tag;
					tag.SetTtl(ttl - 1);
					interP->AddPacketTag(tag);
					interP->AddHeader(lbsrRoutingHeader);
					lbsr->ScheduleInterRequest(interP);//OptionsからRoutingに送信している
					//lbsr->SendLreqUnicast(interP, source, nexti);
					//ユニキャストの場合のScheduleInterRequestを作る

					isPromisc = false;
				}
		//		std::cout << "last optiontype " << optiontype << "\n";
			//	std::cout << "rreq.GetSerializedSize() "
				//		<< rreq.GetSerializedSize() << "\n";
				return rreq.GetSerializedSize();		       //ここはoptiontype=1
				//return optiontype;
			}
		}
	return rreq.GetSerializedSize();
	//return optiontype;
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionRrep);

//Lreq受信処理
TypeId LbsrOptionRrep::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionRrep").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionRrep>();
	return tid;
}

LbsrOptionRrep::LbsrOptionRrep() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionRrep::~LbsrOptionRrep() {
	NS_LOG_FUNCTION_NOARGS ();
}

TypeId LbsrOptionRrep::GetInstanceTypeId() const {
	return GetTypeId();
}

uint8_t LbsrOptionRrep::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();

	return OPT_NUMBER;
}

//Lconf受信処理メイン
uint8_t LbsrOptionRrep::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	//std::cout << "LbsrOptionRrep::Process Ipv4Address " << ipv4Address
		//	<< " source " << source << "\n";
	Ptr<Packet> p = packet->Copy();

	SaveReq savereq;

	// Get the number of routers' address field
	uint8_t buf[2];
	p->CopyData(buf, sizeof(buf));
	uint8_t numberAddress = (buf[1] - 2) / 4;

	LbsrOptionRrepHeader rrep;//Lconf
	rrep.SetNumberAddress(numberAddress); // Set the number of ip address in the header to reserver space for deserialize header
	p->RemoveHeader(rrep);

	Ptr<Node> node = GetNodeWithAddress(ipv4Address);
	Ptr<lbsr::LbsrRouting> lbsr = node->GetObject<lbsr::LbsrRouting>();

	NS_LOG_DEBUG("The next header value " << (uint32_t)protocol);

	std::vector<Ipv4Address> nodeList = rrep.GetNodesAddress();

	//int optiontype = 0;
	//int nextcount = 1;
	int tempcount = 0;
	bool DstFlag = false;
	Ipv4Address targetAddress = "10.1.1.1";//ここを変えたい
	//nodeListにルートが入っている
	for (std::vector<Ipv4Address>::const_iterator i = nodeList.begin();
			i != nodeList.end(); ++i) {
		//std::cout << *i << " ";
		if ((*i) == ipv4Address) {
			//nextcount = tempcount;
		}
		if ((*i) == targetAddress) {
			DstFlag = true;  //メンバにすればいらない
			//std::cout <<"get destination\n";
		}
		tempcount++;
	}
	//std::cout << "nextcount " <<nextcount << "\n";
	int nodeNum = 0;
	//Ipv4Address tempAddress = "10.1.1.0";
	//Must change here by nodeNumber
	std::string temp;
	int nodenum=0;
	nodenum=lbsr->GetNodeNumber();
	for(int j = 0; j < nodenum; j++){
		temp = "10.1.1." + std::to_string(j+1);
		if(ipv4Address == ns3::Ipv4Address(temp.c_str())){
			nodeNum=j;
		}
	}
	//std::cout <<"nodeNum " << nodeNum << " beforeNum " << beforeNum << "\n";
	//NodeCache nCache;

	bool rflag = false;
	bool sflag = false;
	bool cflag = false;
	std::map<Ipv4Address, int>nextmap;
	std::vector<Ipv4Address> getRoute;
	Ipv4Address nexti = "0.0.0.0";
	int hp = 0;
	int addr = 0;

	rflag = GetReqFlag(nodeNum);
	sflag = GetStopFlag(nodeNum);
	cflag = GetConfFlag(nodeNum);

	nextmap = GetNext(nodeNum);
	getRoute = GetNodesRoute(nodeNum);
	if((int)getRoute.size() >= 3){
		nexti="10.1.1.1";
	}else{
		nexti="0.0.0.0";
	}
	hp = GetHops(nodeNum);
	//addr = GetAddr(nodeNum);
	addr= rrep.GetAddrNum();
	//std::cout << "addr is " << addr << "nodeNum " << nodeNum << "\n";


	if (ipv4Address == nodeList.back()) {//受け取ったのがソース

		Ptr<lbsr::LbsrRouting> lbsrRouting;
//		Ptr<Node> allnode = lbsrRouting->GetNode();
		if (DstFlag == false) {
			m_dropTrace(packet);
			//std::cout << "dst is not, so packrt drop \n";
			return 0;
		} else if (DstFlag == true) {
			double battery;
			if (node != NULL) {
				Ptr<EnergySourceContainer> ene = node->GetObject<EnergySourceContainer>();
				if (ene != NULL) {
					//ここのeneにはipv4Addressの値だけが入っているのでGet()の値は0になる
					Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource>(ene->Get(0));
					//std::cout << "Source Battery is " <<basicSourcePtr->GetRemainingEnergy() << "\n";
					//rrep.SetBattery(basicSourcePtr->GetRemainingEnergy());
				} else {
					//std::cout << "ene == NULL\n";
				}
			}
			battery= rrep.GetBatter();
			//std::cout <<"battery is " << battery<<"\n";
			//here, must change (create shcadule),(create class)
			//std::cout << "create s->d->s \n";
			std::vector<Ipv4Address> changeRoute(nodeList);
			SetRouteNodeList(changeRoute, addr, protocol, ipv4Address, numberAddress, battery);

			bool shflag=Getshceflag();
			if(shflag==false){
				Ptr<lbsr::LbsrRouting> lbsrr;
				std::cout << "BroadCast " << lbsrr->GetBroadCastCount() << " Unicast " <<lbsrr->GetUniCastCount() << "\n";
				EventId SendLconf = Simulator::Schedule(Seconds(3), &LbsrOptions::SendRouteNode, this);
				m_eventVector.push_back(SendLconf);
				//SendRouteNode();
				Setshceflag(true);
			}
		}
		return 0;

	} else if (ipv4Address != source) {
		if (nexti == "0.0.0.0") {
			if(ipv4Address == targetAddress){
				std::cout << "des get Lconf\n";
			}


			hp = addr;
			addr=addr-1;
			rrep.SetAddrNum(addr);
			//addr = bfaddr - 1;
			nexti = nodeList[nodeList.size()-addr];
			cflag = true;
			SetNodesRoute(nodeNum, nodeList);
			//optiontype = 2;
			//nCache.SetNodeCache(nodeNum, rflag, sflag, cflag, nexti, hp, addr);
			SetNodeCache(nodeNum, rflag, sflag, cflag, nexti, hp, addr);
			LbsrRoutingHeader saveheader;
			Ipv4Address address;
			uint8_t ttl;
			//std::cout << "First Lconf\n";
			int size=0;
			//size = saveIpv4Address.size();
			size = savereq.GetVectorSize(nodeNum);
			for(int i=0; i<size; i++){
				saveheader = savereq.GetHeader(nodeNum, i);
				//isPromisc=SaveReq::getInstance().GetIsPromisc(i);
				address=savereq.GetIpv4Address(nodeNum, i);
				ttl=savereq.GetTtl(nodeNum, i);
				//LbsrRouting lbsrRoute;
				LbsrRoutingHeader lbsrRoutingHeader;

				NS_LOG_DEBUG("The ttl value here " << (uint32_t)ttl);
				//std::cout << "The ttl value here " << (uint32_t) ttl << "\n";
				SetRoute(nexti, address);
				if (ttl) {
					Ptr<Packet> saveP = Create<Packet>();
					SocketIpTtlTag tag;
					tag.SetTtl(ttl - 1);
					saveP->AddPacketTag(tag);
					saveP->AddHeader(saveheader);
					lbsr->ScheduleInterRequest(saveP);
				      //lbsr->SendLconf (p, address, nexti, m_ipv4Route);
						//lbsr->SendLreqUnicast(p, address, nexti);
				}
				//return rr.GetSerializedSize();		       //ここはoptiontype=1
			}
			savereq.ClearVector(nodeNum);
		} else if (nexti != "0.0.0.0") {

			if (hp > addr) {
				hp = addr;
				addr=addr-1;
				rrep.SetAddrNum(addr);
				nexti = nodeList[nodeList.size()-addr];
				SetNodesRoute(nodeNum, nodeList);
			//	optiontype = 2;
				SetNodeCache(nodeNum, rflag, sflag, cflag, nexti, hp, addr);
				//nextにユニキャスト
			} else if (hp <= addr) {
				addr=addr-1;
				rrep.SetAddrNum(addr);
				//addr = bfaddr - 1;
			//	optiontype = 2;
				SetNodeCache_ButNext(nodeNum, rflag, sflag, cflag, hp, addr);
				//nodeList[nextcount]にユニキャスト
			}
		}
		double battery=0.0;
		if (node != NULL) {
			Ptr<EnergySourceContainer> ene = node->GetObject<EnergySourceContainer>();
			if (ene != NULL) {
				//ここのeneにはipv4Addressの値だけが入っているのでGet()の値は0になる
				Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource>(ene->Get(0));
				battery = basicSourcePtr->GetRemainingEnergy();
				//std::cout << "Lconf 1 battery is " << battery << "\n";
				rrep.SetBattery(battery);
			} else {
				//std::cout << "ene == NULL\n";
			}
		}
		battery=rrep.GetBatter();
		//std::cout << "Lconf 2 battery is " << battery << "\n";

		std::vector<Ipv4Address> changeRoute(nodeList);
		//Ipv4Address nextHop = ReverseSearchNextHop(ipv4Address, changeRoute); // get the next hop
         Ipv4Address nextHop = nodeList[nodeList.size()-addr];
		//create route reply and setUp ,So I should delete it
		rrep.SetNodesAddress(changeRoute); // Set the node addresses in the route reply header
		NS_LOG_DEBUG("The nextHop address " << nextHop);
		Ipv4Address replyDst = changeRoute.front();
		//
		// This part add lbsr header to the packet and send route reply packet
		//
		LbsrRoutingHeader lbsrRoutingHeader;
		lbsrRoutingHeader.SetNextHeader(protocol);
		lbsrRoutingHeader.SetMessageType(1);
		lbsrRoutingHeader.SetSourceId(GetIDfromIP(ipv4Address));
		lbsrRoutingHeader.SetDestId(GetIDfromIP(replyDst));
		// Set the route for route reply
		SetRoute(nextHop, ipv4Address);	//Set m_ipv4Route

		uint8_t length = rrep.GetLength(); // Get the length of the rrep header excluding the type header
		lbsrRoutingHeader.SetPayloadLength(length + 2);
		lbsrRoutingHeader.AddLbsrOption(rrep);
		Ptr<Packet> newPacket = Create<Packet>();
		newPacket->AddHeader(lbsrRoutingHeader);
		lbsr->ScheduleInitialReply(newPacket, ipv4Address, nextHop,
				m_ipv4Route);
	}
	//std::cout << "LbsrOptionsRrep\n";
	return rrep.GetSerializedSize();
	//return optiontype;
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionLstop);

//Lstop受信処理
TypeId LbsrOptionLstop::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionLstop").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionRrep>();
	return tid;
}

LbsrOptionLstop::LbsrOptionLstop() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionLstop::~LbsrOptionLstop() {
	NS_LOG_FUNCTION_NOARGS ();
}

TypeId LbsrOptionLstop::GetInstanceTypeId() const {
	return GetTypeId();
}

uint8_t LbsrOptionLstop::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();

	return OPT_NUMBER;
}

//Lstop受信処理メイン
uint8_t LbsrOptionLstop::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	//std::cout << "LbsrOptionLstop::Process Ipv4Address " << ipv4Address	<< " source " << source << "\n";
	Ptr<Packet> p = packet->Copy();

	// Get the number of routers' address field
	uint8_t buf[2];
	p->CopyData(buf, sizeof(buf));
	uint8_t numberAddress = (buf[1] - 2) / 4;

	LbsrOptionLstopHeader lstop;//Lconf
	lstop.SetNumberAddress(numberAddress); // Set the number of ip address in the header to reserver space for deserialize header
	p->RemoveHeader(lstop);

	Ptr<Node> node = GetNodeWithAddress(ipv4Address);
	Ptr<lbsr::LbsrRouting> lbsr = node->GetObject<lbsr::LbsrRouting>();

	NS_LOG_DEBUG("The next header value " << (uint32_t)protocol);

	std::vector<Ipv4Address> nodeList = lstop.GetNodesAddress();

	//int optiontype = 0;
	//int nextcount = 1;
	int tempcount = 0;
	//Ipv4Address targetAddress = "10.1.1.0";
	//nodeListにルートが入っている
	for (std::vector<Ipv4Address>::const_iterator i = nodeList.begin();
			i != nodeList.end(); ++i) {
	//	std::cout << *i << " ";
		if ((*i) == ipv4Address) {
			//nextcount = tempcount;
		}
		tempcount++;
	}


	int nodeNum = 0;

	//Must change here by nodeNumber
	std::string temp;
	int nodenum=0;
	nodenum=lbsr->GetNodeNumber();
	for(int j = 0; j < nodenum; j++){
		temp = "10.1.1." + std::to_string(j+1);
		if(ipv4Address == ns3::Ipv4Address(temp.c_str())){
			nodeNum=j;
		}
		if(source == ns3::Ipv4Address(temp.c_str())){
			//beforeNum=j;
		}
	}

	//NodeCache nCache;

	//bool rflag = false;
	bool sflag = false;
	//bool cflag = false;
	std::vector<Ipv4Address> getRoute;
	std::map<Ipv4Address, int> nextmap;
	//Ipv4Address nexti = "0.0.0.0";
	//int hp = 0;
	int addr = 0;

	//rflag = GetReqFlag(nodeNum);
	sflag = GetStopFlag(nodeNum);
	//cflag = GetConfFlag(nodeNum);
	nextmap = GetNext(nodeNum);
	//hp = GetHops(nodeNum);
	addr = GetAddr(nodeNum);

	//int bfaddr = 0;
	//bfaddr = GetAddr(beforeNum);

	//std::cout << "nodeCache[beforeNum] " << bfaddr << "\n";
	if(ipv4Address == nodeList.back())//ipv4Address == source
	{
		//std::cout << "get Lstop \n";
		m_dropTrace(packet);
		return 0;
	}else
	{
		sflag = true;
		SetStopFlag(nodeNum, sflag);
		//unicast
		std::vector<Ipv4Address> changeRoute(nodeList);
		//Ipv4Address nextHop = ReverseSearchNextHop(ipv4Address, changeRoute); // get the next hop
         Ipv4Address nextHop = nodeList[nodeList.size()-addr];
		//create route reply and setUp ,So I should delete it
		lstop.SetNodesAddress(changeRoute); // Set the node addresses in the route reply header
		NS_LOG_DEBUG("The nextHop address " << nextHop);
		Ipv4Address replyDst = changeRoute.front();
		//
		// This part add lbsr header to the packet and send route reply packet
		//
		LbsrRoutingHeader lbsrRoutingHeader;
		lbsrRoutingHeader.SetNextHeader(protocol);
		lbsrRoutingHeader.SetMessageType(1);
		lbsrRoutingHeader.SetSourceId(GetIDfromIP(ipv4Address));
		lbsrRoutingHeader.SetDestId(GetIDfromIP(replyDst));
		// Set the route for route reply
		SetRoute(nextHop, ipv4Address);	//Set m_ipv4Route

		uint8_t length = lstop.GetLength(); // Get the length of the rrep header excluding the type header
		lbsrRoutingHeader.SetPayloadLength(length + 2);
		lbsrRoutingHeader.AddLbsrOption(lstop);
		Ptr<Packet> newPacket = Create<Packet>();
		newPacket->AddHeader(lbsrRoutingHeader);
		lbsr->ScheduleInitialReply(newPacket, ipv4Address, nextHop,m_ipv4Route);
		return lstop.GetSerializedSize();
	}
	return 0;
}



NS_OBJECT_ENSURE_REGISTERED(LbsrOptionSR);

TypeId LbsrOptionSR::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionSR").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionSR>();
	return tid;
}

LbsrOptionSR::LbsrOptionSR() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionSR::~LbsrOptionSR() {
	NS_LOG_FUNCTION_NOARGS ();
}

TypeId LbsrOptionSR::GetInstanceTypeId() const {
	return GetTypeId();
}

uint8_t LbsrOptionSR::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();
	return OPT_NUMBER;
}

uint8_t LbsrOptionSR::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Address << ipv4Header << (uint32_t)protocol << isPromisc);
	Ptr < Packet > p = packet->Copy();
	// Get the number of routers' address field
	uint8_t buf[2];
	p->CopyData(buf, sizeof(buf));
	uint8_t numberAddress = (buf[1] - 2) / 4;
	LbsrOptionSRHeader sourceRoute;
	sourceRoute.SetNumberAddress(numberAddress);
	p->RemoveHeader(sourceRoute);

	// The route size saved in the source route
	std::vector < Ipv4Address > nodeList = sourceRoute.GetNodesAddress();
	uint8_t segsLeft = sourceRoute.GetSegmentsLeft();
	uint8_t salvage = sourceRoute.GetSalvage();
	/*
	 * Get the node from IP address and get the LBSR extension object
	 */
	Ptr < Node > node = GetNodeWithAddress(ipv4Address);
	Ptr < lbsr::LbsrRouting > lbsr = node->GetObject<lbsr::LbsrRouting>();
	/*
	 * Get the source and destination address from ipv4 header
	 */
	Ipv4Address srcAddress = ipv4Header.GetSource();
	Ipv4Address destAddress = ipv4Header.GetDestination();

	// Get the node list destination
	//Ipv4Address destination = nodeList.back();
	Ipv4Address destination = "10.1.1.1";
	/*
	 * If it's a promiscuous receive data packet,
	 * 1. see if automatic route shortening possible or not
	 * 2. see if it is a passive acknowledgment
	 */
	if (isPromisc) {
		//here is Getting S->D->S in DSR. So, this process do in Lconf when dstflag = true

		NS_LOG_LOGIC("We process promiscuous receipt data packet");
		//std::cout << "We process promiscuous receipt data packet\n";
		if (ContainAddressAfter(ipv4Address, destAddress, nodeList)) {
			NS_LOG_LOGIC("Send back the gratuitous reply");
			lbsr->SendGratuitousReply(source, srcAddress, nodeList, protocol);
		}

		uint16_t fragmentOffset = ipv4Header.GetFragmentOffset();
		uint16_t identification = ipv4Header.GetIdentification();

		if (destAddress != destination) {
			NS_LOG_DEBUG("Process the promiscuously received packet");
			//std::cout << "Process the promiscuously received packet\n";
			bool findPassive = false;
			int32_t nNodes = NodeList::GetNNodes();
			for (int32_t i = 0; i < nNodes; ++i) {
				NS_LOG_DEBUG("Working with node " << i);

				Ptr < Node > node = NodeList::GetNode(i);
				Ptr < lbsr::LbsrRouting > lbsrNode = node->GetObject<
						lbsr::LbsrRouting>();
				// The source and destination addresses here are the real source and destination for the packet
				findPassive = lbsrNode->PassiveEntryCheck(packet, source,
						destination, segsLeft, fragmentOffset, identification,
						false);
				if (findPassive) {
					break;
				}
			}

			if (findPassive) {
				NS_LOG_DEBUG("We find one previously received passive entry");
				/*
				 * Get the node from IP address and get the LBSR extension object
				 * the srcAddress would be the source address from ip header
				 */
				PrintVector (nodeList);

				NS_LOG_DEBUG("promisc source " << promiscSource);
				Ptr < Node > node = GetNodeWithAddress(promiscSource);
				Ptr < lbsr::LbsrRouting > lbsrSrc = node->GetObject<
						lbsr::LbsrRouting>();
				lbsrSrc->CancelPassiveTimer(packet, source, destination,
						segsLeft);
			} else {
				NS_LOG_DEBUG("Saved the entry for further use");
				lbsr->PassiveEntryCheck(packet, source, destination, segsLeft,
						fragmentOffset, identification, true);
			}
		}
		/// Safely terminate promiscuously received packet
		return 0;
	} else {
		/*
		 * Get the number of address from the source route header
		 */
		//std::cout << "Destination Receives data packet \n";
		uint8_t length = sourceRoute.GetLength();
		uint8_t nextAddressIndex;
		Ipv4Address nextAddress;

		// Get the option type value
		uint32_t size = p->GetSize();
		uint8_t *data = new uint8_t[size];
		p->CopyData(data, size);
		uint8_t optionType = 0;
		optionType = *(data);
		/// When the option type is 160, means there is ACK request header after the source route, we need
		/// to send back acknowledgment
		if (optionType == 160) {
			NS_LOG_LOGIC(
					"Remove the ack request header and add ack header to the packet");
			// Here we remove the ack packet to the previous hop
			LbsrOptionAckReqHeader ackReq;
			p->RemoveHeader(ackReq);
			uint16_t ackId = ackReq.GetAckId();
			/*
			 * Send back acknowledgment packet to the earlier hop
			 * If the node list is not empty, find the previous hop from the node list,
			 * otherwise, use srcAddress
			 */
			Ipv4Address ackAddress = srcAddress;
			if (!nodeList.empty()) {
				if (segsLeft > numberAddress) // The segmentsLeft field should not be larger than the total number of ip addresses
						{
					NS_LOG_LOGIC("Malformed header. Drop!");
					m_dropTrace(packet);
					return 0;
				}
				// -fstrict-overflow sensitive, see bug 1868
				if (numberAddress - segsLeft < 2)   // The index is invalid
						{
					NS_LOG_LOGIC("Malformed header. Drop!");
					m_dropTrace(packet);
					return 0;
				}
				ackAddress = nodeList[numberAddress - segsLeft - 2];
			}
			m_ipv4Route = SetRoute(ackAddress, ipv4Address);
			NS_LOG_DEBUG(
					"Send back ACK to the earlier hop " << ackAddress << " from us " << ipv4Address);
			lbsr->SendAck(ackId, ackAddress, source, destination, protocol,
					m_ipv4Route);
		}
		/*
		 * After send back ACK, check if the segments left value has turned to 0 or not, if yes, update the route entry
		 * and return header length
		 */
		if (segsLeft == 0) {
			NS_LOG_DEBUG("This is the final destination");
			isPromisc = false;
			return sourceRoute.GetSerializedSize();
		}

		if (length % 2 != 0) {
			NS_LOG_LOGIC("Malformed header. Drop!");
			m_dropTrace(packet);
			return 0;
		}

		if (segsLeft > numberAddress) // The segmentsLeft field should not be larger than the total number of ip addresses
				{
			NS_LOG_LOGIC("Malformed header. Drop!");
			m_dropTrace(packet);
			return 0;
		}

		LbsrOptionSRHeader newSourceRoute;
		newSourceRoute.SetSegmentsLeft(segsLeft - 1);
		newSourceRoute.SetSalvage(salvage);
		newSourceRoute.SetNodesAddress(nodeList);
		nextAddressIndex = numberAddress - segsLeft;
		nextAddress = newSourceRoute.GetNodeAddress(nextAddressIndex);
		NS_LOG_DEBUG(
				"The next address of source route option " << nextAddress << " and the nextAddressIndex: " << (uint32_t)nextAddressIndex << " and the segments left : " << (uint32_t)segsLeft);
		/*
		 * Get the target Address in the node list
		 */
		//Ipv4Address targetAddress = nodeList.back();
		Ipv4Address targetAddress ="10.1.1.1";
		Ipv4Address realSource = nodeList.front();
		/*
		 * Search the vector for next hop address
		 */
		Ipv4Address nextHop = SearchNextHop(ipv4Address, nodeList);
		PrintVector (nodeList);

		if (nextHop == "0.0.0.0") {
			NS_LOG_DEBUG("Before new packet " << *lbsrP);
			lbsr->PacketNewRoute(lbsrP, realSource, targetAddress, protocol);
			return 0;
		}

		if (ipv4Address == nextHop) {
			NS_LOG_DEBUG("We have reached the destination");
			newSourceRoute.SetSegmentsLeft(0);
			return newSourceRoute.GetSerializedSize();
		}
		// Verify the multicast address, leave it here for now
		if (nextAddress.IsMulticast() || destAddress.IsMulticast()) {
			m_dropTrace(packet);
			return 0;
		}
		// Set the route and forward the data packet
		SetRoute(nextAddress, ipv4Address);
		NS_LOG_DEBUG("lbsr packet size " << lbsrP->GetSize ());
		lbsr->ForwardPacket(lbsrP, newSourceRoute, ipv4Header, realSource,
				nextAddress, targetAddress, protocol, m_ipv4Route);
	}
	return sourceRoute.GetSerializedSize();
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionRerr);

TypeId LbsrOptionRerr::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionRerr").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionRerr>();
	return tid;
}

LbsrOptionRerr::LbsrOptionRerr() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionRerr::~LbsrOptionRerr() {
	NS_LOG_FUNCTION_NOARGS ();
}

TypeId LbsrOptionRerr::GetInstanceTypeId() const {
	return GetTypeId();
}

uint8_t LbsrOptionRerr::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();
	return OPT_NUMBER;
}

uint8_t LbsrOptionRerr::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	std::cout << "Recieve Error packet , ipv4Address is " << ipv4Address <<"\n";
	Ptr < Packet > p = packet->Copy();
	uint32_t size = p->GetSize();
	uint8_t *data = new uint8_t[size];
	p->CopyData(data, size);
	uint8_t errorType = *(data + 2);
	/*
	 * Get the node from Ip address and get the lbsr extension object
	 */
	Ptr < Node > node = GetNodeWithAddress(ipv4Address);
	Ptr < lbsr::LbsrRouting > lbsr = node->GetObject<lbsr::LbsrRouting>();
	/*
	 * The error serialized size
	 */
	uint32_t rerrSize;
	NS_LOG_DEBUG("The error type value here " << (uint32_t)errorType);
	if (errorType == 1) // unreachable ip address
			{
		/*
		 * Remove the route error header from the packet, and get the error type
		 */
		LbsrOptionRerrUnreachHeader rerrUnreach;
		p->RemoveHeader(rerrUnreach);
		/*
		 * Get the error destination address
		 */
		Ipv4Address unreachAddress = rerrUnreach.GetUnreachNode();
		Ipv4Address errorSource = rerrUnreach.GetErrorSrc();

		NS_LOG_DEBUG(
				"The error source is " << rerrUnreach.GetErrorDst () << "and the unreachable node is " << unreachAddress);
		/*
		 * Get the serialized size of the rerr header
		 */
		rerrSize = rerrUnreach.GetSerializedSize();
		/*
		 * Delete all the routes including the unreachable node address from the route cache
		 */
		Ptr < Node > node = GetNodeWithAddress(ipv4Address);
		lbsr->DeleteAllRoutesIncludeLink(errorSource, unreachAddress,
				ipv4Address);

		Ptr < Packet > newP = p->Copy();
		uint32_t serialized = DoSendError(newP, rerrUnreach, rerrSize,
				ipv4Address, protocol);
		return serialized;
	} else {
		/*
		 * Two other type of error headers:
		 * 1. flow state not supported type-specific information
		 * 2. unsupported option with option number
		 */
		/*
		 * Remove the route error header from the packet, and get the error type
		 */
		LbsrOptionRerrUnsupportHeader rerrUnsupport;
		p->RemoveHeader(rerrUnsupport);
		rerrSize = rerrUnsupport.GetSerializedSize();

		NS_UNUSED(rerrSize);
		/// \todo This is for the other two error options, not supporting for now
		// uint32_t serialized = DoSendError (p, rerrUnsupport, rerrSize, ipv4Address, protocol);
		uint32_t serialized = 0;
		return serialized;
	}
}

uint8_t LbsrOptionRerr::DoSendError(Ptr<Packet> p,
		LbsrOptionRerrUnreachHeader &rerr, uint32_t rerrSize,
		Ipv4Address ipv4Address, uint8_t protocol) {
	// Get the number of routers' address field
	uint8_t buf[2];
	p->CopyData(buf, sizeof(buf));
	uint8_t numberAddress = (buf[1] - 2) / 4;

	// Here remove the source route header and schedule next hop error transmission
	NS_LOG_DEBUG("The number of addresses " << (uint32_t)numberAddress);
	LbsrOptionSRHeader sourceRoute;
	sourceRoute.SetNumberAddress(numberAddress);
	p->RemoveHeader(sourceRoute);
	NS_ASSERT(p->GetSize() == 0);
	/*
	 * Get the node from ip address and the lbsr extension object
	 */
	Ptr < Node > node = GetNodeWithAddress(ipv4Address);
	Ptr < lbsr::LbsrRouting > lbsr = node->GetObject<lbsr::LbsrRouting>();
	/*
	 * Get the segments left field and the next address
	 */
	uint8_t segmentsLeft = sourceRoute.GetSegmentsLeft();
	uint8_t length = sourceRoute.GetLength();
	uint8_t nextAddressIndex;
	Ipv4Address nextAddress;
	/*
	 * Get the route size and the error target address
	 */
	std::vector < Ipv4Address > nodeList = sourceRoute.GetNodesAddress();
	Ipv4Address targetAddress = nodeList.back();
	/*
	 * The total serialized size for both the rerr and source route headers
	 */
	uint32_t serializedSize = rerrSize + sourceRoute.GetSerializedSize();

	if (length % 2 != 0) {
		NS_LOG_LOGIC("Malformed header. Drop!");
		m_dropTrace(p);
		return 0;
	}

	if (segmentsLeft > numberAddress) {
		NS_LOG_LOGIC("Malformed header. Drop!");
		m_dropTrace(p);
		return 0;
	}
	/*
	 * When the error packet has reached to the destination
	 */
	if (segmentsLeft == 0 && targetAddress == ipv4Address) {
		NS_LOG_INFO("This is the destination of the error, send error request");
		lbsr->SendErrorRequest(rerr, protocol);
		return serializedSize;
	}

	// Get the next Router Address
	LbsrOptionSRHeader newSourceRoute;
	newSourceRoute.SetSegmentsLeft(segmentsLeft - 1);
	nextAddressIndex = numberAddress - segmentsLeft;
	nextAddress = sourceRoute.GetNodeAddress(nextAddressIndex);
	newSourceRoute.SetSalvage(sourceRoute.GetSalvage());
	newSourceRoute.SetNodesAddress(nodeList);
	nextAddress = newSourceRoute.GetNodeAddress(nextAddressIndex);

	/// to test if the next address is multicast or not
	if (nextAddress.IsMulticast() || targetAddress.IsMulticast()) {
		m_dropTrace(p);
		return serializedSize;
	}
	std::string temp;
	Ipv4Address realSource;
	int nodenum=lbsr->GetNodeNumber();
	temp = "10.1.1." + std::to_string(nodenum);
	realSource == ns3::Ipv4Address(temp.c_str());

	if(ipv4Address == realSource){
		  for(int i=0; i<nodenum; i++){
			SetNodeCache(i, false, false, false, "0.0.0.0", 255, 0);
			//std::cout <<"node "<< i<< ", rflag " << GetReqFlag(i)<<", next " << lbsrop->GetNext(i)<<"\n";
		  }
	}

	// Set the route entry
	SetRoute(nextAddress, ipv4Address);
	lbsr->ForwardErrPacket(rerr, newSourceRoute, nextAddress, protocol,
			m_ipv4Route);
	return serializedSize;
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionAckReq);

TypeId LbsrOptionAckReq::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionAckReq").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionAckReq>();
	return tid;
}

LbsrOptionAckReq::LbsrOptionAckReq() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionAckReq::~LbsrOptionAckReq() {
	NS_LOG_FUNCTION_NOARGS ();
}

TypeId LbsrOptionAckReq::GetInstanceTypeId() const {
	return GetTypeId();
}

uint8_t LbsrOptionAckReq::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();
	return OPT_NUMBER;
}

uint8_t LbsrOptionAckReq::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	/*
	 * Current implementation of the ack request header processing is coded in source route header processing
	 */
	/*
	 * Remove the ack request header
	 */
	std::cout <<"LbsrOptionAckReq \n";
	Ptr < Packet > p = packet->Copy();
	LbsrOptionAckReqHeader ackReq;
	p->RemoveHeader(ackReq);
	/*
	 * Get the node with ip address and get the lbsr extension and reoute cache objects
	 */
	Ptr < Node > node = GetNodeWithAddress(ipv4Address);
	Ptr < lbsr::LbsrRouting > lbsr = node->GetObject<lbsr::LbsrRouting>();

	NS_LOG_DEBUG("The next header value " << (uint32_t)protocol);

	return ackReq.GetSerializedSize();
}

NS_OBJECT_ENSURE_REGISTERED(LbsrOptionAck);

TypeId LbsrOptionAck::GetTypeId() {
	static TypeId tid =
			TypeId("ns3::lbsr::LbsrOptionAck").SetParent<LbsrOptions>().SetGroupName(
					"Lbsr").AddConstructor<LbsrOptionAck>();
	return tid;
}

LbsrOptionAck::LbsrOptionAck() {
	NS_LOG_FUNCTION_NOARGS ();
}

LbsrOptionAck::~LbsrOptionAck() {
	NS_LOG_FUNCTION_NOARGS ();
}

TypeId LbsrOptionAck::GetInstanceTypeId() const {
	return GetTypeId();
}

uint8_t LbsrOptionAck::GetOptionNumber() const {
	NS_LOG_FUNCTION_NOARGS ();
	return OPT_NUMBER;
}

uint8_t LbsrOptionAck::Process(Ptr<Packet> packet, Ptr<Packet> lbsrP,
		Ipv4Address ipv4Address, Ipv4Address source,
		Ipv4Header const &ipv4Header, uint8_t protocol, bool &isPromisc,
		Ipv4Address promiscSource) {
	NS_LOG_FUNCTION(
			this << packet << lbsrP << ipv4Address << source << ipv4Header << (uint32_t)protocol << isPromisc);
	/*
	 * Remove the ACK header
	 */
	std::cout <<"LbsrOptionAck\n";
	Ptr < Packet > p = packet->Copy();
	LbsrOptionAckHeader ack;
	p->RemoveHeader(ack);
	/*
	 * Get the ACK source and destination address
	 */
	Ipv4Address realSrc = ack.GetRealSrc();
	Ipv4Address realDst = ack.GetRealDst();
	uint16_t ackId = ack.GetAckId();
	/*
	 * Get the node with ip address and get the lbsr extension and route cache objects
	 */
	Ptr < Node > node = GetNodeWithAddress(ipv4Address);
	Ptr < lbsr::LbsrRouting > lbsr = node->GetObject<lbsr::LbsrRouting>();
	lbsr->UpdateRouteEntry(realDst);
	/*
	 * Cancel the packet retransmit timer when receiving the ack packet
	 */
	lbsr->CallCancelPacketTimer(ackId, ipv4Header, realSrc, realDst);
	return ack.GetSerializedSize();
}

} // namespace lbsr
} // namespace ns3
